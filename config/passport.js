var passport = require('passport'),
    mongoose = require('mongoose');
module.exports = function() {
    var User = mongoose.model('User');
     var Control = mongoose.model('Controller');
  

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    passport.deserializeUser(function(id, done) {
        User.findOne({
            _id: id
        }, '-password -salt', function(err, user) {
            done(err, user);
        });
    });

passport.serializeUser(function(control, done) {
        done(null, control.id);
    });
    passport.deserializeUser(function(id, done) {
        Control.findOne({
            _id: id
        }, '-password -salt', function(err, control) {
            done(err, control);
        });
    });
   
    require('./strategies/local.js')();
   
};