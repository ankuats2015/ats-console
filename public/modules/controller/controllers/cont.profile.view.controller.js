var cont = angular.module('controller');
cont.controller('contProfileController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', '$route', '$modal', 'Scopes', 'ngDialog',
  function($rootScope, $scope, $http, $location, Authentication, myService, $route, $modal, Scopes, ngDialog)
  {
    $scope.user = Authentication.user;
    $scope.data = {};
    $scope.datas = {};
    $scope.notfound = false;
    var formdata = {};
    $scope.passwordresetmsg = "";
    var passwordData = {};
    $scope.search = {};
    $scope.addcontrollermsg = "";
    $scope.resetPassword = {};
    $scope.userid = '';
    $scope.username = "";
    $scope._id = "";
    //$scope.format="";
    $scope.selected = {};
    // If user is not signed in then redirect back home
    if (!$scope.user || $scope.user.userType != 'controller') $location.path('/');
    $scope.contprofiletab = true;
    $scope.getInit = function()
    {
      checkUserType($scope.user.userRole ? $scope.user.userRole : $scope.user.roles[0]);
      $scope.searchresults = true;
      $scope.searchcontrol = true;
      $scope.addcontview = false;
      $scope.resetPassView = false;
      $scope.edit = false;
      $scope.typed = true;
    }
    $scope.showAddView = function()
    {
      $scope.searchresults = false;
      $scope.searchcontrol = true;
      $scope.addcontview = true;
      $scope.resetPassView = false;
      $scope.data.end_date="12/31/2999";
    }
    $scope.resetPass = function($event, val)
    {
      console.log(val);
      ngDialog.openConfirm(
      {
        template: 'modalDialogId',
        className: 'ngdialog-theme-default',
        scope: $scope
      }).then(function(value)
      {
        console.log('Modal promise resolved. Value: ', value);
      passwordData.userId = $scope.user.username;
      passwordData.names = val.username;
      passwordData.id = val._id;
      passwordData.temp_pass=value;
      console.log(passwordData)
        $http.post('/controller/resetPass', passwordData).success(function(response)
        {
          //$rootScope.alerts.push({msg:,type:"success"});
          $scope.passwordresetmsg = "Password successfully reset";
          $scope.error = "";
          $scope.resetPassword.newPassword = null;
          $scope.resetPassword.verifyPassword = null;
          $scope.check = true;
          $scope.password = false;
          $scope.resetPassView = false;
          $rootScope.enableLoader = false;
          // $location.path('/password-reset/success');
        }).error(function(response)
        {
          $rootScope.enableLoader = false;
          //$scope.passwordDetails="";
          $scope.error = response.message;
        });
      }, function(reason)
      {
        console.log('Modal promise rejected. Reason: ', reason);
      });
      $event.preventDefault();
      $scope.searchresults = false;
      $scope.searchcontrol = true;
      $scope.addcontview = false;
      // $scope.resetPassView=true;
      // $scope.userid=val.username;
      // $scope.username=val.firstname;
      // $scope._id=val._id;
      // console.log(val);
    }
    $scope.searchResult = function(event)
    {
      event.preventDefault();
      $scope.datas = {};
      $scope.dat = "";
      var string = [];
      $scope.searchresults = true;
      $scope.searchcontrol = true;
      $scope.addcontview = false;
      angular.forEach($scope.search, function(val, key)
      {
        //console.log(key);
        switch (key)
        {
          case "eff_date":
            string.push(
            {
              "tag": "eff_date",
              "val": val
            });
            break;
          case "end_date":
            string.push(
            {
              "tag": "end_date",
              "val": val
            });
            break;
          case "controllername":
            string.push(
            {
              "tag": "controllername",
              "val": val
            });
            break;
        }
      });
      var searchResult = $http.post('/controller/searchUsername/', string);
      searchResult.success(function(res)
      {
        if (res.length > 0)
        {
          $scope.datas = res;
          console.log(res);
        }
        else
        {
          console.log("lknca");
          $scope.dat = "No Data Found";
        }
      });
    }
    $scope.saveController = function(event)
      {
        event.preventDefault();
        $rootScope.alert = [];
        console.log($scope.data);
        console.log(new Date($scope.data.eff_date));
        console.log(new Date($scope.data.end_date));
        formdata = $scope.data;
        console.log(new Date($scope.data.eff_date).toString());
        formdata.eff_date = new Date($scope.data.eff_date).toString();
        formdata.end_date = new Date($scope.data.end_date).toString();
        formdata.pass_status = 0;
        formdata.password = "password";
        formdata.salt = "salt";
        formdata.controllername = $scope.data.firstname + " " + $scope.data.lastname;
        console.log(formdata);
        var data = $http.post('/controller/saveController', formdata);
        data.success(function(res)
        {
          console.log(res);
          $scope.passwordresetmsg = res.msg;
          if (res.msg == "Controller Successfully Created")
          {
            $scope.successmsg = res.msg;
            $scope.addcontview = false;
          }
          else
          {
            $scope.passwordresetmsg = res.msg;
            $scope.addcontview = true;
          }
          /*  $rootScope.alerts.push({
              type:'success',
              msg:"Controller is add Successfully"
            })*/
          // $route.reload();
          //$location.path('/controller/custprofile');
        });
      }
      /*  $scope.editRow=function(row)
   {
    
    console.log(row);
    $scope.edit=true;
    $scope.typed=false;

   }
*/
    $scope.getTemplate = function(dat)
    {
      if (dat.username === $scope.selected.username)
      {
        return 'edit';
      }
      else return 'display';
    };
    $scope.editRow = function(dat)
    {
      $scope.selected = angular.copy(dat);
    };
    $scope.reset = function()
    {
      $scope.selected = {};
    };
    $scope.cancelRow = function()
    {
      $scope.selected = {};
    };
    $scope.updateRow = function($event, dat)
    {
      $event.preventDefault();
      console.log(dat.username);
      var data = {
        "username": dat.username,
        "usertype": $scope.data.user_type ? $scope.data.user_type : dat.user_type,
        "enddate": $scope.data.end_date ? $scope.data.end_date : dat.end_date,
        "lastupdate": new Date,
        "lastupdateby": $scope.user.username
      };
      console.log(data);
      var response = $http.post('/controller/update/', data);
      response.success(function(data)
      {
        console.log(data);
        $scope.searchResult($event);
        $scope.reset();
      });
    }
    $scope.searchController = function(val)
    {
      return $http.get('/controller/search',
      {
        params:
        {
          word: val
        }
      }).then(function(response)
      {
        return response.data.map(function(item)
        {
          console.log(item);
          return item;
        });
      });
    };
    $scope.passwordResetDetail = function(event)
    {
      passwordData = $scope.resetPassword;
    
      console.log(passwordData);
      $http.post('/controller/resetPass', passwordData).success(function(response)
      {
        //$rootScope.alerts.push({msg:,type:"success"});
        $scope.passwordresetmsg = "Password successfully reset";
        $scope.error = "";
        $scope.resetPassword.newPassword = null;
        $scope.resetPassword.verifyPassword = null;
        $scope.check = true;
        $scope.password = false;
        $scope.resetPassView = false;
        $rootScope.enableLoader = false;
        // $location.path('/password-reset/success');
      }).error(function(response)
      {
        $rootScope.enableLoader = false;
        //$scope.passwordDetails="";
        $scope.error = response.message;
      });
    };
    $scope.cancelblock = function()
    {
      $scope.check = true;
      $scope.password = false;
      $scope.resetPassView = false;
    }

    function checkUserType(utype)
    {
      if (utype === 'Super User' || utype === "superuser")
      {
        $scope.addclienttab = true;
        $scope.updateclienttab = true;
        $scope.salestaxtab = true;
        $scope.customertab = true;
        $scope.controllertab = true;
        $scope.companytab = true;
      }
      if (utype === 'Admin')
      {
        $scope.addclienttab = true;
        $scope.updateclienttab = true;
        $scope.salestaxtab = true;
        $scope.customertab = true;
        $scope.controllertab = false;
        $scope.companytab = false;
      }
    }
  }
]);
//ModalInstanceCtrl  Controller Block Here 
cont.controller('ModalInstanceCtrl', function($scope, $rootScope, $modalInstance, Scopes)
{
  //  $scope.items = items;
  $scope.data = {};
  $scope.query = [];
  // $scope.selected = {
  //     item: $scope.items
  // };
  $scope.ok = function(index)
  {
    $modalInstance.close($scope.selected.item);
    console.log(index);
    console.log($scope.selected);
    $scope.query.city = $scope.data.city;
  };
  $scope.showValue = function(index)
  {
    $scope.data = $scope.items[index];
    $scope.checkconfirm = true;
    //$scope.$broadcast('parent', 'Some data');
    $rootScope.$broadcast('datas', $scope.items[index]);
    console.log($rootScope.$broadcast('datas', $scope.items[index]));
    Scopes.store('ModalInstanceCtrl', $scope.items[index]);
  };
  $scope.cancel = function()
  {
    $modalInstance.dismiss('cancel');
  };
});
cont.factory('Scopes', function($rootScope)
{
  var mem = {};
  return {
    store: function(key, value)
    {
      mem[key] = value;
    },
    get: function(key)
    {
      return mem[key];
    }
  };
});