'use strict';

// Authentication service for user variables
angular.module('controller').factory('vendorAndUserTempData', [

	function () {
		var data = {};

		return {
			getData: function () {
            //You could also return specific attribute of the form data instead
            //of the entire data
            return data;
        },
        setData: function (newData) {
            //You could also set specific attribute of the form data instead
            data = newData;
        },
        resetData: function () {
            //To be called when the data stored needs to be discarded
            data = {};
        }
    };
}
]);