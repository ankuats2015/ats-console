angular.module('controller').factory('myService', ['$http', function($http) {
    var formData = null;
    var regCardData = null;
    var tempdata = null;
    var userid = null;
    return {
        saveOrderData: function(orderData)
        {
            return $http({
                method: "post",
                url: "orders",
                data: orderData
            });
        },
        sendRecepentMail: function(recepentData)
        {
            return $http({
                method: "post",
                url: "mail/recepent",
                data: recepentData
            });
        },
        sendUserMail: function(userData)
        {
            return $http({
                method: "post",
                url: "mail/order-confirm",
                data: userData
            });
        },
        getData: function() {
            //You could also return specific attribute of the form data instead
            //of the entire data
            return formData;
        },
        setData: function(newFormData) {
            //You could also set specific attribute of the form data instead
            formData = newFormData
        },
        resetData: function() {
            //To be called when the data stored needs to be discarded
            formData = null;
        },
        getTempData: function() {
            //You could also return specific attribute of the form data instead
            //of the entire data
            return tempdata; //$http({method: "get", url: "controllers/gettempdata"});
        },
        setTempData: function(newData) {
            //You could also set specific attribute of the form data instead
            tempdata = newData
            return $http({
                method: "post",
                url: "controllers/tempdata",
                data: newData
            });
        },
        resetTempData: function() {
            //To be called when the data stored needs to be discarded
            tempdata = {};
        },
        setRegCardData: function(newData) {
            regCardData = newData
        },
        resetRegCardData: function() {
            //To be called when the data stored needs to be discarded
            tempdata = {};
        },
        getRegCardData: function() {
            return regCardData
        },
        getUserId: function() {
            //You could also return specific attribute of the form data instead
            //of the entire data
            return userid; //$http({method: "get", url: "controllers/gettempdata"});
        },
        setUserId: function(newData) {
            //You could also set specific attribute of the form data instead
            userid = newData
                //return $http({method: "post", url: "controllers/tempdata", data: newData});
        },
        resetUserId: function() {
            //To be called when the data stored needs to be discarded
            userid = {};
        },
        getCardDetail: function(cards) {
            var col = [];
            angular.forEach(cards, function(val, key)
            {
                if (val.cards.length > 0)
                {
                    col.push(val.cards);
                }
            });
            if (col.length > 0)
            {
                return col;
            }
        },
    };
}]);