'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Order = mongoose.model('Orders'),
    Product = mongoose.model('Product'),
    config = require('../../../config/config'),
    UserTypes = mongoose.model('userType'),
    nodemailer = require('nodemailer'),
    path = require('path'),
    fs = require('fs');
var async = require('async');
var cfg = require('cfg');
/**
 * User middleware
 */
var smtpTransport = nodemailer.createTransport(config.mailer.options);
exports.userByID = function(req, res, next, id) {
    User.findOne({
        _id: id
    }).exec(function(err, user) {
        if (err) return next(err);
        if (!user) return next(new Error('Failed to load User ' + id));
        req.profile = user;
        next();
    });
};


/**
 * User middleware
 */
exports.userWithVendorByID = function(req, res) {
    User.findOne({
            _id: req.params.id
        })
        .populate('vendorId')
        .exec(function(err, user) {
            //  if (err) return next(err);
            if (!user) return res.status(401).send({
                message: 'Failed to load User ' + id
            });

            user.password = null;
            user.salt = null;
            return res.json(user);

        });

};


/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.status(401).send({
            message: 'User is not logged in'
        });
    }

    next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
    var _this = this;

    return function(req, res, next) {
        _this.requiresLogin(req, res, function() {
            if (_.intersection(req.user.roles, roles).length) {
                return next();
            } else {
                return res.status(403).send({
                    message: 'User is not authorized'
                });
            }
        });
    };
};

//For Checking Email id 

exports.getEmail = function(req, res, next) {
    console.log("hello");
    var regex = req.params.email; // 'i' makes it case insensitive
    console.log(regex);
    return User.find({
            email: regex
        })
        .exec(function(err, email) {
            if (err) {
                // return next(err);
            } else {
                console.log(email);
                res.json(email);

            }
        });

};


//function for getting users detail

exports.getAllUsers = function(req, res) {
    return User.find({
        $and: [{
            "email": req.params.id
        }, {
            "userType": "vendor"
        }]
    }).exec(function(err, user) {
        if (err) {
            res.json(err);
        } else {

            res.json(user);
        }
    });
};

//function for getting users detail
exports.uploadImage = function(req, res) {

    var data = _.pick(req.body, 'type'),
        uploadPath = path.normalize(cfg.data + '../../../public/modules/controller/img/logo/'),
        file = req.files.file;
    console.log("jkljh" + uploadPath);
    var ext = file.name.split('.').pop();
    var d = new Date();
    var fName = d.getTime() + '.' + ext;
    console.log(file);
    fs.readFile(file.path, function(err, data) //This function read the file from  temp folder
        {
            file.fName = fName;
            var newPath = __dirname + "/../../../public/modules/controller/img/logo/" + fName;
            fs.writeFile(newPath, data, function(err) //this function write the image in to destination folder
                {
                    if (err) throw err;
                    else return res.json(file);

                });
        });
};

//function for getting users detail

exports.numberCheck = function(req, res) {
    console.log(req.params.sd);
    return User.find({
        "contact.number": req.params.sd
    }).exec(function(err, user) {
        if (err) {
            res.json(err);
        } else {
            console.log(user);
            res.json(user);
        }
    });
};

exports.getSalesDetail = function(req, res) {
    var reqday = req.params.date;
    var reqmon = new Date('1 ' + req.params.mon + req.params.yr).getMonth() + 1;
    var reqyr = req.params.yr;
    var current = new Date();
    if (reqday == current.getDate() && reqmon == current.getMonth() + 1 && reqyr == current.getFullYear()) {
        //Start Date Setting
        var startday = ('0' + (reqday)).slice(-2);
        var startmon = ('0' + (reqmon)).slice(-2);
        var startyr = reqyr;
        var start = new Date(startyr + "-" + startmon + "-" + startday + "T00:00:00Z").toISOString();
        //Last Date Setting
        var lastday = ('0' + (reqday)).slice(-2);
        var lastmon = ('0' + (reqmon)).slice(-2);
        var lastyr = reqyr;
        var lattime = new Date(lastyr + "-" + lastmon + "-" + lastday).getTime();
        console.log(lattime)
        var last = new Date(lastyr + "-" + lastmon + "-" + lastday + "T23:59:59Z").toISOString();
        console.log(last);
        Order.find({
                "transaction.created": {
                    $lte: last,
                    $gte: start
                }
            })
            .exec(function(err, data) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(data);
                    res.json(data);
                }
            });


    } else {
        //Start Date Setting
        var startday = ('0' + (reqday)).slice(-2);
        var startmon = ('0' + (reqmon)).slice(-2);
        var startyr = reqyr;
        var start = new Date(startyr + "-" + startmon + "-" + startday + "T00:00:00Z").toISOString();
        //Last Date Setting
        var lastday = ('0' + (reqday)).slice(-2);
        var lastmon = ('0' + (reqmon)).slice(-2);
        var lastyr = reqyr;
        var lattime = new Date(lastyr + "-" + lastmon + "-" + lastday).getTime();
        console.log(lattime)
        var last = new Date(lastyr + "-" + lastmon + "-" + lastday + "T23:59:59Z").toISOString();
        console.log(last);
        Order.find({
                "transaction.created": {
                    $lte: last,
                    $gte: start
                }
            })
            .exec(function(err, data) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(data);
                    res.json(data);
                }
            });
    }

    /*



  
  var reqdate=new Date(req.params.date);
  var curdate=new Date(req.params.date);
    var lastdate=new Date(reqdate.setDate(curdate.getDate() - 1));
    var lastdates=('0' + (lastdate.getDate())).slice(-2);
    var curdates=('0' + (curdate.getDate())).slice(-2);
   

  var curmon=('0' + (curdate.getMonth()+1)).slice(-2);
  var lastmon=('0' + (lastdate.getMonth()+1)).slice(-2);

  var curyr=curdate.getFullYear();
  var lastyr=lastdate.getFullYear();

    
  var last = new Date(lastyr+"-"+lastmon+"-"+lastdates+"T00:00:00Z").toISOString();
  var cur = new Date(curyr+"-"+curmon+"-"+curdates+"T23:59:59Z").toISOString();
    
    console.log(last);
    console.log(cur);
   
     Order.find({"transaction.created":{$lte:cur,$gte:last}})
                     .exec(function (err, data) {
                      if (err) {
                               console.log(err);
                            } else {
                              console.log(data);
                               res.json(data);                                
                            }
                    });
*/


};

exports.getMyPrice = function(req, res) {
    console.log("i am here" + req.params.id);
    return Product.find({
            "_id": req.params.id
        }, {
            "price.my_p": true
        })
        .exec(function(err, data) {
            if (err) {
                // return next(err);
            } else {

                res.json(data);

            }
        });


};
exports.getSalesDetails = function(req, res) {

    UserTypes.find({}, function(err, data) {
        if (err) {
            return next(err);
        } else {
            res.json(data);
        }
    });
};

//Get Monthly Sales Report 
exports.getMonthlySales = function(req, res) {
    var mon = req.params.mon;
    var yr = req.params.yr;
    var monno = new Date('1 ' + mon + yr).getMonth() + 1;
    var date = new Date();
    if (monno == date.getMonth() + 1 && yr == date.getFullYear()) {
        var startday = "01";
        console.log(monno);
        var startmonth = ('0' + (monno)).slice(-2);
        console.log(startmonth);
        var startyear = parseInt(yr);
        var start = new Date(startyear + "-" + startmonth + "-" + startday + "T00:00:00Z").toISOString();
        var day = ('0' + (date.getDate() - 1)).slice(-2);
        var mon = ('0' + (date.getMonth() + 1)).slice(-2);
        var end = new Date(yr + "-" + mon + "-" + day + "T23:59:59Z").toISOString();
        Order.find({
            "transaction.created": {
                $gte: start,
                $lte: end
            }
        })

        .exec(function(err, data) {
            if (err) {
                console.log("err" + err);
            } else {
                console.log("data" + data);
                res.json(data);
            }
        });
    } else {
        //Start Date Setting
        var startday = "01";
        var startmonth = ('0' + (monno)).slice(-2);
        var startyear = parseInt(yr);
        var start = new Date(startyear + "-" + startmonth + "-" + startday + "T00:00:00Z").toISOString();
        //Current Date Setting
        var lastdate = new Date(yr, monno, 0);
        var day = lastdate.getDate();
        var mon = ('0' + (monno)).slice(-2);
        var end = new Date(yr + "-" + mon + "-" + day + "T23:59:59Z").toISOString();
        Order.find({
                "transaction.created": {
                    $gte: start,
                    $lte: end
                }
            })
            .exec(function(err, data) {
                if (err) {
                    console.log("err" + err);
                } else {
                    console.log("data" + data);
                    res.json(data);
                }
            });
    }
};

//Get Quater Sales Report 
exports.getQuartelySales = function(req, res) {
    var qtr = req.params.qtr;
    var year = req.params.yr;
    console.log(qtr);
    console.log(year);
    var d = new Date(); // If no date supplied, use today
    var q = [1, 2, 3, 4];
    var qt = q[Math.floor(d.getMonth() / 3)];
    var month_range = getQtrMonths(qtr);
    console.log(month_range);
    console.log(d.getFullYear());
    if (qt == qtr && d.getFullYear() == year) {
        var curdate = new Date();
        var day = ('0' + (curdate.getDate() - 1)).slice(-2);
        var mon = ('0' + (curdate.getMonth() + 1)).slice(-2)
        var yr = curdate.getFullYear();
        if (mon <= parseInt(month_range.endmonth) && mon >= parseInt(month_range.startmonth)) {
            var startday = parseInt("01");
            var startmonth = ('0' + (parseInt(month_range.startmonth))).slice(-2);
            var startyear = parseInt(year);


            var last = new Date(startyear + "-" + startmonth + "-" + "01").toISOString();
            var cur = new Date(startyear + "-" + mon + "-" + day + "T23:59:59Z").toISOString();
            console.log(cur);
            console.log(last);
            Order.find({
                    "transaction.created": {
                        $gte: last,
                        $lte: cur
                    }
                })
                .exec(function(err, data) {
                    if (err) {
                        console.log("err" + err);
                    } else {
                        console.log("data" + data);
                        res.json(data);
                    }
                });
        }
    } else {
        var lastday = month_range.endate;
        var lastmon = ('0' + (parseInt(month_range.endmonth))).slice(-2);
        var lastyr = year;
        console.log(lastmon);
        var startday = "01";
        var startmonth = ('0' + (parseInt(month_range.startmonth))).slice(-2);
        var startyear = parseInt(year);

        if (lastmon <= parseInt(month_range.endmonth) && startmonth >= parseInt(month_range.startmonth)) {



            var last = new Date(lastyr + "-" + lastmon + "-" + lastday + "T23:59:59Z").toISOString();
            var cur = new Date(startyear + "-" + startmonth + "-" + startday + "T00:00:00Z").toISOString();
            console.log(cur);
            console.log(last);
            Order.find({
                    "transaction.created": {
                        $gte: last,
                        $lte: cur
                    }
                })
                .exec(function(err, data) {
                    if (err) {
                        console.log("err" + err);
                    } else {
                        console.log("data" + data);
                        res.json(data);
                    }
                });
        }


    }




};

//Get Quater Sales Report 
exports.getYearlySales = function(req, res) {
    var year = req.params.yr;
    console.log(year);
    var curdate = new Date();
    var current_year = curdate.getFullYear();
    console.log(current_year == year);
    if (current_year == year) {

        //Current Date Setting

        var day = ('0' + (curdate.getDate() - 1)).slice(-2);
        var mon = ('0' + (curdate.getMonth() + 1)).slice(-2)
        var yr = year;
        var end = new Date(yr + "-" + mon + "-" + day + "T23:59:59Z").toISOString();



        //Start Date Setting
        var startday = "01";
        var startmonth = "01";
        var startyear = parseInt(year);
        var start = new Date(startyear + "-" + startmonth + "-" + startday + "T00:00:00Z").toISOString();


        Order.find({
                "transaction.created": {
                    $gte: start,
                    $lte: end
                }
            })
            .exec(function(err, data) {
                if (err) {
                    console.log("err" + err);
                } else {
                    console.log("data" + data);
                    res.json(data);
                }
            });
    } else {
        console.log(year);
        console.log("Ok Let Me check");
        //Current Date Setting

        var day = parseInt("31");
        var mon = parseInt("12");
        var yr = year;
        var last = new Date(yr + "-" + mon + "-" + day + "T23:59:59Z").toISOString();
        //Start Date Setting
        var startday = "01";
        var startmonth = "01";
        var startyear = parseInt(year);
        var start = new Date(startyear + "-" + startmonth + "-" + startday + "T00:00:00Z").toISOString();

        console.log("last" + last);
        console.log("start" + start);

        Order.find({
                "transaction.created": {
                    $gte: start,
                    $lte: last
                }
            })
            .exec(function(err, data) {
                if (err) {
                    console.log("err" + err);
                } else {
                    console.log("data" + data);
                    res.json(data);
                }
            });
    }
};

exports.getEmailList = function(req, res) {
    User.distinct("email", function(err, data) {
        if (err) {
            console.log(err);
        } else {
            res.json(data);
        }

    });

}

exports.getCardDetail = function(req, res) {
    User.find({
            "email": req.params.em
        }, {
            "cards": 1,
            "_id": 0,
            "email": 1
        })
        .exec(function(err, data) {
            if (err) {
                console.log(err);
            } else {
                res.json(data);
            }
        });

}

exports.getTotalTran = function(req, res) {
    var check = [];
    Order.find({
            "transaction.cardNum": req.params.no
        })
        .exec(function(err, data) {
            if (err) {
                console.log("err" + err);
            } else {
                res.json(data);            }
        });


}



function getQtrMonths(qt) {
    if (qt == 1) {
        return {
            "startmonth": "1",
            "startdate": "01",
            "endmonth": "3",
            "endate": "31"
        };
    }
    if (qt == 2) {
        return {
            "startmonth": "4",
            "startdate": "01",
            "endmonth": "6",
            "endate": "30"
        };
    }
    if (qt == 3) {
        return {
            "startmonth": "7",
            "startdate": "01",
            "endmonth": "9",
            "endate": "30"
        };
    }
    if (qt == 4) {
        return {
            "startmonth": "10",
            "startdate": "01",
            "endmonth": "12",
            "endate": "31"
        };
    }
}

exports.sendMailToClient = function(req, res) {
    /*var reqbody = req.body;
  //
 */ //console.log(req.body);
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var card = req.body.card.cardNumber;
    var email = req.body.email;
    // console.log(card);
    var bal = 0;
    var cashback = 0;
    var cashbackearned = 0;
    var cashbackused = 0;
    var pointsearned = 0;
    var pointsused = 0;

    var date = new Date();

    var startday = "01";
    var startmonth = ('0' + (date.getMonth())).slice(-2);

    var startyear = parseInt(date.getFullYear());
    var start = new Date(startyear + "-" + startmonth + "-" + startday + "T00:00:00Z").toISOString();
    var lastdate = new Date(startyear, startmonth, 0);
    // console.log(lastdate);
    var day = ('0' + (lastdate.getDate())).slice(-2);
    //console.log(day);
    var mon = ('0' + (lastdate.getMonth() + 1)).slice(-2);
    //console.log(mon);
    var end = new Date(startyear + "-" + mon + "-" + day + "T23:59:59Z").toISOString();
    var i = 1;
    /* console.log(start);
     console.log(end);*/
    Order.find({
            $and: [{
                "transaction.cardNum": card
            }, {
                "transaction.created": {
                    $gte: start,
                    $lte: end
                }
            }]
        })
        .exec(function(err, data) {
            if (err) {
                console.log("err" + err);
            } else {
                console.log(card + "->>>>" + data.length);

                for (var i = 0; i < data.length; i++) {
                    console.log("hell");
                    var len = data[i].transaction.length;
                    console.log(len);
                    for (var j = 0; j < len; j++) {

                        if (data[i].transaction[j].cardType === "cashback" && data[i].transaction[j].type === "USED") {
                            console.log("cashback");
                            cashbackused += data[i].transaction[j].amount;
                            console.log("Cash Back Used->" + card + cashbackused);
                        }

                        if (data[i].transaction[j].cardType === "cashback" && data[i].transaction[j].type === "EARNED") {
                            // console.log(card+data[i].transaction[j].amount);
                            cashbackearned += data[i].transaction[j].amount;
                            console.log("Cash Back Earned->" + card + cashbackearned);
                        }
                        if (data[i].transaction[j].cardType === "points" && data[i].transaction[j].type === "USED") {
                            // console.log(card+data[i].transaction[j].amount);
                            pointsused += data[i].transaction[j].amount;
                            console.log("Points Used->" + card + pointsused);
                        }
                        if (data[i].transaction[j].cardType === "points" && data[i].transaction[j].type === "EARNED") {
                            console.log(card + data[i].transaction[j].amount);
                            pointsearned += data[i].transaction[j].amount;
                            console.log("Points Earned->" + card + pointsearned);
                        }
                        if (data[i].transaction[j].cardType === "advantage" && data[i].transaction[j].type === "USED") {
                            console.log(card + data[i].transaction[j].amount);
                            bal += data[i].transaction[j].amount;
                            console.log("Advantage->" + card + bal);
                        }
                        console.log(data[i].transaction[j].cardType == "gift");
                        console.log(data[i].transaction[j].type);
                        if (data[i].transaction[j].cardType == "gift" && data[i].transaction[j].type == "USED" || data[i].transaction[j].type == "AG-USED") {
                            console.log(card + data[i].transaction[j].amount);
                            bal += data[i].transaction[j].amount;
                            console.log("Gift->" + card + bal);
                        }

                    }



                }



                if (cashbackearned || cashbackused || pointsused || pointsearned || bal) {
                    console.log("hello");
                    var dat = new Date();
                    var month = monthNames[dat.getMonth() - 1] + "-" + dat.getFullYear();




                    async.waterfall([

                        function(done) {

                            // var text = reqData.tempText.replace(/\n/, '<br>');
                            //console.log(text);
                            res.render('templates/send-email-to-client', {
                                bal: bal,
                                cashbackearned: cashbackearned,
                                cashbackused: cashbackused,
                                pointsearned: pointsearned,
                                pointsused: pointsused,
                                email: email,
                                monthname: month,
                                cardnumber: card,
                                siteurl: config.url
                            }, function(err, emailHTML) {
                                done(err, emailHTML);
                            });
                        },
                        // If valid email, send reset email using service
                        function(emailHTML, done) {


                            var mailOptions = {
                                to: email,
                                from: "ankitmca2009_12@yahoo.in",
                                subject: 'Message',
                                html: emailHTML
                            };
                            smtpTransport.sendMail(mailOptions, function(err, data) {

                                done(err);
                            });
                        }
                    ], function(err) {
                        if (err) console.log(err);

                        else {
                            return res.send('complete');
                        }


                        //
                    });


                }
            }




        });
}




exports.getUserDetail = function(req, res) {
    console.log("ObjectId(" + req.params.id + ")");
    User.findOne({
        "_id": req.params.id
    }).exec(function(err, data) {
        if (err) {
            console.log(err);
        } else {
            console.log("hi");
            res.json(data);
        }

    });

}