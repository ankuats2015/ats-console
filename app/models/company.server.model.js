var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var CompanySchema = new Schema({
    name: {
        type:String,
        required : 'Company Name is required',
    },
    code:{
        type: String,
        default: ''
    },
    addressline1:{
        type:String,
    },
    addressline2:{
        type:String,
    },
    addressline3:{
        type:String,
    },
    city:{
        type:String,
    },
    state:{
        type:String,
    },
    zipcode:{
        type:Number,
    }, 
    contactphone:{
        type: Number,
        required: 'Phone number is required'
    },
    contactext:{
            type:Number,

        },
    faxphone : {
        type: Number,
        required: 'Phone number is required'
    },
    faxext:{
            type:Number,

        },
    email: {
        type : String,
        required : 'Email is required'
    },
    aboutus:{
        type:String,
    },
    clients:{
        type:String,
    },
    faq:{
        type:String,
    },
    howtoget:{
        type:String,
    },
    dic_getcard:{
        type:String,
    },
    dic_security:{
        type:String,
    },
    dic_cust_bu_not:{
        type:String,
    },
    dic_acc_prof:{
        type:String,
    },
    dic_cust_expe:{
        type:String,
    },
    t_c_cust_reg:{
        type:String,
    },
    t_c_bus_reg:{
        type:String,
    },
    status:{
        type:String,
  
    },
    created: {
        type: Date,
        default: Date.now
    },
    aboutusbgcolor:{
        type:String,
    },
    clientsbgcolor:{
        type:String,
    },
    faqbgcolor:{
        type:String,
    },
    howtogetbgcolor:{
        type:String,
    },
    dic_getcard_bgcolor:{
        type:String,
    },
    dic_security_bgcolor:{
        type:String,
    },
    dic_cust_bu_not_bgcolor:{
        type:String,
    },
    dic_acc_prof_bgcolor:{
        type:String,
    },
    dic_cust_expe_bgcolor:{
        type:String,
    },
    t_c_cust_reg_bgcolor:{
        type:String,
    },
    t_c_bus_reg_bgcolor:{
        type:String,
    },
    status_bgcolor:{
        type:String,
  
    }
});

mongoose.model('Company', CompanySchema);