var departments = require('../../app/controllers/department.server.controller');
module.exports = function(app) {
    app.route('/departments').post(departments.create)
    					.get(departments.list);

    app.route('/departments/:departmentId').get(departments.read)
    						   .put(departments.update)
    						   .delete(departments.delete);


    app.route('/departments/add-card').post(departments.addCards);		
	app.route('/department/search').get(departments.searchDepartment);			
  //app.route('/department/search').get(departments.searchCities);      	   

	app.param('departmentId', departments.departmentByID);



	app.route('/department/:accessCode').get(departments.read)
                               .put(departments.update)
                               .delete(departments.delete);

    app.param('accessCode', departments.departmentByAccessCode);

};