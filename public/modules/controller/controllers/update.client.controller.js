var controler = angular.module('controller');
controler.controller('updateClientController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', 'applicationService', 'Upload', '$sce', '$interval',
    function($rootScope, $scope, $http, $location, Authentication, myService, applicationService, Upload, $sce, $interval) {
        $scope.user = Authentication.user;
        // If user is not signed in then redirect back home
        if (!$scope.user || $scope.user.userType != 'controller') $location.path('/');
        $scope.date = {};
        $scope.view = {};
        $scope.search = {};
        $scope.dptnu = "";
        $scope.Data = [];
        $scope.search1 = {};
        $scope.advantagecard = {};
        $scope.formdata = {};
        $scope.checkImage = true;
        $scope.checkFile = true;
        $scope.historydata = {};
        $scope.historyuser = {};
        $scope.historydata.user = {};
        $scope.showdata = {};
        $scope.changevaluesdata = {};
        $scope.changevaluesdata.before = [];
        $scope.changevaluesdata.after = [];
        $scope.content = "";
        $scope.showdata.data = [];
        $scope.card = {};
        $scope.showdata_user = {};
        $scope.showdata_depart = {};
        $scope.showdata_depart.data = {};
        $scope.showdata_user.data = {};
        $scope.updateclienttab = true;
        $scope.processor_notes = "";
        $scope.primarydetails = true;
        var olddata = {};
        var newdata = {};
        $scope.addressdetails = true;
        $scope.emaildetails = true;
        $scope.phonedetails = true;
        $scope.carddetails = true;
        $scope.disclaimerdetails = true;
        $scope.logodetails = true;
        $scope.processordetails = true;
        $scope.miscdetails = true;
        $scope.statusdetails = true;
        $scope.accessdetails = true;
        $scope.card.giftcards = {};

        $scope.card.advantagecard = {};
        $scope.card.advantagecard.data = [];
        $scope.card.cashbackcard = {};
        $scope.card.pointcard = {};

        $scope.update = {};
        $scope.update.departdata = {};
        $scope.update.carddata = {};
        $scope.update.userdata = {};
        $scope.logoname = "";
        $scope.uploadata = [];
        $scope.dptnu = $scope.user.dptnu;
        $scope.filenames = [];
        $scope.getInit = function() {
            $scope.searchresults = true;
            $scope.title = ["This consumer card can be purchased for a dollar value no more than $1000.00. The dollar value of the card decreases as and when it is used. This card can be gifted to anyone.", "This consumer card adds a bonus amount to the card from the actual purchase price of the card. The dollar value of the card decreases as and when it is used.", "This consumer card adds certain cash back amount to the card for purchases made on and above certain amount. This cash back amount can be used for future purchases. When purchased item(s) are returned, Cashback amount for returns will be deducted/adjusted accordingly.", "This consumer card gives you certain points for purchases made on and above certain amount. Defined number of accumulated points can be used for future purchases. When purchased item(s) are returned, Points for returns will be deducted/adjusted accordingly."];
            $scope.updatepage = false;
            $scope.advantage = true;
            $scope.searchclient = true;
            if ($scope.user.userRole) {
                checkUserType($scope.user.userRole);
            } else {
                checkUserType($scope.user.roles[0]);
            }
            $scope.clientviewpage = false;
            $scope.block1view = true;
            $scope.block2view = false;
            $scope.giftcards_img = ['ISSGC1.jpg', 'ISSGC2.jpg', 'ISSGC3.jpg', 'ISSGC4.jpg', 'ISSGC5.jpg'];
            $scope.advantages = ['ISSAC1.jpg', 'ISSAC2.jpg', 'ISSAC3.jpg', 'ISSAC4.jpg', 'ISSAC5.jpg'];
            $scope.point = ['ISSMPC1.jpg', 'ISSMPC2.jpg', 'ISSMPC3.jpg', 'ISSMPC4.jpg', 'ISSMPC5.jpg'];
            $scope.cashbacks = ['ISSCBC1.jpg', 'ISSCBC2.jpg', 'ISSCBC3.jpg', 'ISSCBC4.jpg', 'ISSCBC5.jpg'];
            var username = $scope.user.username;
            //  console.log(username)
            $http.get('/controller/getHistoryData/' + username).success(function(data) {
                //console.log(data);
            })
        }
        $scope.addcard = function($event) {
            $event.preventDefault();
            $scope.card.advantagecard.data.push({
                get: "",
                buy: ""
            });
            // $rootScope.alerts.push({type: 'success', msg: 'Profile Details updated!'});
        }
        $scope.removeProduct = function($event, name) {
            $event.preventDefault();
            $scope.carddetails = false;
            if ($scope.card.advantagecard.data.indexOf(name) !== -1) $scope.card.advantagecard.data.splice($scope.card.advantagecard.data.indexOf(name), 1);
            else alert('Something went wrong, Unable to remove recepent.');
        };
        $scope.showUpdate = function() {
            $scope.searchresults = false;
            $scope.searchclient = false;
            $scope.updatepage = true;
            $scope.clientviewpage = false;
        }
        $scope.fillPer = function(val) {
            $scope.xvalue = val;
        }
        $scope.clientView = function($event,uniquecode,dptnu) {
            $scope.dptnu = dptnu;
            var stop;
            $event.preventDefault();
            $scope.searchresults = false;
            $scope.searchclient = false;
            $scope.updatepage = false;
            $scope.clientviewpage = true;
            $scope.date.effective = '10/9/2015';
            $scope.date.end = '12/31/2199';
            $scope.date.update = '10/9/2015';
            $scope.view.bunitname = "New Spice And Sweet";
            $scope.view.taxid = "IN417898";
            $scope.view.ownername = "Ankit";
            $scope.view.category = "Kids";
            $scope.view.addressline1 = "Type Unit:1052";
            $scope.view.addressline2 = "B I Spaze Park";

            //console.log(dptnu);
            applicationService.category.query().$promise.then(function(catData) {
                $scope.categories = catData;
                $scope.searchBy = "direct";
            });
            var username = $scope.user.username;
            var tag="";
            // console.log($scope.update.userdata)
            applicationService.state.query().$promise.then(function(states) {
                $scope.formdata.states = states;
            }, function(error) {
                alert('Unable to get state list');
                //console.log(error)
            });
            if(!isEmpty(uniquecode))
            { 
                console.log("jndsj")
                tag={"tag":"uniquecode","uniquecode":uniquecode};
              $http.post('/updateclient/getDepartment/',tag).success(function(department) {
                //console.log(department);
                $scope.update.departdata = department;
                $scope.update.departdata.ownerNames = [];
                var ownername = department.ownerName.split(',');
                //console.log(ownername);
                angular.forEach(ownername, function(val, key) {
                    $scope.update.departdata.ownerNames.push({
                        "name": val
                    }); //ownername
                })
                $scope.update.departdata.disclaimer_note = $sce.trustAsHtml(department.disclaimer);
                angular.forEach(department.processor_notes, function(val, key) {
                        val.notes = $sce.trustAsHtml(val.notes);
                    })
                    //   $scope.update.departdata.ownerName=ownername;
                $scope.logoname = department.logo;
                if (department.onlyCheckoutAccess) {
                    $scope.checkout = "Consumer Card without Billing & NO Credit Card Processing";
                } else {
                    $scope.checkout = "Consumer Card with Billing & NO Credit Card Processing";
                }
            });
            $http.post('/updateclient/getCard/',tag).success(function(card) {
                // console.log(card);
                $scope.update.carddata = card;
                angular.forEach(card, function(val, key) {
                    switch (val.type) {
                        case 'gift':
                            $scope.giftcards = val;
                            $scope.card.giftcards = val;
                            $scope.giftcard = false;
                            break;
                        case 'advantage':
                            $scope.advantagecard = val;
                            $scope.card.advantagecard = val;
                            $scope.advantagecard.data = [];
                            $scope.card.advantagecard.data = [];
                            // angular.forEach(val.amountList, function(val, key)
                            // {
                            //   var arr = val.text.split('$');
                            //   console.log()
                            //   $scope.advantagecard.data.push(
                            //   {
                            //     "get": parseInt(arr[1].replace(/[^\d.]/g, '')),
                            //     "buy": parseInt(arr[2])
                            //   })
                            // })
                            angular.forEach(val.amountList, function(val, key) {
                                // console.log("hi")
                                var arr = val.text.split('$');
                                // console.log(parseInt(arr[1].replace(/[^\d.]/g, '')))
                                $scope.card.advantagecard.data.push({
                                    "get": parseInt(arr[1].replace(/[^\d.]/g, '')),
                                    "buy": parseInt(arr[2])
                                })
                            })
                            $scope.advantagedata = $scope.card.advantagecard.data;
                            $scope.advantage = false;
                            break;
                        case 'cashback':
                            $scope.cashbackcard = val;
                            $scope.card.cashbackcard = val;
                            $scope.cashback = false;
                            break;
                        case 'points':
                            $scope.pointcard = val;
                            $scope.card.pointcard = val;
                            $scope.points = false;
                            break;
                    }
                })
            });
            $http.post('/updateclient/getUserdata/',tag).success(function(user) {
                // console.log(user);
                $scope.update.userdata = user;
                $scope.update.userdata.categary = {
                    "name": user.categary
                };
                $scope.update.userdata.state = {
                    "name": user.state
                };
                //console.log();
                var eff_date = new Date(user.eff_date);
                $scope.update.userdata.eff_date = ('0' + (eff_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (eff_date.getDate())).slice(-2) + "/" + eff_date.getFullYear();
                var end_date = new Date(user.end_date);
                $scope.update.userdata.end_date = ('0' + (end_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (end_date.getDate())).slice(-2) + "/" + end_date.getFullYear();
                // $scope.update.userdata.eff_date=
                // $scope.update.userdata.end_date=new Date(user.end_date);
                // $scope.update.userdata.upd_date=new Date(user.upd_date);
                /*   var eff_date=user.eff_date.split('T');
                   var end_date=user.end_date.split('T');
                   var upd_date=user.upd_date.split('T');
                   var eff_date=new Date();
                     
                   $scope.update.userdata.eff_date=('0' + (eff_date.getMonth()+1)).slice(-2) +"/"+('0' + (eff_date.getDate())).slice(-2)+"/"+eff_date.getFullYear();
                  console.log($scope.update.userdata.eff_date);
                  $scope.update.userdata.end_date=new Date(end_date[0]);
                   $scope.update.userdata.upd_date=new Date(upd_date[0]);
                   console.log($scope.update.userdata);*/
                var username = $scope.user.username;
                $http.get('/controller/getHistoryData/' + username + "/" + $scope.update.userdata.username).success(function(data) {
                    console.log(data);
                    $scope.historydata=data;
                    var name="Ankit";
                    var demo="gupta";
                    var string="<b>"+name+"</b>:<span>"+demo+"</span>";
                    $scope.data=$sce.trustAsHtml(string);

                    console.log($scope.data)

                    angular.forEach($scope.historydata,function(val,key)
                    {
                        angular.forEach(val.after,function(v,k)
                        {
                            //$scope.historydata.after.push($sce.trustAsHtml(v));
                            console.log(v)
                            v=$sce.trustAsHtml(v);
                            console.log(v);


                        })
                        angular.forEach(val.before,function(vs,k)
                        {   vs=$sce.trustAsHtml(vs.toString());
                            console.log(vs);
                        })
                        
                    })
                    console.log($scope.historydata)
                    //$scope.historydata = data;
                    // $scope.historyuser = data.userdata;
                })
            });  
            }
            else
            {
                tag={"tag":"dptnu","dptnu":dptnu};
                console.log(tag)
                $http.post('/updateclient/getDepartment/',tag).success(function(department) {
                //console.log(department);
                $scope.update.departdata = department;
                $scope.update.departdata.ownerNames = [];
                var ownername = department.ownerName.split(',');
                //console.log(ownername);
                angular.forEach(ownername, function(val, key) {
                    $scope.update.departdata.ownerNames.push({
                        "name": val
                    }); //ownername
                })
                $scope.update.departdata.disclaimer_note = $sce.trustAsHtml(department.disclaimer);
                angular.forEach(department.processor_notes, function(val, key) {
                        val.notes = $sce.trustAsHtml(val.notes);
                    })
                    //   $scope.update.departdata.ownerName=ownername;
                $scope.logoname = department.logo;
                if (department.onlyCheckoutAccess) {
                    $scope.checkout = "Consumer Card without Billing & NO Credit Card Processing";
                } else {
                    $scope.checkout = "Consumer Card with Billing & NO Credit Card Processing";
                }
            });
            $http.post('/updateclient/getCard/',tag).success(function(card) {
                // console.log(card);
                $scope.update.carddata = card;
                angular.forEach(card, function(val, key) {
                    switch (val.type) {
                        case 'gift':
                            $scope.giftcards = val;
                            $scope.card.giftcards = val;
                            $scope.giftcard = false;
                            break;
                        case 'advantage':
                            $scope.advantagecard = val;
                            $scope.card.advantagecard = val;
                            $scope.advantagecard.data = [];
                            $scope.card.advantagecard.data = [];
                            // angular.forEach(val.amountList, function(val, key)
                            // {
                            //   var arr = val.text.split('$');
                            //   console.log()
                            //   $scope.advantagecard.data.push(
                            //   {
                            //     "get": parseInt(arr[1].replace(/[^\d.]/g, '')),
                            //     "buy": parseInt(arr[2])
                            //   })
                            // })
                            angular.forEach(val.amountList, function(val, key) {
                                // console.log("hi")
                                var arr = val.text.split('$');
                                // console.log(parseInt(arr[1].replace(/[^\d.]/g, '')))
                                $scope.card.advantagecard.data.push({
                                    "get": parseInt(arr[1].replace(/[^\d.]/g, '')),
                                    "buy": parseInt(arr[2])
                                })
                            })
                            $scope.advantagedata = $scope.card.advantagecard.data;
                            $scope.advantage = false;
                            break;
                        case 'cashback':
                            $scope.cashbackcard = val;
                            $scope.card.cashbackcard = val;
                            $scope.cashback = false;
                            break;
                        case 'points':
                            $scope.pointcard = val;
                            $scope.card.pointcard = val;
                            $scope.points = false;
                            break;
                    }
                })
            });
            $http.post('/updateclient/getUserdata/',tag).success(function(user) {
                // console.log(user);
                $scope.update.userdata = user;
                $scope.update.userdata.categary = {
                    "name": user.categary
                };
                $scope.update.userdata.state = {
                    "name": user.state
                };
                //console.log();
                var eff_date = new Date(user.eff_date);
                $scope.update.userdata.eff_date = ('0' + (eff_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (eff_date.getDate())).slice(-2) + "/" + eff_date.getFullYear();
                var end_date = new Date(user.end_date);
                $scope.update.userdata.end_date = ('0' + (end_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (end_date.getDate())).slice(-2) + "/" + end_date.getFullYear();
                // $scope.update.userdata.eff_date=
                // $scope.update.userdata.end_date=new Date(user.end_date);
                // $scope.update.userdata.upd_date=new Date(user.upd_date);
                /*   var eff_date=user.eff_date.split('T');
                   var end_date=user.end_date.split('T');
                   var upd_date=user.upd_date.split('T');
                   var eff_date=new Date();
                     
                   $scope.update.userdata.eff_date=('0' + (eff_date.getMonth()+1)).slice(-2) +"/"+('0' + (eff_date.getDate())).slice(-2)+"/"+eff_date.getFullYear();
                  console.log($scope.update.userdata.eff_date);
                  $scope.update.userdata.end_date=new Date(end_date[0]);
                   $scope.update.userdata.upd_date=new Date(upd_date[0]);
                   console.log($scope.update.userdata);*/
                var username = $scope.user.username;
                $http.get('/controller/getHistoryData/' + username + "/" + $scope.update.userdata.username).success(function(data) {
                    console.log(data);
                    $scope.historydata=data;
                    $scope.data=$sce.trustAsHtml("<b>Business Unit Name</b>:<span>Demo Test Ats</span>");

                    console.log($scope.data)

                    angular.forEach($scope.historydata,function(val,key)
                    {
                        angular.forEach(val.after,function(v,k)
                        {
                            //$scope.historydata.after.push($sce.trustAsHtml(v));
                            console.log(typeof(v))
                            v=$sce.trustAsHtml(v.toString());
                            console.log(v);


                        })
                        angular.forEach(val.before,function(vs,k)
                        {   vs=$sce.trustAsHtml(vs.toString());
                            console.log(vs);
                        })
                        
                    })
                    console.log($scope.historydata)
                    //$scope.historydata = data;
                    // $scope.historyuser = data.userdata;
                })
            });
            }
            
        }
        $scope.enableEditor = function(enableFlag) {
            $scope.success = $scope.error = null;
            $scope[enableFlag] = true;
            //console.log($scope.processor_notes)
            $scope.processor_notes = "";
            console.log($scope.card.advantagecard.data)
            if (isEmpty($scope.card.advantagecard.data)) {
                $scope.card.advantagecard.data.push({
                    "get": "",
                    "buy": ""
                });
            }

            //   $scope.update.userdata.end_date = new Date("1/22/2016");
            // olddata.userdata=$scope.update.userdata;
            // olddata.departdata=$scope.update.departdata;
            // olddata.carddata=$scope.update.carddata;
            // olddata.updateby=$scope.user.username;
            // olddata.phone=$scope.update.userdata.phone;
            // olddata.savestatus="before";
            // console.log(olddata);
            // $http.post('/controller/saveHistoryData/', olddata).success(function(data)
            // {
            //   console.log(data);
            // });
        };
        $scope.searchDepartment = function(val) {
            return $http.get('/department/search', {
                params: {
                    word: val
                }
            }).then(function(response) {
                return response.data.map(function(item) {
                    return item;
                });
            });
        };
        $scope.onSearch = function($event) {
            $rootScope.enableLoader = true;
            var string = [];
            var string1 = [];
            //  console.log("$scope.search=", $scope.search);
            angular.forEach($scope.search, function(val, key) {
                // console.log("key=", key);
                //console.log("val=", val);
                switch (key) {
                    case "buname":
                        string.push({
                            "tag": "name",
                            "val": val
                        });
                        break;
                    case "access":
                        string.push({
                            "tag": "accessCode",
                            "val": val
                        });
                        break;
                    case "confirmation":
                        string.push({
                            "tag": "confirmation",
                            "val": val
                        });
                        break;
                    case "ownername":
                        string.push({
                            "tag": "ownerName",
                            "val": val
                        });
                        break;
                    case "status":
                        string.push({
                            "tag": "status",
                            "val": val
                        });
                        break;
                    case "address1":
                        string.push({
                            "tag": "address1",
                            "val": val
                        });
                        break;
                    case "taxid":
                        string.push({
                            "tag": "taxid",
                            "val": val
                        });
                        break;
                }
            });
            console.log(string)
            $http.post('/updateclient/search/', string).success(function(data) {
                $scope.showdata.data = [];
                $rootScope.enableLoader = false;


                //  console.log(data)

                if (!isEmpty(data)) {
                    $scope.errormsg = "";
                    angular.forEach(data, function(value, key) {
                        console.log(value)
                        if (isEmpty(value.accessCode)) {
                            // console.log(value);
                            $scope.showdata.data.push(value);
                            $scope.usertab = false;
                            $scope.departab = true;
                            $scope.nodatatab = false;
                        } else {
                            //console.log(value.userId);
                            $scope.showdata.data.push(value);
                            $scope.usertab = true;
                            $scope.departab = false;
                            $scope.nodatatab = false;
                        }
                    })
                } else {
                    $scope.usertab = false;
                    $scope.departab = false;
                    $scope.errormsg = "No Data Found";
                    $scope.nodatatab = true;
                }
                //console.log("$scope.showdata.data=", $scope.showdata.data[0]);
                // $scope.showdata_depart.data="";
            });
        }
        $scope.getLogoName = function(file) {
            if (file != null) {
                // console.log(file.name);
                $scope.uploadtext = file.name;
                $scope.checkImage = false;
            }
        }
        $scope.getfilename = function(file) {
            if (file != null) {
                //  console.log(file.name);
                $scope.filename = file.name;
                $scope.checkFile = false;
            }
        }

        function randomIntFromInterval(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }
        $scope.uploadLogo = function(file) {
            $rootScope.enableLoader = true;
            var max_size = 2097000000;
            $scope.accessnumber = randomIntFromInterval(10000, 99990);
            //   console.log($scope.accessnumber);
            if (file != null) {
                if (file.size < max_size) {
                    Upload.upload({
                        url: '/upload/url',
                        fields: {
                            'templatename': $scope.uploadtext,
                        },
                        file: file,
                    }).progress(function(evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        //  console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                    }).success(function(data, status, headers, config) {
                        // console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                        data.templatename = $scope.uploadtext;
                        data.id = $scope.user._id;
                        data.accessnumber = $scope.accessnumber;
                        //    console.log(data.fName);
                        $scope.filename = data.fname;
                        $scope.logoname = data.fName;
                        $scope.uploaded = data.fName;
                        var res = $http.post('/uploadImage', data);
                        res.success(function(res, err) {
                            // console.log($scope.uploaded);
                            $scope.successmsg = "Logo Uploaded Successfully";
                            $rootScope.enableLoader = false;
                        });
                        res.error(function(err) {
                            //   console.log(err);
                        });
                    }).error(function(data, status, headers, config) {
                        // console.log('error status: ' + data);
                    })
                } else {
                    $scope.message = "Please Upload  Image greate than 2mb";
                    // $rootScope.alerts.push({
                    //     'msg': "Please Upload  Image greate than 2mb",
                    //     'type': "danger"
                    // });
                }
            } else {
                $scope.message = "Please Upload Valid Image .jpg,.png";
                // $rootScope.alerts.push({
                //     'msg': "Please Upload Valid Image .jpg,.png",
                //     'type': "danger"
                // });
            }
        };
        $scope.uploadFiles = function(file) {
            $rootScope.enableLoader = true;
            var max_size = 2097000000;
            $scope.accessnumber = randomIntFromInterval(10000, 99990);
            if (file != null) {
                if (file.size < max_size) {
                    Upload.upload({
                        url: '/upload/url',
                        fields: {
                            'templatename': $scope.uploadtext,
                            'accessnumber': $scope.accessnumber
                        },
                        file: file,
                    }).progress(function(evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        //console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                    }).success(function(data, status, headers, config) {
                        //          console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                        data.templatename = config.file.name;
                        data.id = $scope.user._id;
                        data.accessnumber = $scope.accessnumber;
                        //        console.log(data.fName);
                        // $scope.misl.push(data.fName);
                        $scope.filenames.push(data.fName);
                        $scope.uploaded = data.fName;
                        var res = $http.post('/uploadImage', data);
                        res.success(function(res, err) {
                            //        console.log($scope.uploaded);
                            /*  $rootScope.alerts.push({
                                    "msg": config.file.name +" Uploaded",
                                    "type": "success"
                                });*/
                            $scope.miscdetails = false;
                            $scope.uploadata.push({
                                "templatename": config.file.name,
                                "filename": $scope.uploaded
                            });
                            // $scope.formdata.filename.push();
                            $scope.successmsg1 = "Attached Successfully";
                            //$scope.sendemail = false;
                            $rootScope.enableLoader = false;
                        });
                        res.error(function(err) {
                            // console.log(err);
                        });
                    }).error(function(data, status, headers, config) {
                        //   console.log('error status: ' + data);
                    })
                } else {
                    $rootScope.alerts.push({
                        'msg': "Please Upload  Image greate than 2mb",
                        'type': "danger"
                    });
                }
            } else {
                $rootScope.alerts.push({
                    'msg': "Please Upload Valid Image .jpg,.png",
                    'type': "danger"
                });
            }
        };
        $scope.disableEditor = function(enableFlag) {
            $scope[enableFlag] = false;
        };
        $scope.CancelButtonDisableEditor = function(enableFlag, dptnu) {
            $rootScope.enableLoader = true;
            // location.reload(true);
            $scope.primarydetails = true;
            $scope.addressdetails = true;
            $scope.emaildetails = true;
            $scope.phonedetails = true;
            $scope.carddetails = true;
            $scope.disclaimerdetails = true;
            $scope.logodetails = true;
            $scope.processordetails = true;
            $scope.miscdetails = true;
            $scope.statusdetails = true;
            $scope.accessdetails = true;
            // Don't start a new fight if we are already fighting
            if (angular.isDefined(stop)) return;

            stop = $interval(function() {
            	var tag={"tag":"dptnu","dptnu":dptnu};
                $http.post('/updateclient/getDepartment/',tag).success(function(department) {
                    //console.log(department);
                    $scope.update.departdata = department;
                    $scope.update.departdata.ownerNames = [];
                    var ownername = department.ownerName.split(',');
                    //console.log(ownername);
                    angular.forEach(ownername, function(val, key) {
                        $scope.update.departdata.ownerNames.push({
                            "name": val
                        }); //ownername
                    })
                    $scope.update.departdata.disclaimer_note = $sce.trustAsHtml(department.disclaimer);
                    angular.forEach(department.processor_notes, function(val, key) {
                            val.notes = $sce.trustAsHtml(val.notes);
                        })
                        //   $scope.update.departdata.ownerName=ownername;
                    $scope.logoname = department.logo;
                });
                $http.post('/updateclient/getCard/',tag).success(function(card) {
                    //console.log(card);
                    $scope.update.carddata = card;
                    angular.forEach(card, function(val, key) {
                        switch (val.type) {
                            case 'gift':
                                $scope.giftcards = val;
                                $scope.giftcard = false;
                                break;
                            case 'advantage':
                                $scope.advantagecard = val;
                                $scope.advantagecard.data = [];
                                angular.forEach(val.amountList, function(val, key) {
                                    var arr = val.text.split('$');
                                    console.log()
                                    $scope.advantagecard.data.push({
                                        "get": parseInt(arr[1].replace(/[^\d.]/g, '')),
                                        "buy": parseInt(arr[2])
                                    })
                                })
                                $scope.advantage = false;
                                break;
                            case 'cashback':
                                $scope.cashbackcard = val;
                                $scope.cashback = false;
                                break;
                            case 'points':
                                $scope.pointcard = val;
                                $scope.points = false;
                                break;
                        }
                    })
                });
                $http.post('/updateclient/getUserdata/',tag).success(function(user) {
                    //console.log(user);
                    $scope.update.userdata = user;
                    $scope.update.userdata.categary = {
                        "name": user.categary
                    };
                    $scope.update.userdata.state = {
                        "name": user.state
                    };
                    //console.log();
                    var eff_date = new Date(user.eff_date);
                    $scope.update.userdata.eff_date = ('0' + (eff_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (eff_date.getDate())).slice(-2) + "/" + eff_date.getFullYear();
                    $scope[enableFlag] = false;
                    $scope.stopProcess();
                    $rootScope.enableLoader = false;
                });

            }, 3000);



        };



        $scope.stopProcess = function() {
            if (angular.isDefined(stop)) {
                $interval.cancel(stop);
                stop = undefined;
            }
        };

        $scope.getTaxRegions = function() {
            // console.log($scope.update.userdata.state);
            $scope.addressdetails = false;
            // $scope.selectOption = false;
            $http.get('/addClient/getTaxRegions/' + $scope.update.userdata.state.abb).success(function(data) {
                // console.log("ndskj" + data);
                $scope.taxregions = data;
            })
        }
        $scope.itemRemove = function($event, filename) {
            var result = $http.get('/controller/itemRemoveDepart/' + filename);
            //console.log("id=",id);
            result.success(function(res) {
                // console.log(res);
                uploadget();
            });
            result.error(function(err) {
                // console.log(err);
            })
        }

        function toTitleCase(str) {
            // console.log(str)
            return str.replace(/(?:^|\s)\w/g, function(match) {
                return match.toUpperCase();
            });
        }
        $scope.$on('$destroy', function() {
            // Make sure that the interval is destroyed too
            $scope.stopProcess();
        });

        $scope.stopProcess();
        $scope.changevalue = function(tag, newval, oldval) {
            console.log(tag)
            console.log(newval);
            console.log(oldval)
            var after = [];
            var before = [];
            var ownername = [];

            console.log($scope.card.advantagecard.data.get)
            console.log($scope.card.advantagecard.data.length)
            console.log()

            switch (tag) {
                case "OwnerName":
                    $http.get('/controllername/getOwnername/' + $scope.dptnu).success(function(data) {
                        console.log(data.ownerName);
                      ///  before.push();
                        angular.forEach(newval, function(val, key) {
                            ownername.push(val.name);
                        })
                      //  after.push();//+"</span>";
                        $scope.changevaluesdata.before.push({"tag":tag,"data":data.ownerName});
                        $scope.changevaluesdata.after.push({"tag":tag,"data":ownername});
                        console.log($scope.changevaluesdata.before)
                        console.log($scope.changevaluesdata.after)
                    })

                    break;
                case "End Date":
                    after.push({"tag":tag,"data":newval});
                     before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Business Unit Name":
                   
                    $scope.changevaluesdata.before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Tax Id":
                    after.push({"tag":tag,"data":newval});
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Categary":
                    after.push({"tag":tag,"data":newval.name}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval.name});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval.name});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval.name});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Address Line 1":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Address Line 2":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Address Line 3":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;

                case "State":
                    after.push({"tag":tag,"data":newval.name}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval.name});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval.name});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval.name});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "State":
                    after.push({"tag":tag,"data": newval.name}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval.name});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval.name});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval.name});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "City":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Zipcode":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Primary Email Id":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Alternative Email Id":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Primary Phone":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Phone Type":
                    after.push({"tag":tag,"data": newval}); //?newval.name:newval;
                   before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Alternate Phone 1":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Phone Type":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Alternate Phone 2":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Gift Card Name":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Advantage Card Name":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Advantage Card Image":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Cashback Card Name":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Cashback Card Image":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "CashBack Percentage":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Cashback Value":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Point Card Name":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Point Card Image":
                    after.push({"tag":tag,"data": newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Earn Point(s) Value":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "For Every Spent Value":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Earn $ Value":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Shopping For Every Points Value":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Access Type":
                    after.push({"tag":tag,"data":newval});//?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Disclaimer Receipt Return Policy":
                    after.push({"tag":tag,"data":newval});//?newval.name:newval;
                    before.push({"tag":tag,"data": oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;
                case "Status":
                    after.push({"tag":tag,"data":newval}); //?newval.name:newval;
                    before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.before.push({"tag":tag,"data":oldval});
                    $scope.changevaluesdata.after.push({"tag":tag,"data":newval});
                    console.log($scope.changevaluesdata.before)
                    console.log($scope.changevaluesdata.after)
                    break;

            }
            console.log($scope.changevaluesdata)

            // // if(tag='End Date')
            // // {

            // // }



            // if($scope.card.advantagecard.data.length > 0)
            // {
            //  console.log($scope.card.advantagecard.data)

            //     // var text = "";
            //     // var text1 = "";
            //     // console.log("njd")

            //     // angular.forEach($scope.advantagedata.data, function(v, k) {
            //     //     text1 = "Get $" + v.get + " card for $" + v.buy;
            //     // });
            //     // before = tag + ":" + text1;
            //     // angular.forEach($scope.card.advantagecard.data, function(val, key) {
            //     //     text1 = "Get $" + val.get + " card for $" + val.buy;
            //     // });

            //     // after = tag + ":" + text;
            // }

            //  if (!isEmpty(newval.name) ) {
            //     after = tag + ":" + newval.name; //?newval.name:newval;
            //     before = tag + ":" + oldval.name; //?oldval.name:oldval;
            // } else {
            //     after = tag + ":" + newval;
            //     before = tag + ":" + oldval;
            // } 
            // //before = tag +":"+oldval.name?oldval.name:oldval;

            // console.log($scope.changevaluesdata.before)
            // console.log($scope.changevaluesdata.after)
        }
        $scope.save = function(enableFlag, fieldChange, data) {
            $rootScope.enableLoader = true;
            var cards = [];
            var giftcards = {};
            var advantagecard = {};
            var cashbackcard = {};
            var pointcard = {};
            // console.log($scope.changevaluesdata)
            // console.log($scope.update.departdata)
            $scope.disableEditor(enableFlag);
            var userdata = "";
            var departdata = "";
            var carddata = [];
            userdata.stateid=[];
            $scope.successmsg = "";
            $scope.update.departdata.disclaimer_note = $sce.trustAsHtml($scope.update.departdata.disclaimer);
            if (!isEmpty(data)) {
                $scope.update.departdata.processor_notes.push({
                    "notes": data,
                    "created": Date.now()
                });
            }
            userdata = $scope.update.userdata;
            //     userdata.state = $scope.update.userdata.state.name ? $scope.update.userdata.state.name : $scope.update.userdata.state;
            
              /*get states */
            applicationService.state.query().$promise.then(function(states) {
                $scope.statesdata = states;
                console.log($scope.update.userdata.state.name!=undefined)
                console.log($scope.update.userdata.state.name!=null)

                if ($scope.update.userdata.state.name!=undefined) {

                angular.forEach($scope.statesdata, function(state, index){
                    console.log(state.name == $scope.update.userdata.state.name)

                    if (state.name == $scope.update.userdata.state.name)
                    {
                        console.log("state.name")
                    console.log(state)
                        $scope.user.newstate = state;
                    userdata.state = state.name;      
                    userdata.stateid=[state._id];
                    } 
                        
                });
            }
            else
            {
                 angular.forEach($scope.statesdata, function(state, index){
                    if (state.name == $scope.update.userdata.state) 
                          console.log("state")
                    console.log(state)
                        userdata.state = state.name;      
                        userdata.stateid=[state._id];
                });
            }

                
                },
                function(error){
                    alert('Unable to get state list');
                    console.log(error);
                });

            // if (!isEmpty($scope.update.userdata.state.name)) {
            //     console.log($scope.update.userdata.state._id)
            //     userdata.state = $scope.update.userdata.state.name;
            //     userdata.states=$scope.update.userdata.state;

            //     userdata.stateid=[userdata.states._id];
            // } else {
            //     userdata.state = $scope.update.userdata.state;
            // }
            userdata.taxregion = $scope.update.userdata.taxregion.TaxRegionName ? $scope.update.userdata.taxregion.TaxRegionName : $scope.update.userdata.taxregion;
            userdata.categary = $scope.update.userdata.categary.name ? $scope.update.userdata.categary.name : $scope.update.userdata.categary;
            departdata = $scope.update.departdata;
            departdata.ownerName = [];
            angular.forEach($scope.update.departdata.ownerNames, function(value, key) {
                departdata.ownerName.push(value.name);
            });
            departdata.miscattach = [];
            $scope.successmsg = "";
            angular.forEach($scope.uploadata, function(value, key) {
                departdata.miscattach.push(value);
            });
            departdata.logo = $scope.logoname;
            //console.log($scope.card.giftcards);
            //console.log($scope.card.advantagecard);
            // console.log($scope.card.cashbackcard);
            // console.log($scope.card.pointcard);
            if (!isEmpty($scope.card.giftcards)) {
                console.log($scope.card.giftcards)
                if (!isEmpty($scope.card.giftcards._id)) {
                    giftcards._id = $scope.card.giftcards._id;
                }

                giftcards.name = toTitleCase($scope.card.giftcards.name);
                giftcards.cardCode = giftcards.name.match(/\b([A-Z])/g).join('');
                giftcards.cardNumUnit = randomIntFromInterval(10, 99);
                giftcards.type = 'gift';
                giftcards.image = $scope.card.giftcards.image;
                giftcards.dptnu = $scope.update.departdata.dptnu;
                carddata.push(giftcards);
            }
            console.log(!isEmpty($scope.card.advantagecard))
            console.log($scope.card.advantagecard.data[0].get)
            console.log($scope.card.advantagecard.data[0].get == null)
            console.log($scope.card.advantagecard.data[0].get == undefined)

            console.log($scope.card.advantagecard.data.length)
            console.log()


            if (!isEmpty($scope.card.advantagecard) && $scope.card.advantagecard.data[0].get > 0) {
                console.log($scope.card.advantagecard)
                if (!isEmpty($scope.card.advantagecard._id)) {
                    advantagecard._id = $scope.card.advantagecard._id;
                }
                advantagecard.name = toTitleCase($scope.card.advantagecard.name);
                advantagecard.cardCode = advantagecard.name.match(/\b([A-Z])/g).join('');
                advantagecard.cardNumUnit = randomIntFromInterval(10, 99);
                advantagecard.type = 'advantage';
                advantagecard.image = $scope.card.advantagecard.image;
                advantagecard.dptnu = $scope.update.departdata.dptnu;
                advantagecard.amountList = [];
                angular.forEach($scope.card.advantagecard.data, function(val, key) {
                    var text = "Get $" + val.get + " card for $" + val.buy;
                    //console.log(text);
                    advantagecard.amountList.push({
                        "price": val.get,
                        "text": text
                    });
                });
                carddata.push(advantagecard);
            }
            if (!isEmpty($scope.card.cashbackcard)) {
                // console.log($scope.card.cashbackcard)
                if (!isEmpty($scope.card.cashbackcard._id)) {
                    cashbackcard._id = $scope.card.cashbackcard._id;
                }
                cashbackcard.name = toTitleCase($scope.card.cashbackcard.name);
                cashbackcard.cardCode = cashbackcard.name.match(/\b([A-Z])/g).join('');
                cashbackcard.cardNumUnit = randomIntFromInterval(10, 99);
                cashbackcard.type = 'cashback';
                cashbackcard.image = $scope.card.cashbackcard.image;
                cashbackcard.dptnu = $scope.update.departdata.dptnu;
                cashbackcard.cashbackper = $scope.card.cashbackcard.cashbackper; //$scope.formdata.card.cashback.percentage;
                cashbackcard.cashbackvalue = $scope.card.cashbackcard.cashbackvalue; //$scope.formdata.card.cashback.earn;
                carddata.push(cashbackcard);
            }
            if (!isEmpty($scope.card.pointcard)) {
                if (!isEmpty($scope.card.pointcard._id)) {
                    pointcard._id = $scope.card.pointcard._id;
                }
                pointcard.name = toTitleCase($scope.card.pointcard.name);
                pointcard.cardCode = pointcard.name.match(/\b([A-Z])/g).join('');
                pointcard.cardNumUnit = randomIntFromInterval(10, 99);
                pointcard.type = 'points';
                pointcard.image = $scope.card.pointcard.image;
                pointcard.dptnu = $scope.update.departdata.dptnu;
                pointcard.earn1 = $scope.card.pointcard.earn1;
                pointcard.earn2 = $scope.card.pointcard.earn2;
                pointcard.point1 = $scope.card.pointcard.point1;
                pointcard.point2 = $scope.card.pointcard.point2;
                carddata.push(pointcard);
            }
            // console.log($scope.update.departdata.onlyCheckoutAccess)
            if ($scope.update.departdata.onlyCheckoutAccess) {
                $scope.content = "Consumer Card without Billing & NO Credit Card Processing";
            } else {
                $scope.content = "Consumer Card withodddut Billing & NO Credit Card Processing";
            }
            newdata.userdata = $scope.update.userdata;
            newdata.departdata = $scope.update.departdata;
            newdata.carddata = $scope.update.carddata;
            newdata.updateby = $scope.user.username;
            newdata.phone = $scope.update.userdata.phone;
            newdata.savestatus = "after";
            // console.log(olddata);
            $scope.changevaluesdata.userid = $scope.update.userdata._id;
            $scope.changevaluesdata.updateby = $scope.user.username;
            $scope.changevaluesdata.username = $scope.update.userdata.username;
            console.log($scope.changevaluesdata)
            $http.post('/controller/saveHistoryData/', $scope.changevaluesdata).success(function(data) {
                console.log(data);
            });
            //carddata = $scope.update.carddata;
            // console.log("userdata=", userdata);
            $scope.card.dptnu = $scope.update.departdata.dptnu;
            carddata.push({
                "dptnu": $scope.update.departdata.dptnu
            })
            console.log("carddata=", userdata);
            var cardupdate = $http.post('/controller/updateCardData/', carddata);
            cardupdate.success(function(data) {
                console.log(data)
                var tag1={"tag":"dptnu","dptnu":$scope.card.dptnu};
                $http.post('/updateclient/getCard/', tag1).success(function(card) {

                    console.log(card);
                    angular.forEach(card, function(value, key) {
                        cards.push(value._id);
                    });
                    departdata.cards = cards

                    console.log(departdata.cards)
                    $http.post('/controller/updateDepartData/', departdata).success(function(data) {
                        userdata.vendorId = data._id;
                        if (!isEmpty(userdata.city)) {
                           userdata.cities = {
            "name": userdata.city,
            "departments": {"depId":userdata.vendorId,"catname":userdata.categary}
          };
                        }

                        $http.post('/controller/updateUserData/', userdata).success(function(data) {

                        });
                        // console.log($scope.update.departdata.onlyCheckoutAccess);
                    });


                });
            })




            // $http.post('/controller/updateUserData/', userdata).success(function(data) {
            //     $http.post('/controller/updateDepartData/', departdata).success(function(data) {
            //         // console.log($scope.update.departdata.onlyCheckoutAccess);
            //     });
            // });
            // angular.forEach($scope.uploadata, function(val, key) {
            //     $scope.update.departdata.misc_attach.push(val);
            // })
            stop = $interval(function() {
            	var tag={"tag":"dptnu","dptnu":$scope.card.dptnu};

                $http.post('/updateclient/getCard/',tag).success(function(card) {
                    console.log(card);
                    $scope.update.carddata = card;
                    angular.forEach(card, function(val, key) {
                        switch (val.type) {
                            case 'gift':
                                $scope.giftcards = val;
                                $scope.card.giftcards = val;
                                $scope.giftcard = false;
                                break;
                            case 'advantage':
                                $scope.advantagecard = val;
                                $scope.card.advantagecard = val;
                                $scope.advantagecard.data = [];
                                $scope.card.advantagecard.data = [];
                                // angular.forEach(val.amountList, function(val, key)
                                // {
                                //   var arr = val.text.split('$');
                                //   console.log()
                                //   $scope.advantagecard.data.push(
                                //   {
                                //     "get": parseInt(arr[1].replace(/[^\d.]/g, '')),
                                //     "buy": parseInt(arr[2])
                                //   })
                                // })
                                angular.forEach(val.amountList, function(val, key) {
                                    // console.log("hi")
                                    var arr = val.text.split('$');
                                    console.log(parseInt(arr[1].replace(/[^\d.]/g, '')))
                                    $scope.card.advantagecard.data.push({
                                        "get": parseInt(arr[1].replace(/[^\d.]/g, '')),
                                        "buy": parseInt(arr[2])
                                    })
                                })
                                $scope.advantage = false;
                                break;
                            case 'cashback':
                                $scope.cashbackcard = val;
                                $scope.card.cashbackcard = val;
                                $scope.cashback = false;
                                break;
                            case 'points':
                                $scope.pointcard = val;
                                $scope.card.pointcard = val;
                                $scope.points = false;
                                break;
                        }
                    })
                    $scope.stopProcess();
                    $rootScope.enableLoader = false;
                });
var tag1={"tag":"dptnu","dptnu":$scope.update.departdata.dptnu};
                $http.post('/updateclient/getDepartment/',tag1).success(function(department) {
                    console.log(department);
                    $scope.update.departdata = department;
                    $scope.update.departdata.ownerNames = [];
                    var ownername = department.ownerName.split(',');
                    //console.log(ownername);
                    angular.forEach(ownername, function(val, key) {
                        $scope.update.departdata.ownerNames.push({
                            "name": val
                        }); //ownername
                    })
                    $scope.update.departdata.disclaimer_note = $sce.trustAsHtml(department.disclaimer);
                    angular.forEach(department.processor_notes, function(val, key) {
                            val.notes = $sce.trustAsHtml(val.notes);
                        })
                        //   $scope.update.departdata.ownerName=ownername;
                    $scope.logoname = department.logo;
                });
                var username = $scope.user.username;
                $http.get('/controller/getHistoryData/' + username + "/" + $scope.update.userdata.username).success(function(data) {
                    console.log(data);
                    $scope.historydata = data;
                    // $scope.historyuser = data.userdata;
                })

            }, 3300);
        };
        $scope.verifyZip = function($event) {
            $event.preventDefault();
            var state = $scope.update.userdata.state.abb;
            var taxregion = $scope.update.userdata.taxregion.TaxRegionName;
            var zipcode = $scope.update.userdata.zipcode;
            $http.get('/controller/checkforzip/' + state + '/' + taxregion + '/' + zipcode).success(function(data) {
                if (data == null) {
                    $scope.staterror = "One or all of selected State, Tax Region, and Zip Code is incorrect";
                    $scope.formdata.user.zipcode = '';
                } else {
                    // console.log(data);
                    $scope.staterror = "";
                }
            });
        }
        $scope.checkdt = function(vf) {
            //console.log(vf)
            angular.forEach($scope.card.advantagecard.data, function(value, key) {

                if (value.get < value.buy) {
                    //console.log("chota hai ");
                    value.msg = "$ value for 'Get' should be greater than $ value for 'Buy'";
                    $scope.getcheck = true;
                } else {
                    value.msg = "";
                    $scope.getcheck = false;
                }
            })
        }

        function checkUserType(utype) {
            //console.log(utype);
            if (utype === 'Super User' || utype === 'superuser') {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = true;
                $scope.companytab = true;
            }
            if (utype === 'Admin') {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = false;
                $scope.companytab = false;
            }
        }
        $scope.addOwnerNames = function($event) {
            $scope.update.departdata.ownerNames.push({
                "name": ""
            });
        }
        $scope.removeName = function($event, name) {
            $event.preventDefault();
            if ($scope.update.departdata.ownerNames.indexOf(name) !== -1) $scope.update.departdata.ownerNames.splice($scope.update.departdata.ownerNames.indexOf(name), 1);
            else alert('Something went wrong, Unable to remove recepent.');
        }
        var hasOwnProperty = Object.prototype.hasOwnProperty;

        function isEmpty(obj) {
            // null and undefined are "empty"
            if (obj == null) return true;
            // Assume if it has a length property with a non-zero value
            // that that property is correct.
            if (obj.length > 0) return false;
            if (obj.length === 0) return true;
            // Otherwise, does it have any properties of its own?
            // Note that this doesn't handle
            // toString and valueOf enumeration bugs in IE < 9
            for (var key in obj) {
                if (hasOwnProperty.call(obj, key)) return false;
            }
            return true;
        }
        $scope.$watch('update.userdata.end_date', function(newval, oldval) {
            if (new Date($scope.update.userdata.end_date) < new Date($scope.update.userdata.eff_date)) {
                //console.log("mlkd");
                $scope.errormsg = "End Date cannot be prior to Effective date";
                $scope.checkForNext = true;
            } else {
                $scope.checkForNext = false;
                $scope.errormsg = "";
            }
        });
        $scope.onCheck = function(value, status) {
            //console.log(value);
            //console.log(status);
            $scope.carddetails = false;
            switch (value) {
                case 'gift':
                    if (status) {
                        $scope.giftcard = false;
                        $scope.giftselected = true;
                        $scope.checkforgift = false;
                        $scope.getcheck = false;
                        $scope.card.giftcards.imagecheck = true;
                    } else {
                        $scope.giftcard = true;
                        $scope.card.giftcards = "";
                        $scope.card.giftcards.status = "remove";
                        $scope.card.giftcards.imagecheck = false;

                    }
                    break;
                case 'advantage':
                    if (status) {
                        $scope.advantage = false;
                        $scope.advselected = true;
                        $scope.checkforadvantage = false;
                        $scope.getcheck = false;
                        $scope.card.advantagecard.imagecheck = true;
                    } else {
                        $scope.advantage = true;
                        $scope.card.advantagecard.name = "";
                        $scope.card.advantagecard.image = "";
                        $scope.card.advantagecard.data = [{
                            "get": "",
                            "buy": ""
                        }];
                        $scope.card.advantagecard.imagecheck = false;

                        $scope.advselected = false;
                        $scope.checkforadvantage = true;
                        $scope.card.advantagecard.status = "remove";
                    }
                    break;
                case 'cashback':
                    if (status) {
                        $scope.cashback = false;
                        $scope.cashselected = true;
                        $scope.checkforcashback = false;
                        $scope.getcheck = false;
                        $scope.card.advantagecard.imagecheck = true;
                    } else {
                        $scope.cashback = true;
                        $scope.card.cashbackcard = "";
                        $scope.cashselected = false;
                        $scope.checkforcashback = true;
                        $scope.card.cashbackcard.status = "remove";
                        $scope.card.cashbackcard.imagecheck = false;
                    }
                    break;
                case 'point':
                    if (status) {
                        $scope.points = false;
                        $scope.pointselected = true;
                        $scope.checkforpoints = false;
                        $scope.getcheck = false;
                        $scope.card.advantagecard.imagecheck = true;
                    } else {
                        $scope.points = true;
                        $scope.card.pointcard = "";
                        $scope.pointselected = false;
                        $scope.checkforpoints = true;
                        $scope.card.pointcard.status = "remove";
                        $scope.card.pointcard.imagecheck = false;
                    }
                    break;
                default:
                    break;
            }
        }
    }
]);