var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StatesSchema = new Schema({
    name: {
        type:String,
        required : 'State Name is required',
    },
    cities: [{
        name  : {
            type : String,
            required : true
        },
        departments :[{
                        depId:{type:Schema.ObjectId,ref: 'Department'},
                        catname:{type:String}

                    }]           

    }],
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('State', StatesSchema);