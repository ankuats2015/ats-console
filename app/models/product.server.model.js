var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var ProductSchema = new Schema({
    name: {
        type:String,
        required : 'Product Name is required',
    },
    category: { type: Schema.ObjectId,
              ref: 'ProductCategory',
              required : 'Product Category is required',
    },
    department : {
        type: Schema.ObjectId,
        ref: 'Department',
        required : 'Department is required',
    },
    code:{
        type: String,
        unique: true,
        required : 'Product Code is required'
    },
    price : {
        my_p: { type: Number, required : 'My price is required' }, /* My price */
        s_p: { type: Number, required : 'Sale price is required' }, /* Sale price */
        d_s_p: { type: Number }, /* Discount sale price */
    },
    lb : {
        type: Boolean,
        default: false
    },
    app_tax : {
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('Product', ProductSchema);