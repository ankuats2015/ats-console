var Order = require('mongoose').model('CardOrder'),
    Department = require('mongoose').model('Department'),
    async = require('async'),
   /* Barc = require('barc'),
    barc = new Barc(),*/
    fs = require('fs');


function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}


function genrateCardNum(card)
{
    var cardNum = randomIntFromInterval(10000000, 99999999)+'3456'+card.cardNumUnit+randomIntFromInterval(10, 99);

    
    Order.find({cards: {$elemMatch: {cardNumber:cardNum}}}, function(err, order) {
       if (!order)
        genrateCardNum(card);
       else
       return cardNum;
    });

}

function genrateBarCode(code)
{
    /* var buf = barc.code128(code, 300, 200);
        fs.writeFile(__dirname + '/../../public/images/barcode/'+code+'.png', buf, function(){
            console.log('wrote it');
        });*/

    return code+'.png';
}



exports.create = function(req, res, next) {

var reqData = req.body;
var orderNumber = 'ISS'+new Date().getTime();

/* Department number unit */
var dptnu = (reqData.department.dptnu) ? reqData.department.dptnu : 3456; 

async.waterfall([
                function(done) {
                        async.each(reqData.cards, function(card, callback){
                            
                            card.cardNumber = randomIntFromInterval(10000000, 99999999)+dptnu.toString()+card.cardNumUnit.toString()+randomIntFromInterval(10, 99);
                            genrateBarCode(card.cardNumber);
                          
                            callback();
                        }, function(err){
                            done(err, reqData.cards);
                        });   
                       
                    },
                    function(cards, done) {
                        var order = new Order();
                            order.orderId = orderNumber;
                            order.cards = cards;
                            order.department = reqData.department._id;
                            order.user = reqData.user;
                            /*saving order */
                            order.save(function(err, sorder) {
                                if (err) {
                                    return next(err);
                                } else {

                                Order.findOne(sorder).populate('cards.cardId').exec(function (err, item) {
                                    done(item);
                                });


                                    //done(order);
                                }
                            });
                    }
                ], function(order, err) {
                     if (err)
                        return console.error(err);
                      else
                      {
                         res.send(order);
                      }

                });

};


exports.list = function(req, res, next) {

    Order.find({}, function(err, orders) {
        if (err) {
            return next(err);
        } else {
            res.json(orders);
        }
    });
};



exports.searchCard = function(req, res, next) {

    if (req.query.number)
    {
        var cardNum = req.query.number;
        var query = {'cards.cardNumber':cardNum, 'cards.useStatus': false};
        var populateQuery = [{path:'cards.cardId', select:'name type'}, {path:'department', select:'name address contact'}];

        Order.findOne(query)
            .populate(populateQuery)
            .exec(function (err, order) {
              if (err) {
                    return next(err);
                } 


              if (order){
                order.cards.forEach(function(card) {
                    if(card.cardNumber == cardNum)
                    {
                       res.json({cardBaseInfo: card.cardId, price: card.amount, cardNumber:card.cardNumber, user: card.user, status: card.useStatus, department: order.department});

                        
                    }
                  });
            }
            else
            {
                return res.status(200).send({
                    error: 'No records found'
                });
            }
            });

    }
    else
    {
        res.status(400).send({
            message: 'invalid request'
        });
       
    }

    
};


