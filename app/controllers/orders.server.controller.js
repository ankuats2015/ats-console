var ProductOrder = require('mongoose').model('Orders'),
    User = require('mongoose').model('User'),
    async = require('async'),
    config = require('../../config/config'),
    nodemailer = require('nodemailer'),
    errorHandler = require('./errors.server.controller');
 /* Barc = require('barc'),
    barc = new Barc(),*/
    fs = require('fs');


/* get order and return order tranactions   */
function getOrderTransaction(order, maincallback)
{
    
    var transaction = [];
   
    if (order.cards.length) {
      
    async.each(order.cards, function(card, callback) {
            
         
            if (card.type == 'cashback')
            {
                
                /* transaction for cashback used */
                if (card.order.cashback.used)
                    transaction.push({cardNum : card.no, cardType : card.type, amount:  card.order.cashback.used, type: 'USED'});

                /* transaction for cashback earned */
                if (card.order.cashback.earned)
                    transaction.push({cardNum : card.no, cardType : card.type, amount:  card.order.cashback.earned, type: 'EARNED'});

                /* transaction for cashback used */
                if (card.order.cashback.lost)
                    transaction.push({cardNum : card.no, cardType : card.type, amount:  card.order.cashback.lost, type: 'LOST'});

                /* transaction for cash paid */
                if(order.type == 'add'){
                    var cashPayed = (card.order.cashback.used) ? card.grandTotal - card.order.cashback.used : card.grandTotal ;

                    /* add transaction if paid amount has values */
                    if (cashPayed)
                        transaction.push({cardNum : card.no, cardType : card.type, amount:  parseFloat(cashPayed).toFixed(2), type: 'CASH'});
                }
                else
                    transaction.push({cardNum : card.no, cardType : card.type, amount:  parseFloat((card.return.grandTotal - card.order.cashback.lost).toFixed(2)), type: 'RETURN-CASH'});

            }
            else if(card.type == 'points'){
                /* transaction for cashback used */
                if (card.order.points.used)
                    transaction.push({cardNum : card.no, cardType : card.type, amount:  card.order.points.used, type: 'USED'});

                /* transaction for cashback earned */
                if (card.order.points.earned)
                    transaction.push({cardNum : card.no, cardType : card.type, amount:  card.order.points.earned, type: 'EARNED'});

                /* transaction for cashback earned */
                if (card.order.points.lost)
                    transaction.push({cardNum : card.no, cardType : card.type, amount:  card.order.points.lost, type: 'LOST'});

                /* transaction for cash paid */
                if(order.type == 'add'){
                    var cashPayed = (card.order.points.used) ? order.grandTotal - card.order.points.used : order.grandTotal;

                    /* add transaction if paid amount has values */
                    if (cashPayed)
                        transaction.push({cardNum : card.no, cardType :card.type, amount:  (card.grandTotal - card.order.points.used), type: 'CASH'});
                }
                else if(order.type == 'return')
                    transaction.push({cardNum : card.no, cardType :card.type, amount:  (card.return.grandTotal), type: 'RETURN-CASH'});
            }
            else if (card.type == 'advantage' || card.type == 'gift'){
                /* pay full payment by card */
                if(order.type == 'add'){

                    if (card.balance > card.grandTotal ) {
                        transaction.push({cardNum : card.no, cardType :card.type, amount:  card.grandTotal, type: 'AG-USED'});
                    }
                    else
                    {
                        /* pay payment by card as well as cash */
                        transaction.push({cardNum : card.no, cardType :card.type, amount:  card.balance, type: 'AG-USED'});
                        transaction.push({cardNum : card.no, cardType :card.type, amount:  parseFloat((card.grandTotal - card.balance).toFixed(2)), type: 'AG-CASH'});
                    }
                }
                else if(order.type == 'return'){
                  transaction.push({cardNum : card.no, cardType :card.type, amount:  parseFloat(card.return.grandTotal), type: 'AG-RETURNTOCARD'});  
                }
            }
            else
            {
                transaction.push({cardType :'no-card', amount:  parseFloat((card.grandTotal).toFixed(2)), type: 'CASH'});
            }

            callback();

        }, function(err){
           
            // if any of the file processing produced an error, err would equal that error
            if( err ) {
              console.log('A card failed to process');
            } else {
              order.transaction = (order.transaction) ? order.transaction.concat(transaction) : transaction;
            }
            return maincallback(null, order);

        });
    }
    else{
            transaction.push({cardType :'no-card', amount:  parseFloat((order.grandTotal).toFixed(2)), type: 'CASH'});
            order.transaction = (order.transaction) ? order.transaction.concat(transaction) : transaction;
            return maincallback(null, order);
    }

   
}

/* get user and updated cards data */
function updateUserCards(order, maincallback)
{
     async.each(order.cards, function(card, callback) {
            
            var updatedCardsFields = {};
            console.log(card.type);
            if (card.type == 'cashback')
            {
                
                if (order.type == 'add') {
                    var balance = (card.cashback.balance + card.order.cashback.earned - card.order.cashback.used);
                    var earned = (card.cashback.earned + card.order.cashback.earned);
                    var used = (card.cashback.used + card.order.cashback.used);
                }
                else if(order.type == 'return')
                {
                    var balance = (card.cashback.balance - card.order.cashback.lost);
                    var earned = (card.cashback.earned - card.order.cashback.lost);
                    var used = (card.cashback.used);
                }
                updatedCardsFields["cards.$.cashback"] = { balance : balance, earned : earned, used : used };
            }

            if (card.type == 'points')
            {
                if (order.type == 'add') {
                    var balance = (card.points.balance + parseInt(card.order.points.earned) - parseInt(card.order.points.used));
                    var earned = (card.points.earned + parseInt(card.order.points.earned));
                    var used = (card.points.used + parseInt(card.order.points.used));
                }
                else if (order.type == 'return') {
                    var balance = (card.points.balance - parseInt(card.order.points.lost));
                    var earned = (card.points.earned - parseInt(card.order.points.lost));
                    var used = (card.points.used - parseInt(card.order.points.lost));
                }
                updatedCardsFields["cards.$.points"] = { balance : balance, earned : earned, used : used };
            }

            if (card.type == 'advantage' || card.type == 'gift')
            {
                var balance = 0.00;
                if (order.type == 'add') {

                    if (card.balance > card.grandTotal) 
                        balance = parseFloat((card.balance - card.grandTotal).toFixed(2));
                }
                else
                    balance = parseFloat((card.balance + card.return.grandTotal).toFixed(2));

                updatedCardsFields["cards.$.balance"] = balance;
            }
         
            if (card.grandTotal) {

            User.findOneAndUpdate(
                    { "_id": card.user, "cards.cardNumber": card.no },
                    { 
                        "$set": updatedCardsFields
                    },
                    function(err,user) {
                        callback();
                    }
                );
            }
            else
            {
                console.log('card type : '+card.type+' is dummy.');
                callback();
            }

             
        }, function(err){
            // if any of the file processing produced an error, err would equal that error
            if( err ) {
              console.log('A card failed to process');
            } else {
               console.log('processed');
               maincallback(null, order);
            }

        });


    
   }

exports.sendOrderMail = function(req, res, next) {  
var order = req.body;
var smtpTransport = nodemailer.createTransport(config.mailer.options);
   // main async.each

if (order.cards && order.cards.length) {
async.each(order.cards, function(card, callback) {
             async.waterfall([
                function(done) {
                    
                    res.render('templates/user/order/order-notification', {
                        order: order,
                        siteurl : config.url
                    }, function(err, emailHTML) {
                        done(err, emailHTML);
                    });
                },
                // If valid email, send email using service
                function(emailHTML, done) {
                    var mailOptions={
                        to : card.email,
                        subject : 'Order Notification',
                        html: emailHTML
                        };
                    smtpTransport.sendMail(mailOptions, function(err) {

                       /* if (!err) {
                           done(err);
                            
                        } else {
                            return false;
                          
                        }*/
                        done(err);
                    });
                }
            ], function(err) {
                if (err){
                    console.log(err);
                    return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                }
                else
                   callback();

            });
             
        }, function(err){
            // if any of the file processing produced an error, err would equal that error
            if( err ) {
              console.log('A card failed to process');
            } else {
              return res.json({'message' : 'Mail sent!'});
            }
           

        });
}
else
{

    async.waterfall([
                function(done) {
                    
                    res.render('templates/user/order/order-notification', {
                        order: order,
                        siteurl : config.url
                    }, function(err, emailHTML) {
                        done(err, emailHTML);
                    });
                },
                // If valid email, send email using service
                function(emailHTML, done) {
                    var mailOptions={
                        to : order.email,
                        subject : 'Order Notification',
                        html: emailHTML
                        };
                    smtpTransport.sendMail(mailOptions, function(err) {

                       /* if (!err) {
                           done(err);
                            
                        } else {
                            return false;
                          
                        }*/
                        done(err);
                    });
                }
            ], function(err) {
                if (err){
                    console.log(err);
                    return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                }
                else
                    return res.json({'message' : 'Mail sent!'});

            });
}


    
   
};


exports.create = function(req, res, next) {  
    var orderBody = req.body;
    async.waterfall([
            function(callback) {           
             /*  var buf = barc.code128(orderBody.barcode, 300, 200);
                fs.writeFile(__dirname + '/../../public/images/barcode/'+orderBody.barcode+'.png', buf, function(){
                    console.log('wrote it');
                    callback();
                });*/
                //callback();                
            },
            function(callback) {           
                getOrderTransaction(orderBody, callback);                
            },
            function(order, callback) {
                var productOrder = new ProductOrder(order, { _id: false });

                productOrder.save(function(err) {

                            if (err) {
                                 return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {
                                 callback(null, productOrder);
                            }
                        });
            },
            function(order, callback) {
                if (orderBody.cards)
                    updateUserCards(orderBody, callback)
                else
                    callback(null, order );
            }
        ], function (err, order) {
            if (err) 
                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
            else
                return res.json(order);
            
        });
};


exports.list = function(req, res, next) {

    ProductOrder.find({}, function(err, productOrders) {
        if (err) {
            return next(err);
        } else {
            res.json(productOrders);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.productOrder);
};

exports.orderByID = function(req, res, next, id) {
    ProductOrder.findOne({
        _id: id
    }, function(err, productOrder) {
        if (err) {
            return next(err);
        } else {
            req.productOrder = productOrder;
            next();
        }
    });
};

exports.update = function(req, res, next) {


    var orderBody = req.body;

    delete orderBody._id;

    async.waterfall([
            function(callback) {
           
                getOrderTransaction(orderBody, callback);
                
            },
            function(order, callback) {
                if (order.cards.length)
                    updateUserCards(orderBody, callback)
                else
                    callback(null, order );
            },
            function(order, callback) {

                order.card = null;
                order.cashBack = null;
                order.points = null;
                order.previousFinal = null;

                ProductOrder.findByIdAndUpdate(req.productOrder.id, order, function(err, productOrder) {
                        console.log(err);
                        if (err) {
                            return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                        } else {
                           callback(null, order);
                        }
                    });
            }
        ], function (err, order) {
            if (err) 
                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
            else
                return res.json(order);
            
        });


    /*ProductOrder.findByIdAndUpdate(req.productOrder.id, req.body, function(err, productOrder) {
        if (err) {
            return next(err);
        } else {
            res.json(productOrder);
        }
    });*/
};

exports.delete = function(req, res, next) {
    req.productOrder.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.productOrder);
        }
    })
};


exports.orderByQuery = function(req, res, next) {
    var query = req.query;
    if (query.phone)
    {
        var today = new Date();
        var priorDate = new Date().setDate(today.getDate()-30);
        query = { 'cards.phone' : query.phone };
    }

    ProductOrder.find( query)
               /* .populate('user')
                .populate('user.cards.cardId')*/
                .exec(function(err, productOrders) {
                    if (err) {
                         return res.status(400).send({
                                                message: errorHandler.getErrorMessage(err)
                                            });
                    } else {
                            res.json(productOrders);                          
                    }
                });
};


exports.getCardTranactions = function(req, res, next) {
    var query = req.query;

    if (!query.number)
        return res.status(400).send({
                                        message: 'Please enter card number'
                                    });
    var currentDate = new Date();
    var m = (query.month) ? parseInt(query.month) : currentDate.getMonth();
    var y = (query.year) ? parseInt(query.year) : currentDate.getFullYear();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);

    var page = (query.page) ? parseInt(query.page) : 0;
    var itemsPerPage = (query.itemsPerPage) ? parseInt(query.itemsPerPage) : 3;

    var skipItems = ((page-1) * itemsPerPage) ;



    ProductOrder.aggregate(
                            [
                                {
                                    "$unwind": "$transaction"
                                },
                                {
                                    $match: {
                                        'transaction.cardNum': query.number,
                                        'transaction.created' : {
                                            $gt: firstDay, 
                                            $lt: lastDay
                                        }
                                    }
                                },
                                {
                                    "$project": {
                                        "transaction": "$transaction",
                                    }
                                },
                                {
                                    $sort: 
                                        {
                                            'transaction.created': -1
                                        }
                                },
                                { $skip : skipItems },
                                { $limit : itemsPerPage }
                            ], function(err, transactions) {
                                if (err) {
                                    console.log(err);
                                     return res.status(400).send({
                                                            message: errorHandler.getErrorMessage(err)
                                                        });
                                } else {
                                        res.json(transactions);                          
                                }

                            }
                        );
};



