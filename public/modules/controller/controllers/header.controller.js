angular.module('controller').controller('headerController', ['$scope', '$routeParams', '$location', 'Authentication',
    function($scope, $routeParams, $location, Authentication) {
      
        $scope.authentication = Authentication;

      	$scope.isActive = function(route) {
        	var locationPAth = $location.path();
        	if (locationPAth.indexOf("/get-ready") !== -1)
        		locationPAth = "/get-ready";
        	
          return route === locationPAth;
      	}
		
      	$scope.status = {
		    isopen: false
		  };

		  $scope.toggled = function(open) {
		    //$log.log('Dropdown is now: ', open);
		  };

		  $scope.toggleDropdown = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();
		    $scope.status.isopen = !$scope.status.isopen;
		  };


    }
]);

