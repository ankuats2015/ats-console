'use strict';

/**
 * Get unique error field name
 */
var getUniqueErrorMessage = function(err) {
	var output;

	try {
		var errmsg = (err.err) ? err.err : err.errmsg;

		var fieldName = errmsg.substring(errmsg.lastIndexOf('.$') + 2, errmsg.lastIndexOf('_1'));
		if (fieldName == 'username') 
			output = "User existing in database. Please change the User Id.";
		else if (fieldName == 'code') 
			output = "Product code already exists.";
		else if (fieldName == 'phone') 
			output = "Phone number already exists.";
	/*	else if (fieldName == 'email') 
			output = "Email existing in database. Please change the Email Id.";*/
		else
			output = fieldName.charAt(0).toUpperCase() + fieldName.slice(1) + ' already exists';

	} catch (ex) {
		console.log(ex);
		output = 'Unique field already exists';
	}

	return output;
};

/**
 * Get the error message from error object
 */
exports.getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = getUniqueErrorMessage(err);
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};