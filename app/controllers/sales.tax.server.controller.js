var Upload = require('mongoose').model('uploadImage');
var Control = require('mongoose').model('Controller');
var Company=require('mongoose').model('Company');
var TaxRegion=require('mongoose').model('TaxRegion');
var State=require('mongoose').model('State');


exports.searchTax = function(req, res, next) {
   var state=req.params.state;
   var taxregion=new RegExp(req.params.tax, 'i');
   var zip=parseInt(req.params.zip);
   var tag=req.params.tag;


  //console.log(state);
  //.log(taxregion);
  //console.log(zip);
  //console.log(tag);
  switch(tag)
  {
    case 'nozip':
     TaxRegion.find({$and : [{State:state},{TaxRegionName:taxregion}]})            
                     .exec(function (err, data) {
                      if (err) {
                        console.log(err);
                                return next(err);
                            } else {
                              //console.log(data);
                               res.json(data);
                                
                            }
                    });

    break;
    case 'all':
    TaxRegion.find({$and : [{State:state}, {ZipCode:zip},{TaxRegionName:taxregion}]})            
                     .exec(function (err, data) {
                      if (err) {
                        console.log(err);
                                return next(err);
                            } else {
                             // console.log(data);
                               res.json(data);
                                
                            }
                    });

    break;
       case 'onlystate':

    TaxRegion.find({State:state})            
                     .exec(function (err, data) {
                      if (err) {
                        console.log(err);
                                return next(err);
                            } else {
                           //   console.log(data);
                               res.json(data);
                                
                            }
                    });

    break;
        case 'statezip':

    TaxRegion.find({$and:[{State:state},{ZipCode:zip}]})            
                     .exec(function (err, data) {
                      if (err) {
                        console.log(err);
                                return next(err);
                            } else {
                            //  console.log(data);
                               res.json(data);
                                
                            }
                    });

    break;
        case 'onlyzip':

    TaxRegion.find({ZipCode:zip}).populate('stateid')            
                     .exec(function (err, data) {
                      if (err) {
                        console.log(err);
                                return next(err);
                            } else {
                             console.log(data);
                               res.json(data);
                                
                            }
                    });

    break;
    default:
    break;
  }
    
};


exports.updateRow=function(req,res)
{
  console.log(req.body.CountyRate);

   TaxRegion.update({ZipCode:req.body.ZipCode},{$set:{CityRate:parseFloat(req.body.CityRate),CombinedRate:parseFloat(req.body.CombinedRate),State:req.body.State,TaxRegionCode:req.body.TaxRegionCode,TaxRegionName:req.body.TaxRegionName,StateRate:parseFloat(req.body.StateRate),CountyRate:req.body.CountyRate,SpecialRate:parseFloat(req.body.SpecialRate),lastupdateby:req.body.lastupdateby,lastupdate:req.body.lastupdate}}).exec(function(err, department) 
    { if (err) {
            return next(err);
        } else {
          console.log("done");
            res.json({"msg":"Updated Data"});
        }
    });
};

exports.getState=function(req,res)
{
  console.log(req.params.stateabb)
  State.findOne({abb:req.params.stateabb}).exec(function(err,data)
  {
    if(err)
    {
      console.log(err)
    }
    else
    {
      console.log(data)
      res.json(data)
    }
  })
}