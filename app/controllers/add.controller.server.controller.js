var Upload = require('mongoose').model('uploadImage');
var Control = require('mongoose').model('Controller');
var Company = require('mongoose').model('Company');
var User =require('mongoose').model('User');
var time = require('time');
exports.create = function(req, res)
{
  console.log(req.body.username);
  Control.find(
  {
    username: req.body.username
  }).exec(function(err, data)
  {
    if (!isEmpty(data))
    {
      res.json(
      {
        "msg": "User Id already exists"
      });
    }
    else
    {
      var cont = new Control(req.body);
      var eff_date = new time.Date(req.body.eff_date);
      var end_date = new time.Date(req.body.end_date);
      console.log(eff_date.toString());
      console.log(end_date.toString());
      eff_date.setTimezone('UTC', true);
      end_date.setTimezone('UTC', true);
      console.log(eff_date.toString());
      console.log(end_date.toString());
      cont.eff_date = eff_date.toString();
      cont.end_date = end_date.toString();
      //console.log(con)
      // console.log(new Date());
      //  console.log(new Date(cont.eff_date).toString());
      //   console.log(new Date(cont.eff_date).toUTCString());
      //   var dt=new time.Date(cont.eff_date);
      //   console.log(dt.toString());
      //   console.log("Default Behaviour");
      //   dt.setTimezone('UTC',true);
      //   console.log(dt.toString());
      //    console.log("------------");
      //    console.log(dt.getTimezone());
      cont.save(function(err, control)
      {
        if (err)
        {
          return err;
        }
        else
        {
          return res.json(
          {
            "msg": "Controller Successfully Created"
          });
        }
      });
    }
  });
}
exports.postData = function(req, res)
{
  console.log(req.body);
  var cont = new Company(req.body);
  cont.save(function(err, control)
  {
    if (err)
    {
      //var message = errorHandler.getErrorMessage(err);
      console.log(err);
      return err;
    }
    else
    {
      return res.json(
      {
        "msg": "Company Data Save"
      });
    }
  });
}
exports.getData = function(req, res)
{
  Company.findOne(
  {
    status: "true"
  }).exec(function(err, data)
  {
    if (err) return err;
    else
    {
      res.json(data);
    }
  });
};
exports.SearchUsername = function(req, res, next)
{
  var regex = new RegExp(req.query.cont, 'i'); // 'i' makes it case insensitive
  var effdate = req.query.date1;
  var enddate = req.query.date2;
  var tag = req.query.tag;
  var query = {
    $and: []
  };
  console.log(req.body.length);
  for (var i = 0; i < req.body.length; i++)
  {
    switch (req.body[i].tag)
    {
      case 'eff_date':
        var temp = {};
        temp[req.body[i].tag] = req.body[i].val;
        query.$and.push(temp);
        break;
      case 'end_date':
        var temp = {};
        temp[req.body[i].tag] = req.body[i].val;
        query.$and.push(temp);
        break;
      case 'controllername':
        var temp = {};
        var name = new RegExp(req.body[i].val, 'i');
        temp[req.body[i].tag] = name;
        query.$and.push(temp);
        break;
    }
  }
  if (req.body.length == 2)
  {
    var val1 = "";
    var val2 = "";
    for (var i = 0; i < req.body.length; i++)
    {
      switch (req.body[i].tag)
      {
        case 'eff_date':
          var temp = {};
          val1 = req.body[i].val;
          temp[req.body[i].tag] = req.body[i].val;
          query.$and.push(temp);
          break;
        case 'end_date':
          var temp = {};
          val2 = req.body[i].val;
          temp[req.body[i].tag] = req.body[i].val;
          query.$and.push(temp);
          break;
        case 'controllername':
          var temp = {};
          var name = new RegExp(req.body[i].val, 'i');
          temp[req.body[i].tag] = name;
          query.$and.push(temp);
          break;
      }
    }
    if (val1 != undefined && val2 != undefined)
    {
      Control.find(
      {
        $and: [
        {
          eff_date:
          {
            $gte: val1
          }
        },
        {
          end_date:
          {
            $lte: val2
          }
        }]
      }).exec(function(err, data)
      {
        if (err)
        {
          return next(err);
        }
        else
        {
          console.log(data);
          res.json(data);
        }
      });
    }
    else
    {
      Control.find(query).exec(function(err, data)
      {
        if (err)
        {
          return next(err);
        }
        else
        {
          console.log(data);
          res.json(data);
        }
      })
    }
  }
  else
  {
    Control.find(query).exec(function(err, data)
    {
      if (err)
      {
        return next(err);
      }
      else
      {
        console.log(data);
        res.json(data);
      }
    })
  }
  /*   */
};
exports.updateRow = function(req, res)
{
  console.log(req.body);
  Control.update(
  {
    username: req.body.username
  },
  {
    $set:
    {
      userRole: req.body.usertype,
      last_date: req.body.lastupdate,
      last_update_by: req.body.lastupdateby,
      end_date: req.body.enddate
    }
  }).exec(function(err, department)
  {
    if (err)
    {
      return next(err);
    }
    else
    {
      console.log(department);
      res.json(department);
    }
  });
};
exports.updateData = function(req, res)
{
  console.log(req.body);
  console.log(req.body.tag);
  var tag = req.body.tag;
  var data = req.body.data;
  var comp = req.body.comp;
  switch (tag)
  {
    case 'aboutus':
      console.log(tag);
      console.log(req.body.bgcolor);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          aboutus: data,
          aboutusbgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'clients':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          clients: data,
          clientsbgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'faq':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          faq: data,
          faqbgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'howtoget':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          howtoget: data,
          howtogetbgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'dic_getcard':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          dic_getcard: data,
          dic_getcard_bgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'dic_security':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          dic_security: data,
          dic_security_bgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'dic_cust_bu_not':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          dic_cust_bu_not: data,
          dic_cust_bu_not_bgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'dic_acc_prof':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          dic_acc_prof: data,
          dic_acc_prof_bgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 'dic_cust_expe':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          dic_cust_expe: data,
          dic_cust_expe_bgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 't_c_cust_reg':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          t_c_cust_reg: data,
          t_c_cust_reg_bgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    case 't_c_bus_reg':
      console.log(tag);
      Company.update(
      {
        name: comp
      },
      {
        $set:
        {
          t_c_bus_reg: data,
          t_c_bus_reg_bgcolor: req.body.bgcolor
        }
      }).exec(function(err, result)
      {
        if (err) console.log(err);
        else
        {
          console.log(result);
          res.json(
          {
            "msg": "Update Successfully"
          });
        }
      });
      break;
    default:
      break;
  } //swicth end
};
exports.search = function(req, res)
{
  //console.log(req.params.username);
  var date1 = req.params.date1;
  var date2 = req.params.date2;
  var cont = req.params.cont;
  var tag = req.params.tag;
  switch (tag)
  {
    case 1:
      Control.find(
      {
        $or: [
        {
          "username": req.params.username
        },
        {
          "eff_date": req.params.eff_date
        },
        {
          "end_date": req.params.end_date
        }]
      }).exec(function(err, data)
      {
        console.log(data);
      });
  }
};

function isEmpty(obj)
{
  // null and undefined are "empty"
  if (obj == null) return true;
  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length && obj.length > 0) return false;
  if (obj.length === 0) return true;
  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and toValue enumeration bugs in IE < 9
  for (var key in obj)
  {
    if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}
/**
 * Change Password
 */
exports.resetControllerPassword = function(req, res)
{
  // Init Variables
  console.log("ibi");
  var passwordDetails = req.body;
  /*  console.log(req.user);
    if (req.user) {*/
  console.log("nkjds");
  console.log(passwordDetails);
  if (passwordDetails.newPassword)
  {
    console.log(req.body);
    Control.findOne(
    {
      username: passwordDetails.userid
    }, function(err, user)
    {
      console.log(user);
      console.log("errr" + err);
      if (!err && user)
      {
        if (passwordDetails.newPassword === passwordDetails.verifyPassword)
        {
          user.password = passwordDetails.newPassword;
          user.pass_status = 1;
          
          user.save(function(err)
          {
            if (err)
            {
              console.log(err);
              return res.status(400).send(
              {
                message: errorHandler.getErrorMessage(err)
              });
            }
            else
            {
              /*    req.login(user, function(err) {
                    if (err) {
                      res.status(400).send(err);
                    } else {
                      
                  });
                }*/
              res.send(
              {
                message: 'Password is successfully updated'
              });
            }
          });
        }
        else
        {
          res.status(400).send(
          {
            message: 'Confirm correct password'
          });
        }
      }
      else
      {
        res.status(400).send(
        {
          message: 'User is not found'
        });
      }
    });
  }
  else
  {
    res.status(400).send(
    {
      message: 'Please provide a new password'
    });
  }
};
exports.getUserInit = function(req, res)
{
  Control.findOne(
  {
    username: req.params.username
  }).exec(function(err, data)
  {
    res.json(data);
  });
}
exports.getCustDetail = function(req, res)
{
  User.findOne(
  {
    username: req.params.username
  }).exec(function(err, data)
  {
    res.json(data);
  });
}
exports.signin = function(req, res, next)
{
  passport.authenticate('control-local', function(err, control, info)
  {
    if (err || !control)
    {
      if (typeof myVar != 'undefined') res.status(400).send(info);
      else res.status(400).send(
      {
        message: 'Unknown controller or invalid password'
      });
    }
    else
    {
      control.password = undefined;
      control.salt = undefined;
      req.login(control, function(err)
      {
        if (err)
        {
          res.status(400).send(err);
        }
        else
        {
          res.json(control);
        }
      });
    }
  })(req, res, next);
};
/**
 * Change Password
 */
exports.changePassword = function(req, res)
{
  // Init Variables
  var passwordDetails = req.body;
  console.log(passwordDetails)
Control.findById(req.body.id, function(err, user)
    {
        console.log(user);
        console.log(passwordDetails);
      
          user.temp_pass = passwordDetails.temp_pass;
          user.last_date=Date.now();
          user.last_update_by=req.body.userId;
          user.pass_status=0;
          user.save(function(err)
          {
            if (err)
            {
              return res.status(400).send(
              {
                message: errorHandler.getErrorMessage(err)
              });
            }
            else
            {
              req.login(user, function(err)
              {
                if (err)
                {
                  res.status(400).send(err);
                }
                else
                {
                  res.send(
                  {
                    message: 'Password changed successfully'
                  });
                }
              });
            }
          });

    });
  
};