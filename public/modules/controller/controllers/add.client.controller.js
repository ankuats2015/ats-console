var controler = angular.module('controller');
controler.directive('ngDate', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            element.datepicker({
                changeYear: true,
                changeMonth: true,
                dateFormat: 'mm/dd/yy',
                onSelect: function(date) {
                    ctrl.$setViewValue(date);

                    scope.$apply();
                }
            });
        }
    };
});
controler.controller('addClientController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', 'applicationService', 'Upload', '$timeout', '$route', 'ngDialog', '$modal', '$sce',
    function($rootScope, $scope, $http, $location, Authentication, myService, applicationService, Upload, $timeout, $route, ngDialog, $modal, $sce) {
        //console.log(Authentication);
        $scope.user = Authentication.user;
        console.log($scope.user);
        $scope.addclienttab = true;
        $scope.formdata = {};
        $scope.custom = "";
        $scope.pendmsg = "";
        $scope.formdata.user = {};
        $scope.formdata.user.ustate = {};
        $scope.formdata.user.taxregion = {};

        $scope.formdata.user.ustate.name = "";
        $scope.formdata.user.taxregion.TaxRegionName = "";

        $scope.formdata.user.primaryemai = "";
        $scope.formdata.user.contact = {};
        $scope.formdata.department = {};
        $scope.formdata.card = {};
        $scope.formdata.card.advantage = {};
        $scope.formdata.card.giftcard = {};
        $scope.formdata.card.cashback = {};
        $scope.formdata.user.taxregion = {};
        $scope.formdata.card.points = {};
        $scope.formdata.ownernames = [];
        $scope.confirmcarddata = {};
        $scope.confirmdepart = {};
        $scope.confirmuser = {};
        $scope.staterror = "";
        $scope.dataw = {};
        $scope.accessnumber = 0;
        $scope.xvalue = "X";
        var tempdata = {};
        $scope.formdata.states = {};
        //$scope.check=true;
        $scope.filenames = [];
        var accesscode = randomIntFromInterval(1000000000, 9999999999);
        // console.log(accesscode);
        var dptnu = randomIntFromInterval(1000, 9999);
        //   console.log(dptnu);
        var date = new Date();
        var uniqueCode="";
        var confirmation = randomIntFromInterval(100000000, 999999999);
        var uniqueCode = randString(3)+date.getTime()+randString(2);
          console.log(uniqueCode)
          

       // uniqueCode = randString(3)+dptnu+randString(3);
        //console.log(uniqueCode)
        $scope.selectOption = true;
        $scope.zipError = "";
        $scope.check = false;
        $scope.filename = "";
        $scope.logoname = "";
        $scope.misl = [];
        var formdata = [];
        var userdata = {};
        $scope.data = {};
        var departdata = {};
        var carddata = [];
        $rootScope.enableLoader = true;
        // If user is not signed in then redirect back home
        if (!$scope.user || $scope.user.userType != 'controller') $location.path('/');

        function randomIntFromInterval(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        function randString(x) {
            var s = "";
            while (s.length < x && x > 0) {
                var r = Math.random();
                s += (r < 0.1 ? Math.floor(r * 100) : String.fromCharCode(Math.floor(r * 26) + (r > 0.5 ? 97 : 65)));
            }
            return s;
        }
        $scope.initData = function() {
            $scope.firstpart = true;
            $scope.fileaccessnumber = randomIntFromInterval(10000, 99990);
            $scope.giftselected = false;
            $scope.advselected = false;
            $scope.cashselected = false;
            $scope.pointselected = false;
            $scope.getcheck = true;
            $scope.checkForNext = true;
            $scope.checkforgift = true;
            $scope.checkforadvantage = true;
            $scope.checkforcashback = true;
            $scope.attachfile = true;
            $scope.uploadfile = true;
            $scope.checkforpoints = true;
            if ($scope.user.userRole) {
                checkUserType($scope.user.userRole);
            } else {
                checkUserType($scope.user.roles[0]);
            }
            if ($scope.formdata.user.end_date < new Date()) {
                $scope.formdata.user.status = 'Active';
            } else {
                $scope.formdata.user.status = 'Inactive';
            }
            $scope.title = ["This consumer card can be purchased for a dollar value no more than $1000.00. The dollar value of the card decreases as and when it is used. This card can be gifted to anyone.", "This consumer card adds a bonus amount to the card from the actual purchase price of the card. The dollar value of the card decreases as and when it is used.", "This consumer card adds certain cash back amount to the card for purchases made on and above certain amount. This cash back amount can be used for future purchases. When purchased item(s) are returned, Cashback amount for returns will be deducted/adjusted accordingly.", "This consumer card gives you certain points for purchases made on and above certain amount. Defined number of accumulated points can be used for future purchases. When purchased item(s) are returned, Points for returns will be deducted/adjusted accordingly."];
            $scope.secondpart = false;
            $scope.confirmationpage = false;
            $scope.check = true;
            $scope.formdata.user.status = "Active";
            $scope.clientviewpage = false;
            $rootScope.enableLoader = false;
            $scope.formdata.user.contact.primary = {};
            $scope.formdata.user.contact.alternate = {};
            $scope.formdata.user.contact.alternate.phone1 = {};
            $scope.formdata.user.contact.alternate.phone2 = {};
            $scope.formdata.department.ownernames = [{
                name: ""
            }];
            $scope.formdata.card.advantage.datas = [{
                get: "",
                buy: "",
                msg: ""
            }];
            //Get the all States
            applicationService.state.query().$promise.then(function(states) {
                $scope.formdata.states = states;
            }, function(error) {
                alert('Unable to get state list');
            });
            applicationService.category.query().$promise.then(function(catData) {
                $scope.categories = catData;
                $scope.categories.name = "Others";
                console.log($scope.categories);
                $scope.searchBy = "direct";
            });
            $scope.giftcard = true;
            $scope.advantage = true;
            $scope.cashback = true;
            $scope.points = true;
            $scope.giftcards = ['ISSGC1.jpg', 'ISSGC2.jpg', 'ISSGC3.jpg', 'ISSGC4.jpg', 'ISSGC5.jpg'];
            $scope.advantages = ['ISSAC1.jpg', 'ISSAC2.jpg', 'ISSAC3.jpg', 'ISSAC4.jpg', 'ISSAC5.jpg'];
            $scope.point = ['ISSMPC1.jpg', 'ISSMPC2.jpg', 'ISSMPC3.jpg', 'ISSMPC4.jpg', 'ISSMPC5.jpg'];
            $scope.cashbacks = ['ISSCBC1.jpg', 'ISSCBC2.jpg', 'ISSCBC3.jpg', 'ISSCBC4.jpg', 'ISSCBC5.jpg'];
            var data = $http.get('/controllers/gettempdata');
            data.success(function(data) {
                $scope.dataw = data;
                console.log(data);
                console.log(data != null);
                console.log(data == null);
                if (data != null) {
                    var eff_date = new Date(data.eff_date);
                    $scope.formdata.user.eff_date = ('0' + (eff_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (eff_date.getDate())).slice(-2) + "/" + eff_date.getFullYear();
                    var end_date = new Date(data.end_date);
                    $scope.formdata.user.end_date = end_date.getMonth() + 1 + "/" + end_date.getDate() + "/" + end_date.getFullYear();
                    var upd_date = new Date(data.upd_date);
                    $scope.formdata.user.upd_date = ('0' + (upd_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (upd_date.getDate())).slice(-2) + "/" + upd_date.getFullYear();
                    $scope.formdata.department.name = data.buname;
                    $scope.formdata.user.taxid = data.taxid;
                    $scope.formdata.user.category = {
                        'name': data.categary
                    };
                    $scope.formdata.user.addressline1 = data.address1;
                    $scope.formdata.user.addressline2 = data.address2;
                    $scope.formdata.user.addressline3 = data.address3;
                    $scope.formdata.user.ustate = {
                        "name": data.state
                    };
                    $scope.formdata.user.taxregion = {
                        "TaxRegionName": data.taxregion
                    };
                    $scope.formdata.user.city = data.city;
                    $scope.formdata.user.zipcode = parseInt(data.zipcode);
                    $scope.formdata.user.primaryemail = data.email;
                    $scope.formdata.user.alternativeEmails = data.alternativeEmails;
                    $scope.formdata.user.contact.primary.number = parseInt(data.phone);
                    $scope.formdata.user.contact.primary.phoneType = data.phonetype1;
                    $scope.formdata.user.contact.alternate.phone1.number = parseInt(data.alternatephone1);
                    $scope.formdata.user.contact.alternate.phone2.type = data.phonetype2;
                    $scope.formdata.user.contact.alternate.phone2.number = parseInt(data.alternatephone2);
                    $scope.formdata.user.contact.alternate.phone2.type = data.phonetype3;
                    $scope.formdata.card.giftcard.name = data.giftcardname;
                    $scope.formdata.card.giftcard.image = data.giftcardimage;
                    $scope.formdata.card.advantage.name = data.advantagecardname;
                    $scope.formdata.card.advantage.image = data.advantagecardimage;
                    /* angular.forEach($scope.formdata.card.advantage.datas, function(val, key) {
                             tempdata.advantage.push({"get":val.get,"buy":val.buy});                  
                         });*/
                    $scope.formdata.card.cashback.name = data.cashbackcardname;
                    $scope.formdata.card.cashback.image = data.cashbackcardimage;
                    $scope.formdata.card.cashback.percentage = data.cashbackper1;
                    $scope.formdata.card.cashback.earn1 = data.cashbackfor;
                    $scope.formdata.card.cashback.earn = data.cashbackpur;
                    $scope.formdata.card.points.name = data.pointcardname;
                    $scope.formdata.card.points.image = data.pointcardimage;
                    $scope.formdata.card.points.earn1 = data.earnpoint1;
                    $scope.formdata.card.points.point1 = data.earnpoint2;
                    $scope.formdata.card.points.earn2 = data.earnpoint3;
                    $scope.formdata.card.points.point2 = data.earnpoint4;
                    $scope.formdata.department.onlyCheckoutAccess = data.accesstype;
                    $scope.formdata.department.disclaimer = $sce.trustAsHtml(data.disclaimer);
                    $scope.user.username = data.userId;
                    $scope.formdata.department.processor_notes = $sce.trustAsHtml(data.processor_notes);
                    $scope.logoname = data.logo;
                    /*  angular.forEach($scope.filenames, function(val, key) {
                          tempdata.misc_attach.push(val);
                      });*/
                } else {

                }

            });
            console.log("nd");
            var eff_date = new Date();
            $scope.formdata.user.eff_date = ('0' + (eff_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (eff_date.getDate())).slice(-2) + "/" + eff_date.getFullYear();
            var end_date = new Date('12/31/2999');
            $scope.formdata.user.end_date = ('0' + (end_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (end_date.getDate())).slice(-2) + "/" + end_date.getFullYear(); //end_date.getMonth() + 1 + "/" + end_date.getDate() + "/" + end_date.getFullYear();
            var upd_date = new Date();
            $scope.formdata.user.upd_date = ('0' + (upd_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (upd_date.getDate())).slice(-2) + "/" + upd_date.getFullYear();
        }
        $scope.addOwnerNames = function($event) {
            $event.preventDefault();
            $scope.formdata.department.ownernames.push({
                name: ""
            });
            // $rootScope.alerts.push({type: 'success', msg: 'Profile Details updated!'});
        }
        $scope.addcard = function($event) {
            $event.preventDefault();
            $scope.formdata.card.advantage.datas.push({
                get: "",
                buy: ""
            });
            // $rootScope.alerts.push({type: 'success', msg: 'Profile Details updated!'});
        }
        $scope.removeName = function($event, name) {
            $event.preventDefault();
            if ($scope.formdata.department.ownernames.indexOf(name) !== -1) $scope.formdata.department.ownernames.splice($scope.formdata.department.ownernames.indexOf(name), 1);
            else alert('Something went wrong, Unable to remove recepent.');
        };
        $scope.removeProduct = function($event, name) {
            $event.preventDefault();
            if ($scope.formdata.card.advantage.datas.indexOf(name) !== -1) $scope.formdata.card.advantage.datas.splice($scope.formdata.card.advantage.datas.indexOf(name), 1);
            else alert('Something went wrong, Unable to remove recepent.');
        };
        $scope.checkEndDate = function(e) {
            if ($scope.formdata.end_date.length == 10) {
                end_date = new Date($scope.formdata.end_date).getDate();
                end_date_month = new Date($scope.formdata.end_date).getMonth() + 1;
                end_date_year = new Date($scope.formdata.end_date).getFullYear();
                eff_date = new Date($scope.formdata.eff_date).getDate();
                eff_date_month = new Date($scope.formdata.eff_date).getMonth() + 1;
                eff_date_year = new Date($scope.formdata.eff_date).getFullYear();
                /*if(end_date < eff_date || end_date_month < eff_date_month || end_date_year < eff_date_year)
                        {
                            console.log($scope.formdata.end_date <= $scope.formdata.eff_date)
                                console.log($scope.formdata.end_date);
                 
                            $rootScope.alerts.push({type: 'danger', msg: 'End Date cannot be prior to Effective date' });
                        }*/
            }
        }
        $scope.uploadLogo = function(file) {
            $rootScope.enableLoader = true;
            var max_size = 2097000000;
            $scope.accessnumber = randomIntFromInterval(10000, 99990);
            console.log($scope.accessnumber);
            if (file != null) {
                if (file.size < max_size) {
                    Upload.upload({
                        url: '/upload/url',
                        fields: {
                            'templatename': $scope.uploadtext,
                        },
                        file: file,
                    }).progress(function(evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                    }).success(function(data, status, headers, config) {
                        console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                        data.templatename = $scope.uploadtext;
                        data.id = $scope.user._id;
                        data.accessnumber = $scope.accessnumber;
                        console.log(data.fName);
                        $scope.filename = data.fname;
                        $scope.logoname = data.fName;
                        $scope.uploaded = data.fName;
                        var res = $http.post('/uploadImage', data);
                        res.success(function(res, err) {
                            console.log($scope.uploaded);
                            $scope.successmsg = "Logo Uploaded Successfully";
                            $rootScope.enableLoader = false;

                        });
                        res.error(function(err) {
                            console.log(err);
                        });
                    }).error(function(data, status, headers, config) {
                        console.log('error status: ' + data);
                    })
                } else {
                    $rootScope.alerts.push({
                        'msg': "Please Upload  Image greate than 2mb",
                        'type': "danger"
                    });
                }
            } else {
                $rootScope.alerts.push({
                    'msg': "Please Upload Valid Image .jpg,.png",
                    'type': "danger"
                });
            }
        };
        $scope.uploadFiles = function(file) {
            $rootScope.enableLoader = true;
            var max_size = 2097000000;
            if (file != null) {
                if (file.size < max_size) {
                    Upload.upload({
                        url: '/upload/url',
                        fields: {
                            'templatename': $scope.uploadtext,
                            'accessnumber': $scope.accessnumber
                        },
                        file: file,
                    }).progress(function(evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                    }).success(function(data, status, headers, config) {
                        console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                        data.templatename = config.file.name;
                        data.id = $scope.user._id;
                        data.accessnumber = $scope.fileaccessnumber;
                        console.log(data.fName);
                        $scope.misl.push(data.fName);
                        $scope.uploaded = data.fName;
                        var res = $http.post('/uploadImage', data);
                        res.success(function(res, err) {
                            console.log($scope.uploaded);
                            /*  $rootScope.alerts.push({
                                    "msg": config.file.name +" Uploaded",
                                    "type": "success"
                                });*/
                            $scope.formdata.filename = config.file.name;
                            //$scope.filenames = uploadget();
                            $scope.filenames.push({
                                "templatename": config.file.name,
                                "filename": data.fName
                            })
                            $scope.successmsg1 = "Attached Successfully";
                            $rootScope.enableLoader = false;
                            $scope.filename = "";
                            //$scope.sendemail = false;
                        });
                        res.error(function(err) {
                            console.log(err);
                        });
                    }).error(function(data, status, headers, config) {
                        console.log('error status: ' + data);
                    })
                } else {
                    $rootScope.alerts.push({
                        'msg': "Please Upload  Image greate than 2mb",
                        'type': "danger"
                    });
                }
            } else {
                $rootScope.alerts.push({
                    'msg': "Please Upload Valid Image .jpg,.png",
                    'type': "danger"
                });
            }
        };

        function uploadget() {
            var drop = $http.get('/controller/getFile/' + $scope.fileaccessnumber);
            drop.success(function(res, err) {
                console.log(res);
                $scope.filenames = [];
                angular.forEach(res, function(value, key) {
                    console.log(value)
                    $scope.filenames.push({
                        "templatename": value.templatename,
                        "filename": value.filename
                    });
                })
                return res;
            });
        }
        $scope.itemRemove = function($event, file, filename) {
            $scope.filename = "";
            // if ($scope.filenames.indexOf(filename) !== -1) $scope.filenames.splice($scope.filenames.indexOf(filename), 1);
            //    else alert('Something went wrong, Unable to remove recepent.');
            console.log(filename);
            var filename
            var result = $http.post('/controller/itemRemove', filename);
            result.success(function(res) {
                console.log(res);
                uploadget();
            });
            result.error(function(err) {
                console.log(err);
            })
        }


        function randomIntFromInterval(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        function toTitleCase(str) {
            return str.replace(/(?:^|\s)\w/g, function(match) {
                return match.toUpperCase();
            });
        }
        $scope.formSave = function($event) {
            $http.get('/controller/removeEntry').success(function(data) {
                console.log(data);
            })
            console.log($scope.misl);
            console.log($scope.filename);
            //console.log($scope.formdata);
            userdata.contact = [];
            cards = [];
            departdata.ownerName = [];
            //carddata.advantage={};
            var giftcard = {};
            giftcard.name = "";
            giftcard.cardCode = "";
            giftcard.cardNumUnit = "";
            giftcard.type = "";
            giftcard.image = "";
            giftcard.dptnu = "";
            var advantage = {};
            advantage.name = "";
            advantage.cardCode = "";
            advantage.cardNumUnit = "";
            advantage.type = "";
            advantage.image = "";
            advantage.dptnu = "";
            advantage.amountList = [];
            var cashback = {};
            cashback.name = "";
            departdata.processor_notes = [];
            cashback.cardCode = "";
            cashback.cardNumUnit = "";
            cashback.type = "";
            cashback.image = "";
            cashback.dptnu = "";
            var points = {};
            points.name = "";
            points.cardCode = "";
            points.cardNumUnit = "";
            points.type = "";
            points.image = "";
            points.dptnu = "";
            //carddata.cashback={};
            //carddata.points={};
            departdata.misc_attach = [];
            //carddata.advantage.amountList=[];
            /*Collecting Data for User(For saving Vendor Data) Collection*/
            userdata.address1 = $scope.formdata.user.addressline1;
            userdata.address2 = $scope.formdata.user.addressline2;
            userdata.address3 = $scope.formdata.user.addressline3;
            userdata.eff_date = $scope.formdata.user.eff_date;
            userdata.end_date = $scope.formdata.user.end_date;
            userdata.upd_date = $scope.formdata.user.upd_date;
            userdata.state = $scope.formdata.user.ustate.name;
            userdata.stateid = [$scope.formdata.user.ustate._id];
            userdata.roles = ["Super User"];
            userdata.city = $scope.formdata.user.city;
            console.log($scope.formdata.user.taxregion);
            userdata.taxregion = $scope.formdata.user.taxregion.TaxRegionName;
            userdata.taxid = $scope.formdata.user.taxid;
            userdata.userType = "vendor";
            userdata.confirmation = confirmation;
            userdata.dptnu = dptnu;
            userdata.uniqueCode=uniqueCode;
            userdata.username = $scope.formdata.user.primaryemail.substring(0, $scope.formdata.user.primaryemail.indexOf('@')) + randomIntFromInterval(1000, 9999);
            userdata.status = $scope.formdata.user.status;
            userdata.zipcode = $scope.formdata.user.zipcode;
            userdata.categary = $scope.formdata.user.category.name;
            userdata.email = $scope.formdata.user.primaryemail;
            userdata.alternativeEmails = $scope.formdata.user.alternativeEmails;
            userdata.contact.push({
                "number": $scope.formdata.user.contact.primary.number,
                "type": "primary",
                "phoneType": $scope.formdata.user.contact.primary.phoneType
            });
            userdata.phone = $scope.formdata.user.contact.primary.number ? $scope.formdata.user.contact.primary.number : randomIntFromInterval(1000000000, 9999999999);
            angular.forEach($scope.formdata.user.contact.alternate, function(val, key) {
                userdata.contact.push({
                    "number": val.number,
                    "type": "alternate",
                    "phoneType": val.phoneType
                });
            });
            /*Collecting Data for Department Collection*/
            departdata.name = $scope.formdata.department.name;
            angular.forEach($scope.formdata.department.ownernames, function(value, key) {
                departdata.ownerName.push(value.name);
            });
            departdata.onlyCheckoutAccess = $scope.formdata.department.onlyCheckoutAccess;
            departdata.accessCode = accesscode;
            departdata.dptnu = dptnu;
            departdata.confirmation = confirmation;
            departdata.uniqueCode=uniqueCode;
            departdata.address = $scope.formdata.user.city + ',' + $scope.formdata.user.ustate.name;
            departdata.disclaimer = $scope.formdata.department.disclaimer;
            departdata.processor_notes.push({
                "notes": $scope.formdata.department.processor_notes
            });
            departdata.logo = $scope.logoname;
            angular.forEach($scope.filenames, function(val, key) {
                departdata.misc_attach.push(val);
            })
            console.log(departdata);
            /*Collecting Data for Card Collection*/
            //carddata=$scope.formdata.card;
            if ($scope.formdata.card.giftcard.type == true) {
                giftcard.name = toTitleCase($scope.formdata.card.giftcard.name);
                giftcard.cardCode = giftcard.name.match(/\b([A-Z])/g).join('');
                giftcard.cardNumUnit = randomIntFromInterval(10, 99);
                giftcard.type = 'gift';
                giftcard.image = $scope.formdata.card.giftcard.image;
                giftcard.dptnu = dptnu;
                giftcard.uniqueCode = uniqueCode;

                carddata.push(giftcard);
            }
            if ($scope.formdata.card.advantage.type == true) {
                advantage.name = toTitleCase($scope.formdata.card.advantage.name);
                advantage.cardCode = advantage.name.match(/\b([A-Z])/g).join('');
                advantage.cardNumUnit = randomIntFromInterval(10, 99);
                advantage.type = 'advantage';
                advantage.image = $scope.formdata.card.advantage.image;
                advantage.dptnu = dptnu;
                advantage.uniqueCode = uniqueCode;
                console.log($scope.formdata.card.advantage.datas);
                angular.forEach($scope.formdata.card.advantage.datas, function(val, key) {
                    var text = "Get $" + val.get + " card for $" + val.buy;
                    console.log(text);
                    advantage.amountList.push({
                        "price": val.get,
                        "text": text
                    });
                });
                if (advantage.amountList) {
                    console.log(advantage.amountList);
                }
                carddata.push(advantage);
            }
            if ($scope.formdata.card.cashback.type == true) {
                cashback.name = toTitleCase($scope.formdata.card.cashback.name);
                cashback.cardCode = cashback.name.match(/\b([A-Z])/g).join('');
                cashback.cardNumUnit = randomIntFromInterval(10, 99);
                cashback.type = 'cashback';
                cashback.image = $scope.formdata.card.cashback.image;
                cashback.dptnu = dptnu;
                cashback.uniqueCode = uniqueCode;
                cashback.cashbackper = $scope.formdata.card.cashback.percentage;
                cashback.cashbackvalue = $scope.formdata.card.cashback.earn;
                carddata.push(cashback);
            }
            if ($scope.formdata.card.points.type == true) {
                points.name = toTitleCase($scope.formdata.card.points.name);
                points.cardCode = points.name.match(/\b([A-Z])/g).join('');
                points.cardNumUnit = randomIntFromInterval(10, 99);
                points.type = 'points';
                points.image = $scope.formdata.card.points.image;
                points.dptnu = dptnu;
                points.uniqueCode = uniqueCode;
                points.earn1 = $scope.formdata.card.points.earn1;
                points.earn2 = $scope.formdata.card.points.earn2;
                points.point1 = $scope.formdata.card.points.point1;
                points.point2 = $scope.formdata.card.points.point2;
                carddata.push(points);
            }
            console.log(carddata);
            var cardSave = $http.post('/addclient/cardSave/', carddata);
            var getcard = $http.get('/addclient/getcard/' + dptnu);
            getcard.success(function(data) {
                $rootScope.enableLoader = true;
                console.log(data);
                angular.forEach(data, function(value, key) {
                    cards.push(value._id);
                });
                console.log(cards);
                departdata.cards = cards;
                var status = $http.post('/addclient/saveDepart', departdata);
                status.success(function(data) {
                    console.log(data);
                    userdata.vendorId = data._id;
                    userdata.cities = {
                        "name": userdata.city,
                        "departments": {
                            "depId": userdata.vendorId,
                            "catname": $scope.formdata.user.category.name
                        }
                    };

                    var status1 = $http.post('/addclient/saveData', userdata);
                    status1.success(function(data) {
                        console.log(data);
                        $rootScope.enableLoader = false;
                        //          $timeout( function(){ console.log("hud") ;  }, 3000);
                        $scope.firstpart = false;
                        $scope.secondpart = false;
                        $scope.clientviewpage = false;
                        $scope.confirmationpage = true;
                        $scope.confirmcarddata = carddata;
                        $scope.confirmdepart = departdata;
                        $scope.confirmdepart.disclaimer = $sce.trustAsHtml(departdata.disclaimer);
                        $scope.confirmdepart.processor_notes = $sce.trustAsHtml(departdata.processor_notes[0].notes);

                        if (departdata.onlyCheckoutAccess == true) {
                            $scope.confirmdepart.checkout = "Consumer Card without Billing & NO Credit Card Processing ";
                        } else {
                            $scope.confirmdepart.checkout = "Consumer Card with Billing & NO Credit Card Processing";
                        }
                        $scope.confirmuser = userdata;
                        $scope.accesscode = accesscode;
                        $scope.confirmation = confirmation;
                        //$route.reload();
                    });
                });
            });
            // var status1 = $http.post('/addclient/saveData', userdata);
            //                     status1.success(function(data) {
            //                     });
        }
        $scope.getData = function() {
            console.log()
            $scope.confirdata = myService.getR
        }
        $scope.getTaxRegions = function() {
            console.log($scope.formdata.user.ustate);
            $scope.selectOption = false;
            $http.get('/addClient/getTaxRegions/' + $scope.formdata.user.ustate.abb).success(function(data) {
                console.log(data);
                $scope.formdata.user.taxregions = data;
            })
        }
        $scope.setZipCode = function($event, state) {
            console.log(state);
            //$scope.formdata.user.zipcode=state.ZipCode;
        }
        $scope.onCheck = function(value, status) {
            console.log(value);
            console.log(status);
            switch (value) {
                case 'gift':
                    if (status) {
                        $scope.giftcard = false;
                        $scope.giftselected = true;
                        $scope.checkforgift = false;
                        $scope.getcheck = false;
                    } else {
                        $scope.giftcard = true;
                        $scope.formdata.card.giftcard = "";
                        $scope.giftselected = false;
                        $scope.checkforgift = true;
                    }
                    break;
                case 'advantage':
                    if (status) {
                        $scope.advantage = false;
                        $scope.advselected = true;
                        $scope.checkforadvantage = false;
                        $scope.getcheck = false;
                    } else {
                        $scope.advantage = true;
                        $scope.formdata.card.advantage.name = "";
                        $scope.formdata.card.advantage.datas = [{
                            get: "",
                            buy: "",
                            msg: ""
                        }];
                        $scope.advselected = false;
                        $scope.checkforadvantage = true;
                    }
                    break;
                case 'cashback':
                    if (status) {
                        $scope.cashback = false;
                        $scope.cashselected = true;
                        $scope.checkforcashback = false;
                        $scope.getcheck = false;
                    } else {
                        $scope.cashback = true;
                        $scope.formdata.card.cashback = "";
                        $scope.cashselected = false;
                        $scope.checkforcashback = true;
                    }
                    break;
                case 'point':
                    if (status) {
                        $scope.points = false;
                        $scope.pointselected = true;
                        $scope.checkforpoints = false;
                        $scope.getcheck = false;
                    } else {
                        $scope.points = true;
                        $scope.formdata.card.points = "";
                        $scope.pointselected = false;
                        $scope.checkforpoints = true;
                    }
                    break;
                default:
                    break;
            }
        }
        $scope.nextPage = function($event) {
            $event.preventDefault();
            $scope.firstpart = false;
            $scope.secondpart = true;
            $scope.clientviewpage = false;
            $scope.confirmationpage = false;
        }
        $scope.clientview = function($event) {
            $event.preventDefault();
            $scope.firstpart = false;
            $scope.formdata.department.misc_attch = {};
            $scope.secondpart = false;
            $scope.clientviewpage = true;
            console.log($scope.logoname);
            $scope.formdata.department.logo = $scope.logoname;
            $scope.formdata.department.misc_attch = $scope.filenames;
            $scope.formdata.department.accesscode = accesscode;
            $scope.formdata.department.confirmation = confirmation;
            $scope.formdata.department.dptnu = dptnu;
            $scope.formdata.card.giftcard.name = toTitleCase($scope.formdata.card.giftcard.name);
            $scope.formdata.card.advantage.name = toTitleCase($scope.formdata.card.advantage.name);
            $scope.formdata.card.cashback.name = toTitleCase($scope.formdata.card.cashback.name);
            $scope.formdata.card.points.name = toTitleCase($scope.formdata.card.points.name);
            console.log($scope.formdata.department.disclaimer);
            $scope.formdata.department.disclaimer_html = $sce.trustAsHtml($scope.formdata.department.disclaimer);
            $scope.formdata.department.processor_notes_html = $sce.trustAsHtml($scope.formdata.department.processor_notes);
            console.log($scope.formdata);
            $scope.confirmationpage = false;
        }
        $scope.tempSaveData = function($event) {
            $rootScope.enableLoader = true;
            userdata.contact = [];
            cards = [];
            departdata.ownerName = [];
            //carddata.advantage={};
            var giftcard = {};
            giftcard.name = "";
            giftcard.cardCode = "";
            $scope.checkforSave = true;
            giftcard.cardNumUnit = "";
            giftcard.type = "";
            giftcard.image = "";
            giftcard.dptnu = "";
            var advantage = {};
            advantage.name = "";
            advantage.cardCode = "";
            advantage.cardNumUnit = "";
            advantage.type = "";
            advantage.image = "";
            advantage.dptnu = "";
            advantage.amountList = [];
            var cashback = {};
            cashback.name = "";
            departdata.processor_notes = [];
            cashback.cardCode = "";
            cashback.cardNumUnit = "";
            cashback.type = "";
            cashback.image = "";
            cashback.dptnu = "";
            var points = {};
            points.name = "";
            points.cardCode = "";
            points.cardNumUnit = "";
            points.type = "";
            points.image = "";
            points.dptnu = "";
            //carddata.cashback={};
            //carddata.points={};
            departdata.misc_attach = [];
            //carddata.advantage.amountList=[];
            /*Collecting Data for User(For saving Vendor Data) Collection*/

            userdata.address1 = $scope.formdata.user.addressline1;
            userdata.address2 = $scope.formdata.user.addressline2;
            userdata.address3 = $scope.formdata.user.addressline3;
            userdata.eff_date = $scope.formdata.user.eff_date;
            userdata.end_date = $scope.formdata.user.end_date;
            userdata.upd_date = $scope.formdata.user.upd_date;
            if (!isEmpty($scope.formdata.user.ustate.name)) {
                userdata.state = $scope.formdata.user.ustate.name;
                userdata.stateid = [$scope.formdata.user.ustate._id];
            }

            userdata.roles = ["Super User"];
            userdata.city = $scope.formdata.user.city;
            console.log($scope.formdata.user.taxregion);
            userdata.taxregion = $scope.formdata.user.taxregion.TaxRegionName;
            userdata.taxid = $scope.formdata.user.taxid;
            userdata.userType = "vendor";
            userdata.confirmation = confirmation;
            userdata.dptnu = dptnu;
            userdata.uniqueCode=uniqueCode;
            if (!isEmpty($scope.formdata.user.primaryemail)) {
                userdata.username = $scope.formdata.user.primaryemail ? $scope.formdata.user.primaryemail.substring(0, $scope.formdata.user.primaryemail.indexOf('@')) + randomIntFromInterval(1000, 9999) : "";
            } else {
                userdata.username = $scope.formdata.department.name + randomIntFromInterval(1000, 9999);
            }

            userdata.status = $scope.formdata.user.status;
            userdata.zipcode = $scope.formdata.user.zipcode;
            if (!isEmpty($scope.formdata.user.category)) {

                userdata.categary = $scope.formdata.user.category.name;
            }

            userdata.email = $scope.formdata.user.primaryemail;
            userdata.alternativeEmails = $scope.formdata.user.alternativeEmails;
            userdata.contact.push({
                "number": $scope.formdata.user.contact.primary.number,
                "type": "primary",
                "phoneType": $scope.formdata.user.contact.primary.phoneType
            });
            userdata.phone = $scope.formdata.user.contact.primary.number ? $scope.formdata.user.contact.primary.number : randomIntFromInterval(1000000000, 9999999999);
            angular.forEach($scope.formdata.user.contact.alternate, function(val, key) {
                userdata.contact.push({
                    "number": val.number,
                    "type": "alternate",
                    "phoneType": val.phoneType
                });
            });
            /*Collecting Data for Department Collection*/
            console.log(!isEmpty($scope.formdata.department.name))
            if (!isEmpty($scope.formdata.department.name)) {
                departdata.name = $scope.formdata.department.name;
                $scope.pendmsg = "";
            } else {
                $scope.checkforSave = false;
                $rootScope.enableLoader = false;
                $scope.pendmsg = "Please Enter Business Unit Name";
            }
            if (!isEmpty($scope.formdata.department.ownernames)) {
                angular.forEach($scope.formdata.department.ownernames, function(value, key) {
                    departdata.ownerName.push(value.name);
                });
            } else {
                departdata.ownerName.push("");
            }

            departdata.onlyCheckoutAccess = $scope.formdata.department.onlyCheckoutAccess;
            departdata.accessCode = accesscode;
            departdata.dptnu = dptnu;
            departdata.uniqueCode=uniqueCode;
            departdata.confirmation = confirmation;
            departdata.address = $scope.formdata.user.city + ',' + $scope.formdata.user.ustate.name;
            departdata.disclaimer = $scope.formdata.department.disclaimer;
            departdata.processor_notes.push({
                "notes": $scope.formdata.department.processor_notes
            });
            departdata.logo = $scope.logoname;
            angular.forEach($scope.filenames, function(val, key) {
                departdata.misc_attach.push(val);
            })
            console.log(departdata);
            /*Collecting Data for Card Collection*/
            //carddata=$scope.formdata.card;
            if ($scope.formdata.card.giftcard.type == true) {
                giftcard.name = toTitleCase($scope.formdata.card.giftcard.name);
                giftcard.cardCode = giftcard.name.match(/\b([A-Z])/g).join('');
                giftcard.cardNumUnit = randomIntFromInterval(10, 99);
                giftcard.type = 'gift';
                giftcard.image = $scope.formdata.card.giftcard.image;
                giftcard.dptnu = dptnu;
                giftcard.uniqueCode=uniqueCode;
                carddata.push(giftcard);
            }
            if ($scope.formdata.card.advantage.type == true) {
                advantage.name = toTitleCase($scope.formdata.card.advantage.name);
                advantage.cardCode = advantage.name.match(/\b([A-Z])/g).join('');
                advantage.cardNumUnit = randomIntFromInterval(10, 99);
                advantage.type = 'advantage';
                advantage.image = $scope.formdata.card.advantage.image;
                advantage.dptnu = dptnu;
                advantage.uniqueCode=uniqueCode;
                console.log($scope.formdata.card.advantage.datas);
                angular.forEach($scope.formdata.card.advantage.datas, function(val, key) {
                    var text = "Get $" + val.get + " card for $" + val.buy;
                    console.log(text);
                    advantage.amountList.push({
                        "price": val.get,
                        "text": text
                    });
                });
                if (advantage.amountList) {
                    console.log(advantage.amountList);
                }
                carddata.push(advantage);
            }
            if ($scope.formdata.card.cashback.type == true) {
                cashback.name = toTitleCase($scope.formdata.card.cashback.name);
                cashback.cardCode = cashback.name.match(/\b([A-Z])/g).join('');
                cashback.cardNumUnit = randomIntFromInterval(10, 99);
                cashback.type = 'cashback';
                cashback.image = $scope.formdata.card.cashback.image;
                cashback.dptnu = dptnu;
                cashback.uniqueCode=uniqueCode;
                cashback.cashbackper = $scope.formdata.card.cashback.percentage;
                cashback.cashbackvalue = $scope.formdata.card.cashback.earn;
                carddata.push(cashback);
            }
            if ($scope.formdata.card.points.type == true) {
                points.name = toTitleCase($scope.formdata.card.points.name);
                points.cardCode = points.name.match(/\b([A-Z])/g).join('');
                points.cardNumUnit = randomIntFromInterval(10, 99);
                points.type = 'points';
                points.image = $scope.formdata.card.points.image;
                points.dptnu = dptnu;
                points.uniqueCode=uniqueCode;
                points.earn1 = $scope.formdata.card.points.earn1;
                points.earn2 = $scope.formdata.card.points.earn2;
                points.point1 = $scope.formdata.card.points.point1;
                points.point2 = $scope.formdata.card.points.point2;
                carddata.push(points);
            }
            console.log(carddata);
            if ($scope.checkforSave == true) {
                console.log("Save")
                if (!isEmpty(carddata)) {
                    var cardSave = $http.post('/addclient/cardSave/', carddata);
                    var getcard = $http.get('/addclient/getcard/' + dptnu);
                    getcard.success(function(data) {
                        $rootScope.enableLoader = true;
                        console.log(data);
                        angular.forEach(data, function(value, key) {
                            cards.push(value._id);
                        });
                        console.log(cards);
                        departdata.cards = cards;
                        var status = $http.post('/addclient/saveDepart', departdata);
                        status.success(function(data) {
                            console.log(data);
                            userdata.vendorId = data._id;
                            userdata.cities = {
                                "name": userdata.city,
                                "departments": userdata.vendorId
                            };
                            var status1 = $http.post('/addclient/saveData', userdata);
                            status1.success(function(data) {
                                console.log(data);
                                $rootScope.enableLoader = false;
                                $scope.pendmsg = "Your Data Has been Saved";
                                //          $timeout( function(){ console.log("hud") ;  }, 3000);
                                $scope.firstpart = true;
                                $scope.secondpart = false;
                                $scope.clientviewpage = false;
                                $scope.confirmationpage = false;
                                $scope.confirmcarddata = carddata;
                                $scope.confirmdepart = departdata;
                                if (departdata.onlyCheckoutAccess == true) {
                                    $scope.confirmdepart.checkout = "Consumer Card without Billing & NO Credit Card Processing ";
                                } else {
                                    $scope.confirmdepart.checkout = "Consumer Card with Billing & NO Credit Card Processing";
                                }


                                //$scope.formdata="";
                                $scope.confirmuser = userdata;
                                $scope.accesscode = accesscode;
                                $scope.confirmation = confirmation;
                                // $route.reload();
                            });
                        });
                    });
                } else {
                    var status = $http.post('/addclient/saveDepart', departdata);
                    status.success(function(data) {
                        console.log(data);
                        userdata.vendorId = data._id;
                        userdata.cities = {
                            "name": userdata.city,
                            "departments": userdata.vendorId
                        };
                        var status1 = $http.post('/addclient/saveData', userdata);
                        status1.success(function(data) {
                            console.log(data);
                            $rootScope.enableLoader = false;
                            $scope.pendmsg = "Client details successfully saved";
                           $location.path('/successPage/');                            //          $timeout( function(){ console.log("hud") ;  }, 3000);
                            $scope.firstpart = false;
                            $scope.secondpart = false;
                            $scope.clientviewpage = false;
                            $scope.confirmationpage = false;
                            $scope.confirmcarddata = carddata;
                            $scope.confirmdepart = departdata;
                            if (departdata.onlyCheckoutAccess == true) {
                                $scope.confirmdepart.checkout = "Consumer Card without Billing & NO Credit Card Processing ";
                            } else {
                                $scope.confirmdepart.checkout = "Consumer Card with Billing & NO Credit Card Processing";
                            }


                            //$scope.formdata="";
                            $scope.confirmuser = userdata;
                            $scope.accesscode = accesscode;
                            $scope.confirmation = confirmation;
                            // $route.reload();
                        });
                    });
                }


            }

        }
        $scope.cancelfirstPage = function() {
            $scope.formdata = {};
            $route.reload();
            $scope.firstpart = true;
            $scope.secondpart = false;
            $scope.clientviewpage = false;
            $scope.confirmationpage = false;
        }
        $scope.cancelsecondPage = function() {
            $scope.formdata = {};
            $route.reload();
            $scope.firstpart = false;
            $scope.secondpart = true;
            $scope.clientviewpage = false;
            $scope.confirmationpage = false;
        }
        $scope.backPage = function() {
            console.log();
            $scope.firstpart = true;
            $scope.secondpart = false;
            $scope.clientviewpage = false;
            //$scope.formdata = myService.getFormTempData();
            $scope.confirmationpage = false;
        }
        $scope.backverifyPage = function() {
                console.log();
                $scope.firstpart = false;
                $scope.secondpart = true;
                $scope.clientviewpage = false;
                $scope.formdata = myService.getFormTempData();
                $scope.confirmationpage = false;
            }
            /*$scope.$watch($scope.formdata.user.end_date,function(min,max)
            {
                if($scope.formdata.user.end_date < $scope.formdata.user.end_date)
                {
                    alert(";lkdnja");
                }

            });*/
        $scope.fillPer = function(val) {
            $scope.xvalue = val;
        }
        $scope.checkdt = function(vf) {
            angular.forEach($scope.formdata.card.advantage.datas, function(value, key) {
                console.log(value);
                if (value.get < value.buy) {
                    //console.log("chota hai ");
                    value.msg = "$ value for 'Get' should be greater than $ value for 'Buy'";
                    $scope.getcheck = true;
                } else {
                    value.msg = "";
                    $scope.getcheck = false;
                }
            })
        }
        $scope.cancelPage = function() {
            ngDialog.openConfirm({
                template: 'modalDialogId',
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function(value) {
                console.log('Modal promise resolved. Value: ', value);
                $route.reload();
            }, function(reason) {
                console.log('Modal promise rejected. Reason: ', reason);
            });
        }
        $scope.showPdf = function(file) {
            console.log(file);
            $scope.file = 'modules/controller/img/logo/' + file;
            ngDialog.openConfirm({
                template: 'pdfShowModal',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        }
        $scope.showImage = function(file) {
            console.log(file);
            $scope.file = 'modules/controller/img/logo/' + file;
            ngDialog.openConfirm({
                template: 'imageShowModal',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        }
        $scope.getfilename = function(file) {
            if (file != null) {
                console.log(file.name);
                $scope.filename = file.name;
                $scope.attachfile = false;
            }
        }
        $scope.getLogoName = function(file) {
            if (file != null) {
                console.log(file.name);
                $scope.uploadtext = file.name;
                $scope.uploadfile = false;
            }
        }
        $scope.verifyZip = function($event) {
            $event.preventDefault();
            $rootScope.enableLoader = true;
            var state = $scope.formdata.user.ustate.abb;
            var taxregion = $scope.formdata.user.taxregion.TaxRegionName;
            var zipcode = $scope.formdata.user.zipcode;
            $http.get('/controller/checkforzip/' + state + '/' + taxregion + '/' + zipcode).success(function(data) {
                $rootScope.enableLoader = false;
                if (data == null) {
                    $scope.staterror = "One or all of selected State, Tax Region, and Zip Code is incorrect";
                    $scope.formdata.user.zipcode = '';
                } else {
                    console.log(data);
                    $scope.staterror = "";
                }
            });
        }
        $scope.$watch('formdata.user.eff_date', function(newval, oldval) {
            if (new Date($scope.formdata.user.end_date) < new Date($scope.formdata.user.eff_date)) {

                $scope.errormsg = "End Date cannot be prior to Effective date";
                $scope.checkForNext = true;
            } else {
                $scope.checkForNext = false;
                $scope.errormsg = "";
            }
        });
        $scope.$watch('formdata.user.end_date', function(newval, oldval) {
            if (new Date($scope.formdata.user.end_date) < new Date($scope.formdata.user.eff_date)) {
                $scope.errormsg = "End Date cannot be prior to Effective date";
                $scope.checkForNext = true;
            } else {
                $scope.checkForNext = false;
                $scope.errormsg = "";
            }
            if (new Date($scope.formdata.user.end_date) > new Date()) {
                /*$scope.formdata.user.status="Inactive";
                     //$scope.errormsg="End Date cannot be prior to Effective date";*/
                $scope.formdata.user.status = "Active";
            } else {
                $scope.formdata.user.status = "Inactive";
            }
        });
        // $scope.$watch('formdata.user.end_date', function(newval, oldval) {
        //     //  console.log($scope.formdata.user.end_date);
        //     var date = new Date();
        //     var currdate = ('0' + (date.getMonth() + 1)).slice(-2) + "/" + ('0' + (date.getDate())).slice(-2) + "/" + date.getFullYear();
        //     //console.log(currdate);
        //     //     console.log($scope.formdata.user.end_date < currdate);
        //     console.log(new Date($scope.formdata.user.end_date) < new Date())
        // });
        /*Function Block*/
        function checkUserType(utype) {
            console.log(utype);
            if (utype === 'Super User' || utype === 'superuser') {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = true;
                $scope.companytab = true;
            }
            if (utype === 'Admin') {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = false;
                $scope.companytab = false;
            }
        }
        var hasOwnProperty = Object.prototype.hasOwnProperty;

        function isEmpty(obj) {
            // null and undefined are "empty"
            if (obj == null) return true;
            // Assume if it has a length property with a non-zero value
            // that that property is correct.
            if (obj.length > 0) return false;
            if (obj.length === 0) return true;
            // Otherwise, does it have any properties of its own?
            // Note that this doesn't handle
            // toString and valueOf enumeration bugs in IE < 9
            for (var key in obj) {
                if (hasOwnProperty.call(obj, key)) return false;
            }
            return true;
        }
    }
]);
angular.module('ui.date', []).constant('uiDateConfig', {}).directive('uiDate', ['uiDateConfig', '$timeout', function(uiDateConfig, $timeout) {
    'use strict';
    var options;
    options = {};
    angular.extend(options, uiDateConfig);
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, controller) {
            var getOptions = function() {
                return angular.extend({}, uiDateConfig, scope.$eval(attrs.uiDate));
            };
            var initDateWidget = function() {
                var showing = false;
                var opts = getOptions();
                // If we have a controller (i.e. ngModelController) then wire it up
                if (controller) {
                    // Set the view value in a $apply block when users selects
                    // (calling directive user's function too if provided)
                    var _onSelect = opts.onSelect || angular.noop;
                    opts.onSelect = function(value, picker) {
                        scope.$apply(function() {
                            showing = true;
                            controller.$setViewValue(element.datepicker("getDate"));
                            _onSelect(value, picker);
                            element.blur();
                        });
                    };
                    opts.beforeShow = function() {
                        showing = true;
                    };
                    opts.onClose = function(value, picker) {
                        showing = false;
                    };
                    element.on('blur', function() {
                        if (!showing) {
                            scope.$apply(function() {
                                element.datepicker("setDate", element.datepicker("getDate"));
                                controller.$setViewValue(element.datepicker("getDate"));
                            });
                        }
                    });
                    // Update the date picker when the model changes
                    controller.$render = function() {
                        var date = controller.$viewValue;
                        if (angular.isDefined(date) && date !== null && !angular.isDate(date)) {
                            throw new Error('ng-Model value must be a Date object - currently it is a ' + typeof date + ' - use ui-date-format to convert it from a string');
                        }
                        element.datepicker("setDate", date);
                    };
                }
                // If we don't destroy the old one it doesn't update properly when the config changes
                element.datepicker('destroy');
                // Create the new datepicker widget
                element.datepicker(opts);
                if (controller) {
                    // Force a render to override whatever is in the input text box
                    controller.$render();
                }
            };
            // Watch for changes to the directives options
            scope.$watch(getOptions, initDateWidget, true);
        }
    };
}]).constant('uiDateFormatConfig', '').directive('uiDateFormat', ['uiDateFormatConfig', function(uiDateFormatConfig) {
    var directive = {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var dateFormat = attrs.uiDateFormat || uiDateFormatConfig;
            if (dateFormat) {
                // Use the datepicker with the attribute value as the dateFormat string to convert to and from a string
                modelCtrl.$formatters.push(function(value) {
                    if (angular.isString(value)) {
                        return jQuery.datepicker.parseDate(dateFormat, value);
                    }
                    return null;
                });
                modelCtrl.$parsers.push(function(value) {
                    if (value) {
                        return jQuery.datepicker.formatDate(dateFormat, value);
                    }
                    return null;
                });
            } else {
                // Default to ISO formatting
                modelCtrl.$formatters.push(function(value) {
                    if (angular.isString(value)) {
                        return new Date(value);
                    }
                    return null;
                });
                modelCtrl.$parsers.push(function(value) {
                    if (value) {
                        return value.toISOString();
                    }
                    return null;
                });
            }
        }
    };
    return directive;
}]);
//ModalInstanceCtrl  Controller Block Here 
controler.controller('ModalInstanceCtrl', function($scope, $rootScope, $modalInstance, items, Scopes, $route) {
    $scope.items = items;
    $scope.data = {};
    $scope.query = [];
    $scope.selected = {
        item: $scope.items
    };
    $scope.ok = function(index) {
        $route.reload();
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
});
controler.factory('Scopes', function($rootScope) {
    var mem = {};
    return {
        store: function(key, value) {
            mem[key] = value;
        },
        get: function(key) {
            return mem[key];
        }
    };
});