var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PendingDataSchema = new Schema({
     eff_date: {
        type: Date
    },
    end_date: {
        type: Date
    },
    upd_date: {
        type: Date,
        default: Date.now
    },
    buname: {
        type: String,
        required: 'Department Name is required',
    },
     taxid: {
        type: String
    },
    ownerName: [{
        type: String,
        required: 'Owner name requried',
    }],
   categary: {
        type: String
    },    
    address1: {
        type: String,
        "default": '',
        trim: true
    },
    address2: {
        type: String,
        "default": '',
        trim: true
    },
    address3: {
        type: String,
        "default": '',
        trim: true
    },
   
    state: {
        type: String
    },
    taxregion: {
        type: String
    }, 
    city: {
        type: String
    },   
    zipcode: {
        type: String
    },
    email: {
        type: String
    },
     alternativeEmails: {
        type: String
    },
    phone:{
        type:Number
    },
    phonetype1:{
        type:String
    },
    alternatephone1:{
        type:Number
    },
    phonetype2:{
        type:String
    },
      alternatephone2:{
        type:Number
    },
    phonetype3:{
        type:String
    },
    giftcardname:{
        type:String
    },
    giftcardimage:{
        type:String
    },
    advantagecardname:{
        type:String
    },
    advantagecardimage:{
        type:String
    },
    advantage:[{
        getdata:Number,
        buy:Number
    }],
    cashbackcardname:{
        type:String
    },
    cashbackcardimage:{
        type:String
    },
    cashbackper1:{
        type:Number
    },
    cashbackvalue:{
        type:Number
    },
    pointcardname:{
        type:String
    },
    pointcardimage:{
        type:String
    },
    earn1:{
        type:Number
    },
    point1:{
        type:Number
    },
    earn2:{
        type:Number
    },
    point2:{
        type:Number
    },
    accesstype:{
        type:Boolean,
        default:false
    },
    disclaimer:{
        type:String
    },
    logo:{
        type:String
    },
    status:{
        type:String
    },
    misc_attach: [{
        type: String
    }],
   userId: {
        type:String
    },
    update_date:{
        type:Date,
        default:Date.now()
    },
    updateby:{
        type:String
    },
    savestatus:{type:String}
});

mongoose.model('HistoryData', PendingDataSchema);