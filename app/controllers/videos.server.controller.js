var Videos = require('mongoose').model('Videos');
exports.create = function(req, res, next) {
    var video = new Videos(req.body, { _id: false });
    video.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(video);
        }
    });
};

exports.list = function(req, res, next) {
    Videos.find({}, function(err, videos) {
        if (err) {
            return next(err);
        } else {
            res.json(videos);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.video);
};

exports.videoByID = function(req, res, next, id) {
    Videos.findOne({
        _id: id
    }, function(err, video) {
        if (err) {
            return next(err);
        } else {
            req.video = video;
            next();
        }
    });
};


exports.update = function(req, res, next) {
    Videos.findByIdAndUpdate(req.video.id, req.body, function(err, video) {
        if (err) {
            return next(err);
        } else {
            res.json(video);
        }
    });
};

exports.delete = function(req, res, next) {
    req.video.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.video);
        }
    })
};