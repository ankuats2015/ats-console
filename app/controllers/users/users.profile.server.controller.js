'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors.server.controller.js'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User'),
    async = require('async'),
    config = require('../../../config/config'),
    nodemailer = require('nodemailer');

var smtpTransport = nodemailer.createTransport(config.mailer.options);

/**
 * Send User
 */
exports.me = function(req, res) {
    res.json(req.user || null);
};

/**
 * add cards to custoomer account
 */

exports.addCards = function(req, res, next) {
    // Init Variables
    var user = req.user;
    var message = null;

    var cards = req.body;
    // For security measurement we remove the roles from the req.body object
    delete req.body.roles;

    if (user) {

        async.waterfall([
            function(done) {
                var check = true;
                async.each(cards, function(card, callback) {
                    //console.log(_.filter(user.cards, { 'cardNumber': card.cardNumber.toString() }));
                    if (!_.filter(user.cards, {
                            'cardNumber': card.cardNumber.toString()
                        }).length) {

                        var userCard = require('mongoose').model('userCards');
                        var newCard = new userCard({
                            cardId: card.id,
                            department: card.department,
                            cn: card.user.name,
                            cardNumber: card.cardNumber,
                            pin: card.pin
                        });

                        if (card.type == 'gift' || card.type == 'advantage') {
                            newCard.initialVal = card.price.price;
                            newCard.balance = card.price.price;
                            newCard.txt = card.price.text;
                        }

                        if (card.type == 'cashback')
                            newCard.cashback = {
                                earned: card.price.price,
                                used: 0,
                                balance: card.price.price
                            };


                        if (card.type == 'points')
                            newCard.points = {
                                earned: card.price.price,
                                used: 0,
                                balance: card.price.price
                            };

                        user.cards.push(newCard);
                        callback();
                    } else {
                        check = false;
                        res.status(400).send({
                            message: "Card number " + card.cardNumber + " already exist."
                        });

                    }




                }, function(err) {
                    if (check)
                        done(err, user);
                });

            },
            function(user, done) {

                user.save(function(err) {
                    if (err) {
                        done(err, user);
                    } else {



                        req.login(user, function(err) {
                            if (err) {
                                done(err, user);
                            } else {
                                done(user);
                            }
                        });
                    }
                });


            }
        ], function(user, err) {
            if (err) {
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.json(user);
            }

        });

    } else {
        res.status(400).send({
            message: 'User is not signed in'
        });
    }
};


/**
 * return user cards
 */

exports.getUserCards = function(req, res, next) {

    var userID = req.params.userId;
    var query = {
        '_id': userID
    };
    var populateQuery = [{
        path: 'cards.cardId',
        select: 'name image type'
    }, {
        path: 'cards.department',
        select: 'name address contact'
    }];

    User.findOne(query)
        .populate(populateQuery)
        .exec(function(err, user) {
            if (err)
                return next(err);

            if (user) {
                res.json(user.cards);
            } else {
                return res.status(200).send({
                    error: 'No records found'
                });
            }
        });
};



exports.getUserCardsByCardNumber = function(req, res, next) {

    if (req.user) {
        var cards = [];
        var userList = [];
        var cardBalance = true;
        var cardNumber = req.query.cardNumber;
        var regex = new RegExp(cardNumber, 'i');
        //console.log(cardnumber+regex);
        var query = {
            'cards.cardNumber': regex,
            'cards.department': req.user.vendorId
        };
        //   console.log("query"+query);
        var populateQuery = [{
            path: 'cards.cardId'
        }, {
            path: 'cards.department'
        }];

        User.find(query)
            .populate(populateQuery)
            .exec(function(err, users) {
                if (err)
                    return next(err);

                if (users) {
                    users.forEach(function(user, index) {

                        user.cards.forEach(function(card, index) {
                            if (card.department._id.toString() == req.user.vendorId.toString() && card.cardNumber == cardNumber) {

                                card = card.toObject();
                                delete card._id;
                                card.no = card.cardNumber;
                                card.user = user._id;
                                card.type = card.cardId.type;
                                card.email = user.email;
                                card.order = {
                                    cashback: {
                                        earned: 0,
                                        used: 0
                                    },
                                    points: {
                                        earned: 0,
                                        used: 0
                                    }
                                };
                                card.subTotal = 0;
                                card.tax = 0;
                                card.grandTotal = 0;


                                var cardExist = false;
                                cards.forEach(function(currCart, index) {
                                    if (currCart.cardNumber == card.cardNumber)
                                        cardExist = true;
                                });

                                if ((card.type == 'advantage' || card.type == 'gift') && card.balance == 0)
                                    cardBalance = false;




                                if (!cardExist)
                                    cards.push(card);

                            }
                        });

                        userList.push({
                            email: user.email,
                            phone: user.phone,
                            names: user.names
                        });

                    });

                    if (!cardBalance)
                        return res.status(200).send({
                            error: 'No Card Balance left'
                        });
                    else
                        return res.json({
                            cards: cards,
                            users: userList
                        });
                } else {
                    return res.status(200).send({
                        error: 'No records found'
                    });

                }
            });
    } else {
        return res.status(400).send({
            error: 'User not sign in.'
        });
    }
};

exports.getCardByCardNoAndUser = function(req, res, next) {

    //console.log(req.query);
    var cardNumber = req.query.cardNumber;
    var regex = new RegExp(cardNumber, 'i');
    var query = {
        _id: req.query._id,
        'cards.cardNumber': regex
    };
    var populateQuery = [{
        path: 'cards.cardId'
    }, {
        path: 'cards.department'
    }];

    User.findOne(query)
        .populate(populateQuery)
        .exec(function(err, user) {
            if (err)
                return next(err);

            if (user) {
                var check = false;
                user.cards.forEach(function(card, index) {
                    if (card.cardNumber == cardNumber) {
                        check = true;
                        return res.json(card);

                    }
                });

                if (!check) return res.json(null);
            } else {
                return res.status(200).send({
                    error: 'No records found'
                });
            }
        });
};

exports.getUserByPhone = function(req, res, next) {

    if (req.user) {

        var cards = [];
        var userList = [];
        var phone = req.query.phone;
        var query = {
            'phone': phone,
            'cards.department': req.user.vendorId
        };

        var populateQuery = [{
            path: 'cards.cardId'
        }, {
            path: 'cards.department'
        }];

        User.find(query)
            .populate(populateQuery)
            .exec(function(err, users) {
                if (err)
                    return next(err);

                if (users) {
                    users.forEach(function(user, index) {

                        user.cards.forEach(function(card, index) {
                            if (card.department._id.toString() == req.user.vendorId.toString()) {

                                card = card.toObject();

                                card.no = card.cardNumber;
                                card.user = user._id;
                                card.type = card.cardId.type;
                                card.email = user.email;
                                card.order = {
                                    cashback: {
                                        earned: 0,
                                        used: 0
                                    },
                                    points: {
                                        earned: 0,
                                        used: 0
                                    }
                                };
                                card.subTotal = 0;
                                card.tax = 0;
                                card.grandTotal = 0;



                                var cardExist = false;
                                var checkBalance = true;
                                cards.forEach(function(currCart, index) {
                                    if (currCart.cardNumber == card.cardNumber)
                                        cardExist = true;
                                });

                                if ((card.type == 'advantage' || card.type == 'gift') && card.balance == 0)
                                    checkBalance = false;

                                if (!cardExist && checkBalance)
                                    cards.push(card);



                            }
                        });

                        userList.push({
                            email: user.email,
                            phone: user.phone,
                            names: user.names
                        });

                    });

                    res.json({
                        cards: cards,
                        users: userList
                    });
                } else {
                    return res.status(200).send({
                        error: 'No records found'
                    });
                }
            });
    } else {
        return res.status(403).send({
            error: 'User not sign in'
        });
    }
};

exports.getUserById = function(req, res) {
    var id = req.params.userId;
    var populateQuery = [{
        path: 'cards.cardId'
    }, {
        path: 'cards.department'
    }];
    console.log(id);
    if (!id)
        return res.status(404).send({
            message: 'Please enter user id'
        });

    User.findOne({
        "_id": id
    }).populate(populateQuery).exec(function(err, user) {
        if (err) return next(err);
        if (!user) {
            return res.status(404).send({
                message: 'User not found'
            });
        } else {
            user.password = null;
            user.salt = null;
            res.json(user);
        }

    });
};


exports.validateCard = function(req, res, next) {

    //if (req.query)
    if (!req.query.number)
        return res.json({
            'isValid': false,
            'message': 'Please enter card number'
        });

    User.findOne({
            'cards.cardNumber': req.query.number,
            'cards.pin': req.query.pin,
        })
        .populate('cards.cardId')
        .populate('cards.department')
        .exec(function(err, user) {

            if (err)
                return res.status(400).send({
                    'isValid': false,
                    message: errorHandler.getErrorMessage(err)
                });
            else if (!user)
                return res.status(400).send({
                    'isValid': false,
                    'message': 'Invalid Card Details'
                });
            else {

                var card = {};

                user.cards.forEach(function(currentCard, index) {
                    if (currentCard.cardNumber == req.query.number)
                        card = currentCard;
                })

                return res.json({
                    'isValid': true,
                    card: card,
                    'message': 'Card is valid'
                });
            }

        });



};


/**
 * Update user details
 */
exports.update = function(req, res, next) {
    // Init Variables
    var user = req.user;
    var message = null;
    var fieldChange = req.body.fieldChange;

    // For security measurement we remove the roles from the req.body object
    delete req.body.roles;

    if (user) {

        /* init user informations */
        var userBeforeSave = {};

        async.waterfall([
            function(done) {

                User.findOne({
                    _id: user._id
                }, function(err, currentUser) {

                    userBeforeSave = currentUser;
                    done(err);
                });
            },
            function(done) {

                // Merge existing user
                user = _.extend(user, req.body);
                user.updated = Date.now();
                user.save(function(err) {
                    if (err) {
                        console.log(err);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        req.login(user, function(err) {
                            if (err) {
                                res.status(400).send(err);
                            } else {
                                done(err, user);
                                //res.json(user);
                            }
                        });
                    }
                });
            },
            function(user, done) {

                res.render('templates/user/profile/update-notification', {
                    beforeSaveUserInfo: userBeforeSave,
                    currentUserInfo: user,
                    fieldChange: fieldChange

                }, function(err, emailHTML) {
                    done(err, emailHTML, user);
                });
            },
            // If valid email, send reset email using service
            function(emailHTML, user, done) {
                var mailOptions = {
                    to: user.email,
                    from: config.mailer.from,
                    subject: 'You just updated your Account Profile Information',
                    html: emailHTML
                };
                smtpTransport.sendMail(mailOptions, function(err) {
                    if (!err) {

                        res.json(user);
                        /*res.send({
                            message: 'Profile details updated, An email has been sent to ' + user.email + '.'
                        });*/
                    } else {
                        return res.status(400).send({
                            message: 'Profile updated successfully, Failure sending email'
                        });
                    }

                    done(err);
                });
            }
        ], function(err) {
            if (err) return next(err);
        });



    } else {
        res.status(400).send({
            message: 'User is not signed in'
        });
    }

}