var company = require('../../app/controllers/company.server.controller');
module.exports = function(app) {
    app.route('/company').post(company.create)
                        .get(company.list);

    app.route('/company/:companyId').get(company.read)
                               .put(company.update)
                               .delete(company.delete);

    app.param('companyId', company.companyById);

    app.route('/company/code/:companyCode').get(company.read)
                               .put(company.update)
                               .delete(company.delete);

    app.param('companyCode', company.companyByCode);
};