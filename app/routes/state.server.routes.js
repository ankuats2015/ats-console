var states = require('../../app/controllers/state.server.controller');
module.exports = function(app) {
    app.route('/states').post(states.create)
    					.get(states.list);

    app.route('/states/:stateId').get(states.read)
    						   .put(states.update)
    						   .delete(states.delete);

    app.route('/states/add-department-to-city').post(states.addDepartmentToCity); 						   						   

   

	app.param('stateId', states.stateByID);
};