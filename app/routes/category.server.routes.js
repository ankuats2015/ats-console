var category = require('../../app/controllers/category.server.controller');
module.exports = function(app) {
    app.route('/category').post(category.create)
    					.get(category.list);

    app.route('/category/:categoryId').get(category.read)
    						   .put(category.update)
    						   .delete(category.delete);

    app.route('/category/add-state/:categoryId').put(category.addState); 						   

	app.param('categoryId', category.categoryByID);
};