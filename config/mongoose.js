var config = require('./config'),
    mongoose = require('mongoose');
module.exports = function() {
    var db = mongoose.connect(config.db);

    require('../app/models/card.server.model');
    require('../app/models/upload.image.server.model');
    require('../app/models/category.server.model');
    require('../app/models/product-category.server.model');
    require('../app/models/product.server.model');
    require('../app/models/department.server.model');
    require('../app/models/state.server.model');
    require('../app/models/videos.server.model');
    require('../app/models/cards-orders.server.model');
    require('../app/models/user.server.model');
    require('../app/models/contact-query.server.model');
    require('../app/models/company.server.model');
    require('../app/models/orders.server.model');
    require('../app/models/user-types.server.model');
    require('../app/models/controller.server.model');
    require('../app/models/image.server.model');
    require('../app/models/tax.regions.server.model');
    require('../app/models/pending.data.server.model');
    require('../app/models/pending.status.server.model');
    require('../app/models/history.server.model');

    return db;
};