var addclient=require('../../app/controllers/add.client.server.controller.js');
var customer=require('../../app/controllers/customer.profile.view.server.controller.js');
var addcontroller=require('../../app/controllers/add.controller.server.controller.js');
var updateclient=require('../../app/controllers/updateclient.server.controller.js');
var salestax=require('../../app/controllers/sales.tax.server.controller.js');
  var  upload=require('../../app/controllers/upload.Image.server.controller');



module.exports = function(app) {

app.route('/upload/url').post(addclient.uploadImage);
app.route('/controller/getFile/:accessnumber').get(addclient.getFiles);
app.route('/controller/itemRemove').post(addclient.removeItem);
app.route('/controller/getSearchResults/').post(customer.getSearchResults);
app.route('/controller/getCustomerDetail/').get(customer.getCustomerDetail);
app.route('/controller/saveController').post(addcontroller.create);
app.route('/controller/searchController/:date1/:date2/:cont').get(addcontroller.search);
app.route('/controller/postData').post(addcontroller.postData);
app.route('/controller/getData').get(addcontroller.getData);
app.route('/controller/searchUsername/').post(addcontroller.SearchUsername);
app.route('/controller/update/').post(addcontroller.updateRow);
app.route('/controller/updateData/').post(addcontroller.updateData);
app.route('/controller/getbuname/:id').get(customer.getbuname);
app.route('/addClient/getTaxRegions/:abb').get(addclient.getTaxRegions);
app.route('/addclient/saveData').post(addclient.setFormData);
app.route('/addclient/saveDepart').post(addclient.saveDepart);
app.route('/addclient/cardSave/').post(addclient.cardSave);
app.route('/addclient/getcard/:dptnu').get(addclient.getcard);
app.route('/updateclient/search/').post(updateclient.getSearch);
app.route('/updateclient/searchFull/').post(updateclient.getSearchFull);
app.route('/updateclient/searchUserDetail/:vendorid').get(updateclient.getSearchUserDetail);
// /app.route('/updateclient/searchtwo/:name/:ownerName/:tag').get(updateclient.getSearchTwo);

app.route('/updateclient/getUserdata/').post(updateclient.getUserData);
app.route('/updateclient/getDepartment/').post(updateclient.getDepartment);
app.route('/updateclient/getCard/').post(updateclient.getCard);
app.route('/salestax/searchTax/:state/:tax/:zip/:tag').get(salestax.searchTax);
app.route('/salestax/updateData/').post(salestax.updateRow);
app.route('/uploadCsv/:code/:fname/:username').post(upload.createCsv);
app.route('/controller/search').get(customer.getController);
app.route('/controller/resetPassword').post(addcontroller.resetControllerPassword);
app.route('/controller/resetPass').post(addcontroller.changePassword);
app.route('/controller/getUserDetail/:cardid/:depid').get(customer.getUserDetail);
app.route('/controller/getDeparDetail/:id/:dptnu').get(customer.getDeparDetail);
app.route('/controller/getImageName/:accessnumber').get(upload.getLogoName);
app.route('/controller/getUserInit/:username').get(addcontroller.getUserInit);
app.route('/controller/checkforzip/:state/:tax/:zip').get(addclient.getcheckforzip);
app.route('/controller/updateUserData/').post(updateclient.updateUserData);
app.route('/controller/updateDepartData/').post(updateclient.updateDepartData);
app.route('/controller/updateCardData/').post(updateclient.updateCardData);
app.route('/controller/getCompanyDetail/').get(customer.getCompanyDetail);
app.route('/controller/cities/').get(customer.getcities);
app.route('/controller/signin').post(addcontroller.signin);
//app.route('/controllers/tempdata').post(addclient.setTempData);
//app.route('/controllers/gettempdata').get(addclient.getTempData);
//app.route('/controllers/removeEntry').get(addclient.removeEntry);
app.route('/controller/saveHistoryData/').post(updateclient.saveHistory);
app.route('/controller/getHistoryData/:username/:userid').get(updateclient.getHistoryData);
app.route('/controller/getStatename/:stateabb').get(salestax.getState);
app.route('/controller/getCustDetail/:username').get(addcontroller.getCustDetail);
app.route('/controllername/getOwnername/:dptnu').get(updateclient.getOwnerName);



 

}
