var contactQuery = require('mongoose').model('contactQuery'),
    mongoose = require('mongoose'),
    Company = mongoose.model('Company'),
    config = require('../../config/config'),
    nodemailer = require('nodemailer'),
    async = require('async');


var smtpTransport = nodemailer.createTransport(config.mailer.options);


function sendQueryDetailsToAdmin(req, res, query)
{
    async.waterfall([
        function(done) {
            res.render('templates/user/contact-request-admin', {
                query: query,
                company: config.company
            }, function(err, emailHTML) {
                done(err, emailHTML, query);
            });
        },
        // If valid email, send reset email using service
        function(emailHTML, query, done) {
            var mailOptions = {
                to: config.company.email,
                from: config.mailer.from,
                subject: 'New Customer Request: '+query.requestNumber,
                html: emailHTML
            };
            smtpTransport.sendMail(mailOptions, function(err) {
                if (!err) {
                    /*res.send({
                        message: 'Request  send to admin.'
                    });*/
                } else {
                    /*return res.status(400).send({
                        message: 'Request success, Failure sending email'
                    });*/
                }
                done(err);
            });
        }
    ], function(err) {
        if (err) return next(err);
    });
}

exports.create = function(req, res, next) {
    var queryRequest = req.body;
    var cQuery = new contactQuery(queryRequest, { _id: false });
    cQuery.save(function(err) {
        if (err) {
            return next(err);
        } else {

    async.waterfall([
        function(done) {
            Company.findOne({code: 'giftcards'}, function(err, company) {
                  done(err, company);

                });
        },
        function(company, done) {
            res.render('templates/user/contact-request', {
                query: queryRequest,
                company: company
            }, function(err, emailHTML) {
                done(err, emailHTML, company);
            });
        },
        // If valid email, send reset email using service
        function(emailHTML, company, done) {
            var mailOptions = {
                to: cQuery.email,
                from: config.mailer.from,
                subject: 'Confirmation of your request to '+company.name,
                html: emailHTML
            };
            smtpTransport.sendMail(mailOptions, function(err) {
                if (!err) {
                    res.send({
                        message: 'Request success, An email has been sent to ' + cQuery.email + '.'
                    });
                } else {
                    return res.status(400).send({
                        message: 'Request success, Failure sending email'
                    });
                }

                done(err);
            });
        }, 
        function(done){
            sendQueryDetailsToAdmin(req, res, queryRequest);
            done();
        }
    ], function(err) {
        if (err) return next(err);
    });

        }
    });
};

exports.list = function(req, res, next) {
    contactQuery.find({}, function(err, queries) {
        if (err) {
            return next(err);
        } else {
            res.json(queries);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.query);
};

exports.queryByID = function(req, res, next, id) {
    contactQuery.findOne({
        _id: id
    }, function(err, query) {
        if (err) {
            return next(err);
        } else {
            req.query = query;
            next();
        }
    });
};


exports.update = function(req, res, next) {
    contactQuery.findByIdAndUpdate(req.query.id, req.body, function(err, query) {
        if (err) {
            return next(err);
        } else {
            res.json(query);
        }
    });
};

exports.delete = function(req, res, next) {
    req.query.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.query);
        }
    })
};