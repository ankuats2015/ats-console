var vendor = angular.module('vendor');
vendor.controller('registerCardController', ['$rootScope', '$scope', '$route', '$http', '$location', 'Authentication', 'applicationService', 'flash', 'myService', 'ngDialog', '$modal', 'Scopes', '$log',
  function($rootScope, $scope, $route, $http, $location, Authentication, applicationService, flash, myService, ngDialog, $modal, Scopes, $log)
  {
    $scope.user = Authentication.user;
    var formData = "";
    var showData = "";
    var editData = {};
    $scope.cards = [];
    $scope.checkbox = [];
    $scope.get = [];
    $scope.confirm = '';
    $scope.showrequired = true;
    $scope.items = [];
    $scope.issac = [];
    $scope.issgc = [];
    $scope.val = "";
    $scope.query = [];
    $scope.selectedCard = [];
    $scope.department = {};
    $scope.departmentDetail = [];
    $scope.emailexist = "";
    $scope.datas = "";
    var cards = [];
    $scope.div = [];
    $scope.div1 = [];
    $scope.issac1 = [];
    $scope.your = {
      name: '',
      email: ''
    };
    $scope.issgc1 = [];
    $scope.cardcode = false;
    $scope.query = {};
    $scope.query.contactDetails = {};
    /*  $scope.query.contactDetails.alternate = {
      number: '',
      ext: ''
    };*/
    $scope.query.uemail = {};
    $scope.query.usernames = [
    {}];
    // If user is not signed in then redirect back home
    if (!$scope.user || $scope.user.userType != 'vendor') $location.path('/');
    $scope.selectedTab = 'register';
    /*get states */
    applicationService.state.query().$promise.then(function(states)
    {
      $scope.query.states = states;
    }, function(error)
    {
      alert('Unable to get state list');
      console.log(error);
    });
    //AddMore Function  
    $scope.addNames = function($event)
    {
      $event.preventDefault();
      if ($scope.query.usernames.length < 5) $scope.query.usernames.push(
      {
        name: ""
      });
      /*rootScope.alerts.push({
        type: 'success',
        msg: 'Profile Details updated!'
      });*/
    };
    $scope.registerInit = function()
    {
      $scope.query.states = {};
      $scope.register = true;
      $scope.confirm = false;
      $scope.thanks = false;
      $scope.checkconfirm = false;
      $scope.query.contactDetails.primary = "";
      $scope.query.contactDetails.alternate = "";
      applicationService.department.get(
      {
        departmentId: $scope.user.vendorId
      }).$promise.then(function(department)
      {
        $scope.department = department;
        console.log(department);
      });
      $scope.your.name = $scope.user.fullname ? $scope.user.fullname : $scope.user.username;
      $scope.your.email = $scope.user.email;
      editData = myService.getRegCardData();
      console.log(angular.equals(
      {}, editData));
      if (!angular.equals(
        {}, editData))
      {
        console.log(editData.department);
        console.log(editData);
        $scope.datas = editData.department;
        $scope.selectedCard = editData.cards;
        $scope.cards = editData.allCards;
        $scope.query.usernames = editData.names;
        $scope.query.uemail.primary = editData.email;
        $scope.query.uemail.alternate = editData.alternativeEmails;
        $scope.query.contactDetails.primary = editData.contact[0].number;
        $scope.query.contactDetails.alternate = editData.contact[1].number;
        $scope.query.address1 = editData.address1;
        $scope.query.address2 = editData.address2;
        $scope.query.address3 = editData.address3;
        $scope.query.city = editData.city;
        $scope.query.state = {
          "name": editData.state
        };
        $scope.query.zipcode = editData.zipcode;
        $scope.cardcode = true;
        angular.forEach(editData.cards, function(val, key)
        {
          console.log(val);
          if (val.type !== 'advantage' && val.type !== 'gift')
          {
            if (val.type == 'cashback')
            {
              $scope.div.push(
              {
                msg1: val.cashbackper ? "Cashback Percentage: " + val.cashbackper + "%" : "",
                name: val.name,
                info: val.cashbackper ? "Earn " + val.cashbackper + "% Cashback for every purchase of $" + val.cashbackvalue + " and above" : card.info,
                ownername: editData.department.name
              });
            }
            if (val.type == "points")
            {
              $scope.div.push(
              {
                msg1: val.earn2 ? "Earn $" + val.earn2 + "shopping for every" + val.point2 : "",
                name: val.name,
                info: val.earn2 ? "Earn " + val.earn1 + " Point for every $" + val.point1 + " Spent points" : val.info,
                ownername: editData.department.name
              });
            }
          }
          else
          {
            if (val.type === 'advantage')
            {
              $scope.issac1.push(
              {
                name: val.name,
                info: val.info,
                price: val.amount.price,
                text: val.amount.text,
                ownername: editData.department.name
              });
            }
            if (val.type === 'gift')
            {
              if (val.amount < 1000)
              {
                $scope.issgc1.push(
                {
                  name: val.name,
                  info: val.info,
                  price: val.amount,
                  text: val.amount.text,
                  ownername: editData.department.name
                });
              }
            }
          }
        });
      }
      else
      {
        $http.get('/department/getdata/' + $scope.user.vendorId).success(function(data)
        {
          //console.log(data);
          //   $scope.department.push(data);
          $scope.datas = data;
          angular.forEach($scope.datas.cards, function(val, key)
          {
            console.log(val);
            $http.get('/cards/cardByID/' + val).success(function(response)
            {
              cards.push(response);
              $scope.cards = cards;
            });
          });
          // $scope.cards = cards;
        });
      }
      checkUserType($scope.user.roles[0]);
    }
    $scope.removeName = function($event, name)
    {
      $event.preventDefault();
      if ($scope.query.usernames.indexOf(name) !== -1) $scope.query.usernames.splice($scope.query.usernames.indexOf(name), 1);
      else alert('Something went wrong, Unable to remove recepent.');
    };
    $scope.checkEmail = function()
    {
      var data = {};
      data.names = [];
      data['cards'] = $scope.selectedCard;
      data['your'] = $scope.your;
      angular.forEach($scope.query.usernames, function(value, key)
      {
        data.names.push(
        {
          "name": value.name,
          "email": $scope.query.uemail.primary,
        });
      });
      angular.forEach(data['cards'], function(value, index)
      {
        value.recepents = data.names;
      });
      data.department = $scope.department;
      $http.get('/users/' + $scope.query.uemail.primary + '/register').success(function(response)
      {
        $scope.query.uemail.alternate = response[0].email;
        var text = "Given Email Id is already registered.";
        var value = "";
        var text2 = "Cards will be automatically added to the account under this email id";
        if (response[0].email)
        {
          ngDialog.openConfirm(
          {
            template: 'modalDialogId',
            className: 'ngdialog-theme-default',
            scope: $scope
          }).then(function(value)
          {
            console.log('Modal promise resolved. Value: ', value);
            console.log(response[0]);
            $scope.emailexist = "yes";
            var orderCards = [];
            console.log(response[0])
            $scope.query.city = response[0].city;
            $scope.query.address1 = response[0].address1;
            $scope.query.address2 = response[0].address2;
            $scope.query.address3 = response[0].address3;
            $scope.query.contactDetails.primary = response[0].phone;
            $scope.query.state = { "name": response[0].state };
            $scope.query.zipcode = response[0].zipcode;
            /*get states */
            // applicationService.state.query().$promise.then(function(states) {
            //     $scope.query.states = states;
            //     angular.forEach($scope.query.states, function(state, index) {
            //         if (state.name == $scope.query.state) $scope.user.newstate = state;
            //     });
            // }, function(error) {
            //     alert('Unable to get state list');
            //     console.log(error);
            // });
          }, function(reason)
          {
            console.log('Modal promise rejected. Reason: ', reason);
            $scope.query.uemail.alternate = null;
            $scope.query.uemail.primary = null;
          }).error(function(response)
          {
            //$scope.error = response.message;
          });
        }
      });
    };
    $scope.numberCheck = function()
    {
      var sd = $scope.query.contactDetails.primary;
      console.log(sd.length);
      var al = $scope.query.contactDetails.alternate;
      if (sd.length == 10)
      {
        console.log(sd);
        $http.get('/users/numberCheck/' + sd).success(function(response)
        {
          console.log()
          if (response.length > 0)
          {
            var modalInstance = $modal.open(
            {
              animation: $scope.animationsEnabled,
              templateUrl: 'myModalContent.html',
              controller: 'ModalInstanceCtrl',
              // size: size,
              resolve:
              {
                items: function()
                {
                  return response;
                }
              }
            });
            modalInstance.result.then(function(result)
            {
              var data = Scopes.get('ModalInstanceCtrl');
              console.log("Transaction received: ", result);
              $scope.query.city = response[0].city;
              $scope.query.address1 = response[0].address1;
              $scope.query.address2 = response[0].address2;
              $scope.query.address3 = response[0].address3;
              // $scope.query.contactDetails.primary = response[0].phone;
              $scope.query.state = {
                "name": response[0].state
              };
              $scope.query.zipcode = response[0].zipcode;
            }, function()
            {
              $log.info('Modal dismissed at: ' + new Date());
            });
          }
        });
      }
    };
    //Card Selection function
    $scope.cardSelection = function($event, card, name, info, amount)
    {
      $scope.cardcode = true;
      // console.log("card=",card);
      // console.log("card.type=",card.type);
      var checkbox = $event.target;
      if (checkbox.checked || card.type == 'gift')
      {
        console.log("check block");
        if (card.amountList.length != 2 && card.type != 'gift')
        {
          if ($scope.div1.length)
          {
            $scope.div1.push(
            {
              name: card.name,
              info: card.info,
              ownername: $scope.datas.name
            });
            console.log("$scope.div1=", $scope.div1);
          }
          else
          {
            if (card.type == 'cashback')
            {
              $scope.div.push(
              {
                msg1: card.cashbackper ? "Cashback Percentage: " + card.cashbackper + "%" : "",
                name: card.name,
                info: card.cashbackper ? "Earn " + card.cashbackper + "% Cashback for every purchase of $" + card.cashbackvalue + " and above" : card.info,
                ownername: $scope.datas.name
              });
            }
            if (card.type == "points")
            {
              $scope.div.push(
              {
                msg1: card.earn2 ? "Earn $" + card.earn2 + " " + "shopping for every" + " " + card.point2 + " " + "points" : "",
                name: card.name,
                info: card.earn2 ? "Earn " + card.earn1 + " Point(s) for every $" + card.point1 + " Spent" : card.info,
                ownername: $scope.datas.name
              });
            }
          }
        }
        if (card.type === 'gift')
        {
          $scope.issgc = [];
          console.log("hello");
          console.log("card.amount=", card.amount);
          if (card.amount == undefined)
          {
            $scope.issgc.push(
            {
              name: card.name,
              info: card.info,
              // price: amount.price ? amount.price : card.amount,
              //text: amount.text,
              ownername: $scope.datas.name
            });
            console.log("$scope.issgc=", $scope.issgc);
          }
          else if (card.amount < 1000)
          {
            $scope.issgc.push(
            {
              name: card.name,
              info: card.info,
              price: amount.price ? amount.price : card.amount,
              text: amount.text,
              ownername: $scope.datas.name
            });
          }
        }
        //console.log("we are here now");
      }
      else
      {
        if (card.type == 'advantage' || card.type == 'gift') card.amount = '';
        // console.log("we are here");
        // console.log("$scope.div=",$scope.div);
        for (var i = $scope.div.length - 1; i >= 0; i--)
        {
          if ($scope.div[i].name == name)
          {
            $scope.div.splice(i, 1);
          }
        }
        for (var i = $scope.div1.length - 1; i >= 0; i--)
        {
          if ($scope.div1[i].name == name)
          {
            $scope.div1.splice(i, 1);
          }
        }
      }
      $scope.selectedCard.push(card);
      console.log("$scope.selectedCard=", $scope.selectedCard);
    };
    $scope.showPopup = function($event, card, name, info, amount)
    {
      console.log(amount.price ? amount.price : card.amount);
      if (card.type === 'advantage')
      {
        $scope.issac.push(
        {
          name: card.name,
          info: card.info,
          price: amount.price,
          text: amount.text,
          ownername: $scope.datas.name
        });
      }
      // if (card.type === 'gift')
      // {
      //   if (card.amount < 1000)
      //   {
      //     $scope.issgc.push(
      //     {
      //       name: card.name,
      //       info: card.info,
      //       price: amount.price ? amount.price : card.amount,
      //       text: amount.text,
      //       ownername: $scope.datas.name
      //     });
      //   }
      // }
      /* $scope.cardcode = true;

        var checkbox = $event.target;

        if (checkbox.checked) {
          $scope.div.push({
            name: card.name,
            info: card.info
          });
        } else {
          for (var i = $scope.div.length - 1; i >= 0; i--) {
            if ($scope.div[i].name == name) {
              $scope.div.splice(i, 1);
            }
          }
        }
        $scope.selectedCard.push(card);*/
    };
    $scope.register = function($event)
    {
      $event.preventDefault();
      var contact = [
      {
        number: $scope.query.contactDetails.primary.number,
        type: 'primary'
      },
      {
        number: $scope.query.contactDetails.alternate.number,
        type: 'alternate'
      }];
      $scope.query.contact = contact;
      /*enable loader*/
      // $rootScope.enableLoader = true;
      var res = $http.post('/users/register');
      // var res = $http.post('/user/register');
      console.log("dnf");
    };

    function randomIntFromInterval(min, max)
    {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }
    $scope.validateandregister = function($event)
    {
      $event.preventDefault();
      var formData = {};
      var userid = $scope.query.uemail.primary.split('@');
      formData.names = [];
      formData.recepents = [];
      formData.contact = [];
      formData.alternativeEmails = [];
      formData['state'] = ($scope.query.state) ? $scope.query.state.name : '';
      formData['city'] = $scope.query.city;
      formData['department'] = $scope.department;
      formData['allCards'] = $scope.cards;
      formData['cards'] = $scope.selectedCard;
      formData['your'] = $scope.your;
      formData['username'] = userid[0] + randomIntFromInterval(1000, 9999);
      //Address Line 
      formData['address1'] = $scope.query.address1;
      formData['address2'] = $scope.query.address2;
      formData['address3'] = $scope.query.address3;
      //All the names enterd By User
      angular.forEach($scope.query.usernames, function(value, key)
      {
        formData.names.push(
        {
          "name": value.name,
          "email": $scope.query.uemail.primary,
        });
      });
      angular.forEach(formData['cards'], function(value, index)
      {
        value.recepents = formData.names;
        /*angular.forEach(value.recepents, function (recepent, rindex) {
        recepent.name = $scope.samerecepentname;
        recepent.email = $scope.samerecepentemail;
        });*/
      });
      console.log(formData.recepents);
      //Contact Details 
      formData.contact.push(
      {
        number: $scope.query.contactDetails.primary,
        type: "primary"
      });
      formData.contact.push(
      {
        number: $scope.query.contactDetails.alternate,
        type: "alternate"
      });
      formData.phone = $scope.query.contactDetails.primary;
      //Contact email Details 
      formData.email = $scope.query.uemail.primary;
      formData.alternativeEmails = $scope.query.uemail.alternate;
      formData.zipcode = $scope.query.zipcode;
      //Collect the all recepents
      angular.forEach($scope.query.usernames, function(value, key)
      {
        formData.recepents.push(
        {
          "name": value.name,
          "email": formData.email
        });
      });
      console.log(formData);
      myService.setRegCardData(formData);
      $scope.nonred = true;
      $scope.query = "";
      $scope.selectedTab = "false";
      $scope.confirm = true;
      $scope.thanks = false;
      // $location.path('/confirmationPage');
    }
    $scope.confirminit = function()
    {
      showData = myService.getRegCardData();
      console.log(showData);
      if (showData)
      {
        $scope.paypal = false;
        $scope.cards = showData.cards;
        $scope.consumer = showData.department.name;
        $scope.cardBelongsTo = showData.department.name + ', ' + showData.department.address;
        $scope.your = showData.your;
        $scope.recepents = showData.recepents;
        angular.forEach(showData.cards, function(value, index)
        {
          //  console.log()
          if (value.cardCode == "ISSGC" || value.cardCode == "ISSAC")
          {
            $scope.paypal = true;
            return false;
          };
        });
      }
      else
      {
        $location.path('/get-ready');
      }
    };
    $scope.showRedbutton = function()
    {
      if ($scope.query.address1.length > 1)
      {}
    };
    $scope.$watch('query.address1', function(newval, oldval)
    {
      if ($scope.query.address1.toString().length > 0)
      {
        angular.element('.font').attr('ng-required', true);
        /*  $scope.query.city.attr('ng-required',true);
            $scope.query.city.addClass('red');*/
        $scope.showrequired = false;
      }
      else
      {
        $scope.showrequired = true;
      }
    }, true);
    $scope.modifyOrder = function()
    {
      //  editData = myService.getRegCardData();
      $scope.selectedTab = 'register';
      $route.reload();
      /* $scope.query.uemail={};
        $scope.query.uemail.primary ="";
           $scope.query.uemail.alternate = "";//editData.alternativeEmails;
         if (editData) {
           console.log(editData);

           $scope.selectedCard = editData.cards;
           $scope.cards = editData.Allcards;
           $scope.query.usernames = editData.names;
           $scope.query.uemail.primary = editData.email;
           $scope.query.uemail.alternate = editData.alternativeEmails;
           $scope.query.contactDetails.primary = editData.contact[0].number;
           $scope.query.contactDetails.alternate = editData.contact[1].number;
           $scope.query.address1 = editData.address1;
           $scope.query.address2 = editData.address2;
           $scope.query.address3 = editData.address3;
           $scope.query.city = editData.city;
           $scope.query.state = {
               "name": editData.state
           };
           $scope.query.zipcode = editData.zipcode;
           $scope.cardcode = true;
           angular.forEach(editData.cards, function(val, key) {
               console.log(val);
               if (val.cardCode !== 'ISSAC' && val.cardCode !== 'ISSGC') {


                   $scope.div1.push({
                       name: val.name,
                       info: val.info,
                       ownername: editData.department[0].name
                   });
               } else {

                   if (val.cardCode === 'ISSAC') {

                       $scope.issac1.push({
                           name: val.name,
                           info: val.info,
                           price: val.amount.price,
                           text: val.amount.text,
                           ownername: editData.department[0].name
                       });
                   }

                   if (val.cardCode === 'ISSGC') {

                       if (val.amount < 1000) {
                           $scope.issgc1.push({
                               name: val.name,
                               info: val.info,
                               price: val.amount,
                               text: val.amount.text,
                               ownername: editData.department[0].name
                           });
                       }
                   }
               }



           });*/
      $scope.confirm = false;
      $scope.thanks = false;
    }

    function checkUserType(utype)
    {
      if (utype === 'Super User')
      {
        $scope.checkouttab = true;
        $scope.registercardtab = true;
        $scope.returnstab = true;
        $scope.salestab = true;
        $scope.addusertab = true;
        $scope.advertisetab = true;
        $scope.pricingtab = true;
        $scope.sendmailtab = true;
        $scope.aprofiletab = true;
      }
      if (utype === 'Prime User')
      {
        $scope.checkouttab = true;
        $scope.registercardtab = true;
        $scope.returnstab = true;
        $scope.salestab = false;
        $scope.addusertab = true;
        $scope.advertisetab = false;
        $scope.pricingtab = true;
        $scope.aprofiletab = false;
      }
      if (utype === 'Normal User')
      {
        $scope.checkouttab = true;
        $scope.registercardtab = true;
        $scope.returnstab = true;
        $scope.salestab = false;
        $scope.addusertab = false;
        $scope.advertisetab = false;
        $scope.pricingtab = false;
        $scope.aprofiletab = false;
      }
    }

    function randomIntFromInterval(min, max)
    {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }
    $scope.confirmAndPlaceOrder = function()
    {
      var formData = myService.getRegCardData();
      console.log(formData);
      $rootScope.enableLoader = true;
      console.log($scope.emailexist == "yes")
      if ($scope.emailexist === "yes")
      {
        $scope.loader = {
          'enable': true,
          'content': 'Please wait we are placng your order'
        };
        var orderCards = [];
        angular.forEach(formData.cards, function(value, index)
        {
          console.log(value);
          angular.forEach(value.recepents, function(recepent, index)
          {
            console.log(recepent);
            var amountObj = value.amount;
            if (value.type == 'gift') amountObj = {
              price: value.amount,
              text: ''
            }
            orderCards.push(
            {
              cardId: value._id,
              name: value.name,
              cardNumUnit: value.cardNumUnit,
              amount: amountObj,
              user:
              {
                name: recepent.name,
                email: recepent.email
              }
            });
          });
        });
        console.log(orderCards);
        console.log(formData.department);
        console.log(formData.your);
        /*enable loader */
        /*Saving order Emails */
        var orderrequest = myService.saveOrderData(
        {
          cards: orderCards,
          department: formData.department,
          user: formData.your
        });
        orderrequest.success(function(order)
        {
          console.log(order);
          formData.orderid = order.orderId;
          angular.forEach(order.cards, function(value, key)
          {
            var orderdata = {};
            orderdata.cardNumber = value.cardNumber;
            orderdata.department = order.department;
            orderdata.departmentdata = {
              "id": order.department,
              "name": order.user.name
            };
            if (value.amount)
            {
              orderdata.price = value.amount.price;
            }
            else
            {
              orderdata.price = "";
            }
            orderdata.id = value.cardId._id;
            orderdata.name = value.cardId.name;
            orderdata.pin = 0000;
            orderdata.type = value.cardId.type;
            orderdata.user = {
              "email": value.user.email,
              "name": value.user.name
            };
            var res = $http.post('/user/add_new_cards_vendor', orderdata);
            res.success(function(response)
            {
              console.log(response);
              /*Sending Recepent Emails */
              console.log(formData.department.name);
              var recepentRequest = myService.sendRecepentMail(
              {
                order: order,
                department: formData.department,
                sender: formData.department.name
              });
              recepentRequest.success(function(recepentMailResponse)
              {
                $scope.loader = {
                  'enable': true,
                  'content': 'Sending Admin Mails'
                };
                /*Sending Admin Emails */
                var adminRequest = myService.sendUserMail(
                {
                  admin: formData.your,
                  order: order,
                  department: formData.department
                });
                adminRequest.success(function(response)
                {
                  $rootScope.enableLoader = false;
                  $scope.loader = {
                    'enable': false,
                    'content': ''
                  };
                  //  $location.path('/thanks');
                  $scope.confirm = false;
                  $scope.selectedTab = "false";
                  $scope.thanks = true;
                });
              });
              // flash.setMessage({
              //     type: 'alert-danger',
              //     text: 'Cards added successfully.'
              // });
              // $route.reload();
            });
            res.error(function(response)
            {
              $scope.error = response.message;
            });
          });
        });
      }
      else
      {
        $scope.loader = {
          'enable': true,
          'content': 'Please wait. We are placing your order'
        };
        //Place Orders Cards
        var orderCards = [];
        if (formData.cards.length > 0)
        {
          for (var i = 0; i < formData.cards.length; i++)
          {
            var amountObj = formData.cards[i].amount;
            // console.log("amountObjxxx=",amountObj);
            if (formData.cards[i].type == 'gift') amountObj = {
                price: formData.cards[i].amount,
                text: ''
              }
              // console.log("amountObjxxx=",amountObj);
              // console.log("value.recepents=",value.recepents);
            $scope.rnames = [];
            for (var j = 0; j < formData.cards[i].recepents.length; j++)
            {
              $scope.rnames.push(formData.cards[i].recepents[j].name);
            }
            orderCards.push(
            {
              cardId: formData.cards[i]._id,
              name: formData.cards[i].name,
              cardNumUnit: formData.cards[i].cardNumUnit,
              amount: amountObj,
              user:
              {
                name: $scope.rnames,
                email: formData.cards[i].recepents[0].email
              }
            });
          }
        }
        else
        {
          angular.forEach(formData.cards, function(value, index)
          {
            console.log("value.recepents=", value.recepents);
            angular.forEach(value.recepents, function(recepent, rindex)
            {
              var amountObj = value.amount;
              console.log("amountObj=", amountObj);
              if (value.type == 'gift') amountObj = {
                price: value.amount,
                text: ''
              }
              console.log("amountObj=", amountObj);
              orderCards.push(
              {
                cardId: value._id,
                name: value.name,
                cardNumUnit: value.cardNumUnit,
                amount: amountObj,
                user:
                {
                  name: recepent.name,
                  email: recepent.email
                }
              });
            });
          });
        }
        // angular.forEach(formData.cards, function(value, index)
        // {
        //   console.log(value.recepents);
        //   angular.forEach(value.recepents, function(recepent, rindex)
        //   {
        //     var amountObj = value.amount;
        //     if (value.type == 'gift') amountObj = {
        //       price: value.amount,
        //       text: ''
        //     }
        //     orderCards.push(
        //     {
        //       cardId: value._id,
        //       name: value.name,
        //       cardNumUnit: value.cardNumUnit,
        //       amount: amountObj,
        //       user:
        //       {
        //         name: recepent.name,
        //         email: recepent.email
        //       }
        //     });
        //   });
        // });
        console.log(orderCards)
          //        /*enable loader 
        $rootScope.enableLoader = true;
        /*Saving order Emails */
        var orderrequest = myService.saveOrderData(
        {
          cards: orderCards,
          department: formData.department,
          user: formData.your
        });
        orderrequest.success(function(order)
        {
          $rootScope.enableLoader = true;
          formData.orderid = order.orderId;
          $scope.loader = {
            'enable': true,
            'content': 'Order done, Sending Recipient Mails'
          };
          console.log(order);
          /*Sending Recepent Emails */
          var recepentRequest = myService.sendRecepentMail(
          {
            order: order,
            department: formData.department,
            sender: formData.your
          });
          recepentRequest.success(function(recepentMailResponse)
          {
            $rootScope.enableLoader = true;
            $scope.loader = {
              'enable': true,
              'content': 'Sending Admin Mails'
            };
            /*Sending Admin Emails */
            var adminRequest = myService.sendUserMail(
            {
              admin: formData.your,
              order: order,
              department: formData.department
            });
            adminRequest.success(function(response)
            {
              $rootScope.enableLoader = false;
              $scope.loader = {
                'enable': false,
                'content': ''
              };
              //  $location.path('/thanks');
              $scope.confirm = false;
              $scope.selectedTab = "false";
              $scope.thanks = true;
            });
          });
        });
      }
    }
    $scope.thanksInit = function()
      {
        var formData = myService.getRegCardData();
        if (formData)
        {
          console.log(formData);
          $scope.cards = formData.cards;
          $scope.orderID = formData.orderid;
          $scope.consumer = formData.department.name;
          $scope.cardBelongsTo = formData.department.name + ', ' + formData.department.address;
          $scope.your = formData.your;
          myService.resetRegCardData();
        }
        else
        {
          $location.path('/vendor/checkout/register-card');
        }
      }
      //end of the block
  }
]);
//ModalInstanceCtrl  Controller Block Here 
vendor.controller('ModalInstanceCtrl', function($scope, $rootScope, $modalInstance, items, Scopes)
{
  $scope.items = items;
  $scope.data = {};
  $scope.query = [];
  $scope.selected = {
    item: $scope.items
  };
  $scope.ok = function(index)
  {
    $modalInstance.close($scope.selected.item);
    console.log(index);
    console.log($scope.selected);
    $scope.query.city = $scope.data.city;
  };
  $scope.showValue = function(index)
  {
    $scope.data = $scope.items[index];
    $scope.checkconfirm = true;
    //$scope.$broadcast('parent', 'Some data');
    $rootScope.$broadcast('datas', $scope.items[index]);
    console.log($rootScope.$broadcast('datas', $scope.items[index]));
    Scopes.store('ModalInstanceCtrl', $scope.items[index]);
  };
  $scope.cancel = function()
  {
    $modalInstance.dismiss('cancel');
  };
});
vendor.factory('Scopes', function($rootScope)
{
  var mem = {};
  return {
    store: function(key, value)
    {
      mem[key] = value;
    },
    get: function(key)
    {
      return mem[key];
    }
  };
});