var category = require('../../app/controllers/product-category.server.controller');
module.exports = function(app) {
    app.route('/product-category').post(category.create)
    					.get(category.list);

    app.route('/product-category/:categoryId').get(category.read)
    						   .put(category.update)
    						   .delete(category.delete);

    app.param('categoryId', category.categoryByID);
};