var app = angular.module('controller');
app.controller('companyController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', '$route', '$sce',
    function($rootScope, $scope, $http, $location, Authentication, myService, $route, $sce) {
        $scope.user = Authentication.user;
        $scope.detail = {};
        //scope.detail.fax={};
        $scope.data = $scope.detail;
        $scope.status = false;
        $scope.detail.contact = [];
        $scope.selected = {};



        //var detailData=$scope.detail;
        // If user is not signed in then redirect back home
        if (!$scope.user || $scope.user.userType != 'controller') $location.path('/');

        $scope.initData = function() {
            checkUserType($scope.user.userRole ? $scope.user.userRole : $scope.user.roles[0]);
            var data = $http.get('controller/getData');
            $scope.aboutushow = true;
            $scope.clients = true;
            $scope.faq = true;
            $scope.howtoget = true;
            $scope.dic_getcard = true;
            $scope.dic_security = true;
            $scope.dic_cust_bu_not = true;
            $scope.dic_acc_prof = true;
            $scope.dic_cust_expe = true;
            $scope.t_c_cust_reg = true;
            $scope.dic_cust_expe = true;
            $scope.background = "";
            $scope.t_c_bus_reg = true;
            data.success(function(data) {
                console.log(data);
                if (data) {
                    $scope.status = data.status;
                    $scope.getdata = data;
                    $scope.getdata.aboutus = data.aboutus;
                    $scope.getdata.about_us_html = $sce.trustAsHtml(data.aboutus);
                    $scope.getdata.aboutusbgcolor = data.aboutusbgcolor;

                    $scope.getdata.clients = data.clients;
                    $scope.getdata.clients_html = $sce.trustAsHtml(data.clients);
                    $scope.getdata.clientsbgcolor = data.clientsbgcolor;

                    $scope.getdata.faq = data.faq;
                    $scope.getdata.faq_html = $sce.trustAsHtml(data.faq);
                    $scope.getdata.faqbgcolor = data.faqbgcolor;

                    $scope.getdata.howtoget = data.howtoget;
                    $scope.getdata.howtoget_html = $sce.trustAsHtml(data.howtoget);
                    $scope.getdata.howtogetbgcolor = data.howtogetbgcolor;

                    $scope.getdata.dic_getcard = data.dic_getcard;
                    $scope.getdata.dic_getcard_html = $sce.trustAsHtml(data.dic_getcard);
                    $scope.getdata.dic_getcardbgcolor = data.dic_getcard_bgcolor;

                    $scope.getdata.dic_security = data.dic_security;
                    $scope.getdata.dic_security_html = $sce.trustAsHtml(data.dic_security);
                    $scope.getdata.dic_securitybgcolor = data.dic_security_bgcolor;

                    $scope.getdata.dic_cust_bu_not = data.dic_cust_bu_not;
                    $scope.getdata.dic_cust_bu_not_html = $sce.trustAsHtml(data.dic_cust_bu_not);
                    $scope.getdata.dic_cust_bu_notbgcolor = data.dic_cust_bu_not_bgcolor;

                    $scope.getdata.dic_acc_prof = data.dic_acc_prof;
                    $scope.getdata.dic_acc_prof_html = $sce.trustAsHtml(data.dic_acc_prof);
                    $scope.getdata.dic_acc_profbgcolor = data.dic_acc_prof_bgcolor;

                    $scope.getdata.dic_cust_expe = data.dic_cust_expe;
                    $scope.getdata.dic_cust_expe_html = $sce.trustAsHtml(data.dic_cust_expe);
                    $scope.getdata.dic_cust_expebgcolor = data.dic_cust_expe_bgcolor;

                    $scope.getdata.t_c_cust_reg = data.t_c_cust_reg;
                    $scope.getdata.t_c_cust_reg_html = $sce.trustAsHtml(data.t_c_cust_reg);
                    $scope.getdata.t_c_cust_regbgcolor = data.t_c_cust_reg_bgcolor;

                    $scope.getdata.t_c_bus_reg = data.t_c_bus_reg;
                    $scope.getdata.t_c_bus_reg_html = $sce.trustAsHtml(data.t_c_bus_reg);
                    $scope.getdata.t_c_bus_regbgcolor = data.t_c_bus_reg_bgcolor;
                    $scope.value = true;
                    $scope.clients = true;
                    $scope.faq = true;
                    $scope.howtoget = true;
                    $scope.dic_getcard = true;
                    $scope.dic_security = true;
                    $scope.dic_cust_bu_not = true;
                    $scope.dic_acc_prof = true;
                    $scope.dic_cust_expe = true;
                    $scope.t_c_cust_reg = true;
                    $scope.t_c_bus_reg = true;




                }


            })




        }

        $scope.saveDetail = function($event) {
            $event.preventDefault();
            //console.log($scope.detail);
            detailData = $scope.detail;

            angular.forEach($scope.detail.contact, function(value, key) {
                console.log(value);
            });

            console.log(detailData);
            var dataRes = $http.post('/controller/postData', detailData);
            dataRes.success(function(res) {
                console.log(res);
                $route.reload();
            });
            dataRes.error(function(err) {
                console.log(err);
            });

        }

        $scope.updateAboutus = function($event) {
            $event.preventDefault();
          
            $scope.aboutushow = false;
        }

        $scope.saveAboutus = function($event, data) {
            $event.preventDefault();
            var aboutusbgcolor = "";
          
            $scope.aboutushow = true;
            $scope.background = 'background-color:' + getBackcolor();
            var aboutusbgcolor = "";
            aboutusbgcolor = 'background-color:' + getBackcolor();
            var dt = {
                "data": data,
                "tag": "aboutus",
                "comp": $scope.getdata.name,
                "bgcolor": aboutusbgcolor
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.cancelAboutus = function() {
            $scope.aboutushow = true;

        }

        $scope.updateClients = function($event) {

            $event.preventDefault();
            $scope.clients = false;


        }

        $scope.saveClients = function($event, data) {
            $event.preventDefault();
            console.log("clinet");
            $scope.clients = true;
            /*var clientsbgcolor="";
            clientsbgcolor=*/

            var dt = {
                "data": data,
                "tag": "clients",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.cancelClients = function() {
            $scope.clients = true;

        }



        $scope.updatefaq = function($event) {
            $event.preventDefault();
            $scope.faq = false;
        }

        $scope.savefaq = function($event, data) {
            $event.preventDefault();
            $scope.faq = true;
            var dt = {
                "data": data,
                "tag": "faq",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.cancelfaq = function() {
            $scope.faq = true;
        }

        $scope.updatehowtoget = function($event) {
            $event.preventDefault();
            $scope.howtoget = false;
        }

        $scope.savehowtoget = function($event, data) {
            $event.preventDefault();
            $scope.howtoget = true;
            var dt = {
                "data": data,
                "tag": "howtoget",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.cancelhowtoget = function() {
            $scope.howtoget = true;
        }
        $scope.updatedic_getcard = function($event) {
            $event.preventDefault();
            $scope.dic_getcard = false;
        }

        $scope.savedic_getcard = function($event, data) {
            $event.preventDefault();
            $scope.dic_getcard = true;
            var dt = {
                "data": data,
                "tag": "dic_getcard",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.canceldic_getcard = function() {
            $scope.dic_getcard = true;
        }
        $scope.updatedic_security = function($event) {
            $event.preventDefault();
            $scope.dic_security = false;
        }

        $scope.savedic_security = function($event, data) {
            $event.preventDefault();
            $scope.dic_security = true;
            var dt = {
                "data": data,
                "tag": "dic_security",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.canceldic_security = function() {
            $scope.dic_security = true;
        }
        $scope.updatedic_cust_bu_not = function($event) {
            $event.preventDefault();
            $scope.dic_cust_bu_not = false;
        }

        $scope.savedic_cust_bu_not = function($event, data) {
            $event.preventDefault();
            $scope.dic_cust_bu_not = true;
            var dt = {
                "data": data,
                "tag": "dic_cust_bu_not",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.canceldic_cust_bu_not = function() {
            $scope.dic_cust_bu_not = true;
        }
        $scope.updatedic_acc_prof = function($event) {
            $event.preventDefault();
            $scope.dic_acc_prof = false;
        }

        $scope.savedic_acc_prof = function($event, data) {
            $event.preventDefault();
            $scope.dic_acc_prof = true;
            var dt = {
                "data": data,
                "tag": "dic_acc_prof",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.canceldic_acc_prof = function() {
            $scope.dic_acc_prof = true;
        }
        $scope.updatedic_cust_expe = function($event) {
            $event.preventDefault();
            $scope.dic_cust_expe = false;
        }

        $scope.savedic_cust_expe = function($event, data) {
            $event.preventDefault();
            $scope.dic_cust_expe = true;
            var dt = {
                "data": data,
                "tag": "dic_cust_expe",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.canceldic_cust_expe = function() {
            $scope.dic_cust_expe = true;
        }
        $scope.updatet_c_cust_reg = function($event) {
            $event.preventDefault();
            $scope.t_c_cust_reg = false;
        }

        $scope.savet_c_cust_reg = function($event, data) {
            $event.preventDefault();
            $scope.t_c_cust_reg = true;
            var dt = {
                "data": data,
                "tag": "t_c_cust_reg",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.cancelt_c_cust_reg = function() {
            $scope.t_c_cust_reg = true;
        }
        $scope.updatet_c_bus_reg = function($event) {
            $event.preventDefault();
            $scope.t_c_bus_reg = false;
        }

        $scope.savet_c_bus_reg = function($event, data) {
            $event.preventDefault();
            $scope.t_c_bus_reg = true;
            var dt = {
                "data": data,
                "tag": "t_c_bus_reg",
                "comp": $scope.getdata.name,
                "bgcolor": 'background-color:' + getBackcolor()
            };
            var result = $http.post('/controller/updateData/', dt);

            result.success(function(response) {
                console.log(response);
                $scope.initData();
            }); //console.log(data);
        }
        $scope.cancelt_c_bus_reg = function() {
            $scope.t_c_bus_reg = true;
        }

        function getBackcolor() {
            var data = angular.element.find('.wysiwyg-bgcolor');
            console.log(data);
            console.log(data[0].value);
            $scope.getdata.aboutusbgcolor = data[0].value;
            return data[0].value;
        }

        function checkUserType(utype) {
            if (utype === 'Super User' || utype === "superuser") {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = true;
                $scope.companytab = true;

            }

            if (utype === 'Admin') {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = false;
                $scope.controllertab = false;
                $scope.companytab = false;
            }


        }

    }
]);