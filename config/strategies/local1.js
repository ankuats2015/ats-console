var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    Controller = require('mongoose').model('Controller');
module.exports = function() {
    // Use local strategy
    passport.use(new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        },
        function(username, password, done) {

       
            var criteria = {username: username};//(isNaN(parseInt(username))) ?  : {phone: username, userType : userType};   
           
            Controller.findOne(criteria, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }

                return done(null, user);
            });
        }
    ));
};