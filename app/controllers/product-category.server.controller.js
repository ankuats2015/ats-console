var ProductCategory = require('mongoose').model('ProductCategory'),
    errorHandler = require('./errors.server.controller');

exports.create = function(req, res, next) {
    var category = new ProductCategory(req.body, { _id: false });
    category.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(category);
        }
    });
};

exports.list = function(req, res, next) {

    
    var query = req.query;
        query.parent = null;

      ProductCategory.find(query).populate({ path: 'childs' }).exec(function (err, categorys) {

            if (err) {
                return next(err);
            } else {
                res.json(categorys);
                /*var opts = [
                      { path: 'childs.childs' }
                    ];
                    ProductCategory.populate(categorys, opts, function(err, categorys) {

                         var opts = [
                              { path: 'childs.childs.childs' }
                            ];
                            ProductCategory.populate(categorys, opts, function(err, categorys) {
                       
                               res.json(categorys);
                            });

                       //res.json(categorys);
                    });*/


                //res.json(categorys);
            }
        });
};

exports.read = function(req, res) {
    res.json(req.category);
};

exports.categoryByID = function(req, res, next, id) {
    ProductCategory.findOne({
        _id: id
    }, function(err, category) {
        if (err) {
            return next(err);
        } else {
            req.category = category;
            next();
        }
    });
};


exports.update = function(req, res, next) {

    delete req.body._id
    ProductCategory.findByIdAndUpdate(req.category.id, req.body, function(err, category) {
        if (err) {
            return next(err);
        } else {
            res.json(category);
        }
    });
};

exports.delete = function(req, res, next) {
    req.category.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.category);
        }
    })
};
