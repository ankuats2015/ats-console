angular.module('controller').controller('custProfileController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', 'applicationService',
    function($rootScope, $scope, $http, $location, Authentication, myService, applicationService) {
        $scope.user = Authentication.user;
        // If user is not signed in then redirect back home
        if (!$scope.user || $scope.user.userType != 'controller') $location.path('/');
        $scope.selectOption = true;
        $scope.custprofiletab = true;
        $scope.datas = {};
        $scope.getuserdetail = [];
        $scope.getUserDetail = [];
        $scope.giftcards = [];
        $scope.data = [];
        $scope.checkfortable1 = true;
        $scope.checkfortable2 = false;
        $scope.checkfortable3 = false;

        //  $scope.datas.ustate={};
        // $scope.datas.taxregion={};
        $scope.getInit = function() {
            checkUserType($scope.user.userRole ? $scope.user.userRole : $scope.user.roles[0]);
            //Get the all States
            applicationService.state.query().$promise.then(function(states) {
                $scope.datas.states = states;

            }, function(error) {
                alert('Unable to get state list');
                console.log(error)
            });
        }
        $scope.getSearchResult = function(event) {
            console.log(string);
            var string = [];
            $scope.getuserdetail = [];
           //  $scope.giftcards=[];

            angular.forEach($scope.datas, function(val, key) {
                //  console.log(key+"=>"+val);
                switch (key) {
                    case "buname":
                        string.push({
                            "tag": "name",
                            "val": val
                        });
                        break;
                    case "ustate":
                        string.push({
                            "tag": "state",
                            "val": val.name
                        });
                        break;
                    case "city":
                        string.push({
                            "tag": "city",
                            "val": val
                        });
                        break;
                    case "taxregion":
                        string.push({
                            "tag": "taxregion",
                            "val": val.TaxRegionName
                        });
                        break;
                }

            });
   var result = $http.post("/controller/getSearchResults/", string);
                result.success(function(data) {
                  console.log(data)
                    if (data.length > 0) {
                        /*$scope.checkfortable1=true;
                        $scope.checkformsg=false;*/
                      console.log(data);

                        angular.forEach(data, function(vald, key) {

                          if(vald.cards.length>0)
                          {



                            angular.forEach(vald.cards, function(v, k) {
                             // console.log(v)
                                $http.get('/controller/getUserDetail/' + v._id + '/' + vald._id).success(function(subdata) {
                                 //  console.log(subdata)
                                    if (subdata.length > 0) {
                                        angular.forEach(subdata, function(values, key) {
                                         console.log(values);
                                            angular.forEach(values.cards, function(val1, key) {
                                           //   console.log(val1)
                                              if(val1.cardId==v._id)
                                              {
                                                  $scope.giftcards.push({
                                                    "type": val1.cardId.type,
                                                    "name": val1.cardId.name,
                                                    "cardNumber": val1.cardNumber,
                                                    "names": values.names,
                                                    "email": values.email,
                                                    "phone": values.phone,
                                                    "taxid": vald.userId.taxid,
                                                    "buname":vald.name,
                                                    "cardtype":v.type,
                                                    "cardname":v.name
                                                });

                                              }
                                              
                                            });

                                        });
                                        $scope.checkfortable1 = true;
                                        $scope.checkfortable2 = false;
                                        $scope.checkfortable3 = false;
                                        $scope.checkfortable4 = false;
                                        $scope.checkfortable5 = false;
                                     //  console.log($scope.giftcards)
                                        // $scope.getuserdetail.push({
                                        //     "data": val,
                                        //     "cards": $scope.giftcards,
                                        //     "customer": subdata
                                        // });
                                    } 

                                    else {
                                        $scope.checkfortable1 = false;
                                        $scope.checkfortable2 = false;
                                        $scope.checkfortable3 = true;
                                        
                                      
                                       
                                    }


                                    //console.log($scope.getuserdetail);
                                });
                            })

                        }
                        else
                        {
                           $scope.checkfortable1 = false;
                                        $scope.checkfortable2 = false;
                                        $scope.checkfortable3 = true;
                        $scope.checkformsg = true;
                        $scope.giftcards.push({"data":"No Data Found"});
                        } 

                        });
                 
                    } else {
                       $scope.checkfortable1 = false;
                                        $scope.checkfortable2 = false;
                                        $scope.checkfortable3 = true;
                        $scope.checkformsg = true;
                        $scope.giftcards.push({"data":"No Data Found"});
                    }

                });




            // if ($scope.datas.buname != undefined) {
            //     console.log(string);
             
            // } else {
            //     console.log(string);
            //     var result = $http.post("/controller/getSearchResults/", string);
            //     result.success(function(data) {
            //         if (data.length > 0) {
            //             /*$scope.checkfortable1=true;
            //             $scope.checkformsg=false;*/
            //             console.log(data);

            //             angular.forEach(data, function(val, key) {

            //                 $http.get('/controller/getUserDetail/' + val._id).success(function(subdata) {

            //                     if (subdata.length > 0) {
            //                         angular.forEach(subdata, function(values, key) {
            //                             console.log(values);
            //                             angular.forEach(values.cards, function(value, key) {
            //                                 $scope.giftcards.push({
            //                                     "type": value.cardId.type,
            //                                     "name": value.cardId.name,
            //                                     "cardNumber": value.cardNumber,
            //                                     "names": values.names,
            //                                     "email": values.email,
            //                                     "phone": values.phone,
            //                                     "taxid": values.taxid
            //                                 });

            //                             });

            //                         });
            //                         $scope.checkfortable1 = false;
            //                         $scope.checkfortable12 = false;
            //                         $scope.checkfortable13 = false;
            //                         $scope.checkfortable4 = false;
            //                         $scope.checkfortable5 = true;

            //                         $scope.getuserdetail.push({
            //                             "data": val,
            //                             "cards": $scope.giftcards,
            //                             "customer": subdata
            //                         });
            //                     } else {
            //                         $scope.checkfortable1 = false;
            //                         $scope.checkfortable2 = false;
            //                         $scope.checkfortable3 = false;
            //                         $scope.checkfortable4 = true;
            //                         $scope.checkfortable5 = false;


            //                         $scope.getuserdetail.push({
            //                             "data": val
            //                         });
            //                     }


            //                     console.log($scope.getuserdetail);
            //                 });


            //             });
            //         } else {
            //             $scope.checkfortable1 = false;
            //             $scope.checkformsg = true;
            //             $scope.errormsg = "No Data Found";
            //         }

            //     });
            // }




            /*

                     if($scope.datas.buname!=undefined)
                     {
                      console.log("hi");
               
                     }
                     else
                     {
                       var result=$http.post("/controller/getSearchResults/",string);
                      result.success(function(data)
                      {
                        console.log(data);
                        angular.forEach(data,function(val,key){
                          console.log(val._id);
                          $http.get('/controller/getDeparDetail/'+val.vendorId+'/'+val.dptnu).success(function(subdata)
                          {
                            console.log(subdata);
                            $scope.data.push({"name":val.name,"taxid":subdata.taxid,"names":subdata.names,"email":subdata.email,"phone":subdata.contact[0].number});
                          });
                        });
                      });
                     }*/


        }

        function getUserdetail(id) {

        }
        $scope.getTaxRegions = function() {
            $scope.selectOption = false;

            console.log();
            var result = $http.get('/addClient/getTaxRegions/' + $scope.datas.ustate.abb);
            result.success(function(data) {

                console.log(data);
                $scope.datas.taxregions = data;

            });
        }
        $scope.searchDepartment = function(val) {
            return $http.get('/department/search', {
                params: {
                    word: val
                }
            }).then(function(response) {
                console.log(response);
                return response.data.map(function(item) {
                    return item;
                });
            });
        };
        $scope.searchCities = function(val) {
            return $http.get('/controller/cities/', {
                params: {
                    word: val
                }
            }).then(function(response) {
                console.log(response);
                return response.data.map(function(item) {
                    console.log(item);
                    return item;
                });
            });
        };

        function checkUserType(utype) {
            if (utype === 'Super User' || utype === "superuser") {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = true;
                $scope.companytab = true;

            }

            if (utype === 'Admin') {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = false;
                $scope.companytab = false;
            }


        }


    }
]);