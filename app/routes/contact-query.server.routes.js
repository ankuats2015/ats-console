var contactQuery = require('../../app/controllers/contact-query.server.controller');
module.exports = function(app) {
    app.route('/query').post(contactQuery.create)
                        .get(contactQuery.list);

    app.route('/query/:queryId').get(contactQuery.read)
                               .put(contactQuery.update)
                               .delete(contactQuery.delete);

    app.param('queryId', contactQuery.queryByID);
};