var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var VideosSchema = new Schema({
    title: {
        type:String,
        required : 'Video title is required',
    },
    videoEmbedCode : {
        type:String,
        required : 'Embed Code is required',
    },
    created: {
        type: Date,
        default: Date.now
    },
    createdBy: {
                type: Schema.ObjectId,
                ref: 'User'
                }
});

mongoose.model('Videos', VideosSchema);