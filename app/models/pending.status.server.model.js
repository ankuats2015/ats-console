var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var PendingSatusSchema = new Schema({

	pending_status_number:{
		type:String
	},
	pending_status:{
		type:String
	}
});

mongoose.model('PendingSatus', PendingSatusSchema);