var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var uploadImageSchema = new Schema({

    filename: {
        type:String,
        required : 'Image Filename  is required',
    },

    templatename: {
        type:String,
        
    },
    type: {
        type:String,
        required : 'Type is required',
        
    },
    userid: {
        type:String,
        required : 'User Id required',
        
    },

    accessnumber: {
        type:Number,
        required : 'User Id required',
        
    },

    created: {
        type: Date,
        default: Date.now
    }
});


var sendEmailImageSchema = new Schema({
    refrenceid: {
        type:String,
        //required : '',
    },

    filename: {
        type:String,
       // required : 'Image Filename  is required',
    },

    templatename: {
        type:String,
        
    },
    type: {
        type:String,
        //required : 'Image Filename  is required',
        
    },
      tempText: {
        type:String,
        //required : 'Image Filename  is required',
        
    },
    created: {
        type: Date,
        default: Date.now
    }
});
mongoose.model('sendEmailImage', sendEmailImageSchema);
mongoose.model('uploadImage', uploadImageSchema);