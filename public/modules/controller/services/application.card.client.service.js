angular.module('controller').factory('applicationService', ['$resource',
    function($resource) {

    	var service = {
    		/* company service */
    		company : 	$resource('/company/:companyId', {
				            companyId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),

    		/* state service */
			state: 		$resource('/states/:stateId', {
					            stateId: '@_id'
					        }, {
					            update: {
					                method: 'PUT'
					            }
						}),

			/* card service */
    		card: 		$resource('/cards/:cardId', {
				            cardId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),

    		/* category service */
    		category:   $resource('/category/:categoryId', {
				            categoryId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),

    		/* product category service */
    		productCategory:   $resource('/product-category/:categoryId', {
				            categoryId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),

    		/* products service */
    		product:   $resource('/products/:productId', {
				            productId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),


    		/* department service */
    		department: $resource('/departments/:departmentId', {
				            departmentId: '@_id'
				        }, {
				        	query: {method: 'GET', isArray: false},
				            update: {
				                method: 'PUT'
				            }
				        }),

    		departmentVendor: $resource('/department/:accessCode', {
				            departmentId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),
    		
  		      departmentUser: $resource('/department/:userId', {
  		      				
				           userId: '@userId'
				        }, {
				        	query: {method: 'GET',isArray:false},
				            update: {
				                method: 'PUT'
				            }
				        }),




    		/* vendor service */
    		vendor: 	$resource('/vendors/:accessCode', {
				            accessCode: '@accessCode'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),

    		/* video service */
    		video: 		$resource('/videos/:videoId', {
				            videoId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        }),

    		/* product order service */
    		productOrder: 		$resource('/product-orders/:orderId', {
				            orderId: '@_id'
				        }, {
				            update: {
				                method: 'PUT'
				            }
				        })
    	};
    		


        return service;
    }
]);