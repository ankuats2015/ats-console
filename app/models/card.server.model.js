var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var CardSchema = new Schema({
    name: {
        type: String,
        required: 'Card Name is required',
    },
    cardCode: String,
    type: {
        type: String,
        "default": "cashback"
    },
    /* types are cashback, points, gift, advantage */
    help: {
        type: String,
        "default": ""
    },
    cardNumUnit: Number,
    info: String,
    image: String,
    amountList: [{
        price: Number,
        text: String
    }],
    earn1:Number,
    point1:Number,
    earn2:Number,
    point2:Number,
    cashbackper:Number,
    cashbackvalue:Number,

    created: {
        type: Date,
        default: Date.now
    },
    dptnu: {
        type: Number
    },
    uniqueCode:{
        type:String
    }
});

mongoose.model('Card', CardSchema);