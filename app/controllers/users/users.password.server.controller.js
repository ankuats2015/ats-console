'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User'),
	Department = mongoose.model('Department'),
	config = require('../../../config/config'),
	nodemailer = require('nodemailer'),
	async = require('async'),
	crypto = require('crypto');
	
var smtpTransport = nodemailer.createTransport(config.mailer.options);

/**
 * Forgot for reset password (forgot POST)
 */
exports.forgot = function(req, res, next) {
	async.waterfall([
		// Generate random token
		function(done) {
			
			crypto.randomBytes(6, function(err, buffer) {
						var token = buffer.toString('hex').slice(0,6);;
						done(err, token);
					});
		},
		// Lookup user by username
		function(token, done) {
			if (req.body.username) {

			
				if (req.body.type && req.body.type != 'password') 
				{
					Department.findOne({ accessCode: req.body.username })
							.populate('userId')
							.exec(function (err, department) {
							
								if (!department) {
									return res.status(400).send({
										message: 'No account with that accessCode has been found'
									});
								} else if (department.userId.provider !== 'local') {
									return res.status(400).send({
										message: 'It seems like you signed up using your ' + department.userId.provider + ' account'
									});
								} else {
									department.userId.resetPasswordToken = token;
									department.userId.resetPasswordExpires = Date.now() + 3600000; // 1 hour

									department.userId.save(function(err) {
										done(err, token, department.userId);
									});
								}


							})
				}
				else
				{
					var fieldName;
					var username = req.body.username;
					var messsageNotFound;
					if (req.body.type) 
					{
						var criteria = {username: username, userType : 'vendor'};
						messsageNotFound = "No account with that username has been found";
					}
					else
					{
						//var criteria = (username.indexOf('@') === -1) ? {phone: username} : {username: username};
						var criteria = (isNaN(parseInt(username))) ? {username: username, userType : 'customer'} : {phone: username, userType : 'customer'};    
						messsageNotFound = "No account with that Phone# or User Id has been found";
					}

		

					User.findOne(criteria, '-salt -password', function(err, user) {
						if (!user) {
							return res.status(400).send({
								message: messsageNotFound
							});
						} else if (user.provider !== 'local') {
							return res.status(400).send({
								message: 'It seems like you signed up using your ' + user.provider + ' account'
							});
						} else {
							user.resetPasswordToken = token;
							user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

							user.save(function(err) {
								done(err, token, user);
							});
						}
					});
				}

				
			} else {

				if (req.body.type && req.body.type != 'password')
					return res.status(400).send({
							message: 'Accesscode must not be blank'
						});
				else
					return res.status(400).send({
							message: 'Username field must not be blank'
						});
			}
		},
		function(token, user, done) {
			res.render('templates/password/reset-password-email', {
				name: user.username,
				appName: config.app.title,
				securityCode: token,
				//url: 'http://' + req.headers.host + '/auth/reset/' + token,
				requestType: req.body.type,
				company: config.company
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Security Code Request',
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'An email has been sent to ' + user.email + ' with further instructions.'
					});
				} else {
					return res.status(400).send({
						message: 'Failure sending email'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});
};

/**
 * Reset password GET from email token
 */
exports.validateResetToken = function(req, res) {
	console.log(req.params.token);
	User.findOne({
		resetPasswordToken: req.params.token,
		resetPasswordExpires: {
			$gt: Date.now()
		}
	}, function(err, user) {
		if (!user) {
			return res.status(400).send({
						message: 'Invalid Security Code entered. Please enter a valid security code.'
					});
			//return res.redirect('/#!/password-reset/invalid');
		}
		return res.status(200).send({
						message: 'Valid Token', userId : user.username
					});
		//res.redirect('/#!/password/reset/' + req.params.token);
	});
};

/**
 * Reset password POST from email token
 */
exports.reset = function(req, res, next) {
	// Init Variables
	var passwordDetails = req.body;

	async.waterfall([

		function(done) {
			User.findOne({
				resetPasswordToken: req.params.token,
				resetPasswordExpires: {
					$gt: Date.now()
				}
			}, function(err, user) {
				if (!err && user) {
					if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
						user.password = passwordDetails.newPassword;
						user.resetPasswordToken = undefined;
						user.resetPasswordExpires = undefined;

						user.save(function(err) {
							if (err) {
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
							} else {

								// Return authenticated user 
								res.json(user);
								done(err, user);


								/*req.login(user, function(err) {
									if (err) {
										res.status(400).send(err);
									} else {
										// Return authenticated user 
										res.json(user);

										done(err, user);
									}
								});*/
							}
						});
					} else {
						return res.status(400).send({
							message: 'Passwords do not match'
						});
					}
				} else {
					return res.status(400).send({
						message: 'Password reset token is invalid or has expired.'
					});
				}
			});
		},
		function(user, done) {
			res.render('templates/password/reset-password-confirm-email', {
				name: user.username,
				company: config.company
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Your password has been changed',
				html: emailHTML
			};

			smtpTransport.sendMail(mailOptions, function(err) {
				done(err, 'done');
			});
		}
	], function(err) {
		if (err) return next(err);
	});
};

/**
 * Change Password
 */
exports.changePassword = function(req, res) {
	// Init Variables
	var passwordDetails = req.body;

	if (req.user) {
		if (passwordDetails.newPassword) {
			User.findById(req.user.id, function(err, user) {
				if (!err && user) {
					if (user.authenticate(passwordDetails.currentPassword)) {
						if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
							user.password = passwordDetails.newPassword;

							user.save(function(err) {
								if (err) {
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									req.login(user, function(err) {
										if (err) {
											res.status(400).send(err);
										} else {
											res.send({
												message: 'Password changed successfully'
											});
										}
									});
								}
							});
						} else {
							res.status(400).send({
								message: 'Passwords do not match'
							});
						}
					} else {
						res.status(400).send({
							message: 'Current password is incorrect'
						});
					}
				} else {
					res.status(400).send({
						message: 'User is not found'
					});
				}
			});
		} else {
			res.status(400).send({
				message: 'Please provide a new password'
			});
		}
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};


/**
 * return security questions from token
 */
exports.getSecurityQuestions = function(req, res) {
	User.findOne({
		resetPasswordToken: req.params.token,
		resetPasswordExpires: {
			$gt: Date.now()
		}
	}, function(err, user) {
		if (!user) {
			return res.status(400).send({
						message: 'Invalid Security Code entered. Please enter a valid security code.'
					});
			
		}
		return res.json(user.security);
		//res.redirect('/#!/password/reset/' + req.params.token);
	});
};


/**
 * Change Password
 */
exports.resetPassword = function(req, res) {
	// Init Variables
	var passwordDetails = req.body;
	console.log(req.body.id);

	if (req.user) {
		if (passwordDetails.newPassword) {
			User.findById(req.body.id, function(err, user) {
				console.log(user);
				if (!err && user) {
					
					
						if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
							user.password = passwordDetails.newPassword;

							user.save(function(err) {
								if (err) {
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									req.login(user, function(err) {
										if (err) {
											res.status(400).send(err);
										} else {
											res.send({
												message: 'Password is successfully updated'
											});
										}
									});
								}
							});
						} else {
							res.status(400).send({
								message: 'Confirm correct password'
							});
						}
					
				} else {
					res.status(400).send({
						message: 'User is not found'
					});
				}
			});
		} else {
			res.status(400).send({
				message: 'Please provide a new password'
			});
		}
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};
