angular.module('controller').controller('salesTaxController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', 'Upload', 'applicationService',
  function($rootScope, $scope, $http, $location, Authentication, myService, Upload, applicationService)
  {
    $scope.user = Authentication.user;
    console.log($scope.user)
    // If user is not signed in then redirect back home
    if (!$scope.user || $scope.user.userType != 'controller') $location.path('/');
    $scope.data = {};
    $scope.data.states = {};
    $scope.data.taxregions = {};
    $scope.data.user = {};
    $scope.data.user.ustate = {};
    $scope.filename = "";
    $scope.edit = {};
    $scope.updata = {};
    $scope.data.user.taxregion = {};
    $scope.records = {};
    $scope.accessnumber = randomIntFromInterval(10000, 99990);
    var updata = {};
    updata.ZipCode = "";
    updata.State = "";
    updata.TaxRegionCode = "";
    updata.TaxRegionName = "";
    updata.CityRate = "";
    updata.StateRate = "";
    updata.CombinedRate = "";
    updata.CountyRate = "";
    updata.SpecialRate = "";
    $scope.orderByField = 'state';
    $scope.reverseSort = false;
    $scope.data.user.taxregion.ZipCode = "";

    function randomIntFromInterval(min, max)
    {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }
    $scope.initApp = function()
    {
      checkUserType($scope.user.userRole ? $scope.user.userRole : $scope.user.roles[0]);
      $scope.show = true;
      $scope.check = true;
      $scope.edittab = true;
      //Get the all States
      applicationService.state.query().$promise.then(function(states)
      {
        $scope.data.states = states;
      }, function(error)
      {
        alert('Unable to get state list');
        console.log(error)
      });
      $scope.editactive1 = false;
      $scope.editactive = false;
    }
    $scope.getTaxRegions = function()
    {
      $scope.show = false;
      console.log($scope.data.user.ustate)
      if ($scope.data.user.ustate != null)
      {
        var result = $http.get('/addClient/getTaxRegions/' + $scope.data.user.ustate.abb);
        result.success(function(data)
        {
          $scope.data.taxregions = data;
        });
      }
    }
    $scope.searchTax = function(event)
      {
        console.log($scope.data.user.ustate)
        state = $scope.data.user.ustate == null ? undefined : $scope.data.user.ustate.abb;
        taxregion = $scope.data.user.taxregion.TaxRegionName; //?$scope.data.user.taxregion.TaxRegionName:undefined;
        zipcode = $scope.data.user.zipcode;
        $rootScope.enableLoader = true;
        $scope.loader = {
          'enable': true,
          'content': 'Please wait we are placng your order'
        };
        var tag = "";
        if (state != undefined && taxregion == undefined && zipcode == undefined)
        {
          console.log("onlystate");
          taxregion = 0;
          zipcode = 0;
          tag = "onlystate";
          $http.get('/salestax/searchTax/' + state + '/' + taxregion + '/' + zipcode + '/' + tag).success(function(data)
          {
            console.log(data);
            if(data.length>0)
            {
            	  $scope.statename = $scope.data.user.ustate.name;
            $scope.records = data;
            $scope.orderByField = 'TaxRegionName';
            $rootScope.enableLoader = false;
            $scope.nodata = false;
            }
            else
            {
$rootScope.enableLoader = false;
              $scope.nodata = true;
            }
          
          });
        }
        if (state && taxregion && !zipcode)
        {
          console.log("nozip");
          //taxregion=0;
          zipcode = 0;
          tag = "nozip";
          $http.get('/salestax/searchTax/' + state + '/' + taxregion + '/' + zipcode + '/' + tag).success(function(data)
          {
            console.log(data);
            if(data.length>0)
            {
            	 $scope.statename = $scope.data.user.ustate.name;
            $scope.records = data;
            $rootScope.enableLoader = false;
            $scope.nodata = false;
            }
           else
           {
$rootScope.enableLoader = false;
              $scope.nodata = true;
           }
          });
        }
        if (state && taxregion && zipcode)
        {
          console.log('all')
          tag = "all";
          $http.get('/salestax/searchTax/' + state + '/' + taxregion + '/' + zipcode + '/' + tag).success(function(data)
          {
            console.log(data);
            if(data.length >0)
            {
            	$scope.statename = $scope.data.user.ustate.name;
            $scope.records = data;
            $rootScope.enableLoader = false;
            $scope.nodata = false;
            }
            else
            {
            	$rootScope.enableLoader = false;
              $scope.nodata = true;
            }
            
          });
        }
        if (state != undefined && taxregion == undefined && zipcode != undefined)
        {
          console.log('statezip')
          tag = "statezip";
          $http.get('/salestax/searchTax/' + state + '/' + taxregion + '/' + zipcode + '/' + tag).success(function(data)
          {
            console.log(data.length);
            if(data.length > 0)
            {
            	console
            $scope.statename = $scope.data.user.ustate.name;
            $scope.records = data;
            $rootScope.enableLoader = false;
             $scope.nodata = false;

            }
            else
            {
              $rootScope.enableLoader = false;
              $scope.nodata = true;
            }
            
          });
        }
        if (!state && !taxregion && zipcode)
        {
          console.log('onlyzip')
          tag = "onlyzip";
          $http.get('/salestax/searchTax/' + state + '/' + taxregion + '/' + zipcode + '/' + tag).success(function(data)
          {
            console.log(data);
            if (!isEmpty(data))
            {
            	$http.get('/controller/getStatename/'+data[0].State).success(function(state)
            	{	console.log(state)
            		 $scope.statename = state.name;
            	})

             
              $scope.records = data;
              $rootScope.enableLoader = false;
              $scope.nodata = false;
            }
            else
            {
              $rootScope.enableLoader = false;
              $scope.nodata = true;
            }
          });
        }
      }
      // $scope.editactive = function()
      // {
      //   $scope.editactive = true;
      // }
    $scope.saveEditDetail = function(event, updat, res)
    { //$scope.editactive = false;
      $rootScope.enableLoader = true;
      console.log(updat);
      updata.ZipCode = res.ZipCode;
      updata.State = res.State;
      updata.TaxRegionCode = res.TaxRegionCode;
      updata.TaxRegionName = res.TaxRegionName;
      updata.CityRate = parseFloat(updat.CityRate ? updat.CityRate : res.CityRate);
      updata.StateRate = parseFloat(updat.StateRate ? updat.StateRate : res.StateRate);
      updata.CombinedRate = parseFloat(updat.CombinedRate ? updat.CombinedRate : res.CombinedRate);
      updata.CountyRate = parseFloat(updat.CountyRate ? updat.CountyRate : res.CountyRate);
      updata.SpecialRate = parseFloat(updat.SpecialRate ? updat.SpecialRate : res.SpecialRate);
      updata.lastupdateby = $scope.user.username;
      updata.lastupdate = Date.now();
      console.log(updata);
      var det = $http.post('/salestax/updateData/', updata);
      det.success(function()
      {
        console.log("Succes");
        $rootScope.enableLoader = true;
        $scope.searchTax();
      });
    }
    $scope.uploadFiles = function(file)
    {
      $rootScope.enableLoader = true;
      var max_size = 2097000000;
      //console.log($scope.accessnumber);
      if (file != null)
      {
        //if (file.size < max_size) {
        Upload.upload(
          {
            url: '/upload/url',
            fields:
            {
              'templatename': $scope.uploadtext,
              //'accessnumber':$scope.accessnumber
            },
            file: file,
          }).progress(function(evt)
          {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
          }).success(function(data, status, headers, config)
          {
            console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
            console.log(data);
            data.templatename = config.file.name;
            data.id = $scope.user._id;
            data.type = data.type;
            data.accessnumber = $scope.accessnumber;
            $scope.filename = data.fName;
            var res = $http.post('/uploadImage', data);
            res.success(function(res, err)
            {
              console.log(res);
              if (res.name != 'undefined')
              {
                console.log(res);
                var filename = res.name;
                console.log(filename);
                var check = res.name.substr(14, 2);
                //data={'check':check,'fname':res.fname};
                var dtr = $http.post('/uploadCsv/' + check + '/' + res.fName+'/'+$scope.user.username);
                dtr.success(function(res, err)
                {
                  console.log("Hello");
                  $rootScope.enableLoader = false;
                  $scope.successmsg = "Uploaded Successfully";
                  location.reload(true);
                });
              }
            });
            res.error(function(err)
            {
              console.log(err);
            });
          }).error(function(data, status, headers, config)
          {
            console.log('error status: ' + data);
          })
          /*  } else {
              $rootScope.alerts.push({
                'msg': "Please Upload  Image greate than 2mb",
                'type': "danger"
              });
            }*/
      }
      else
      {
        /*
            $rootScope.alerts.push({
              'msg': "Please Upload Valid Image .jpg,.png",
              'type': "danger"
            });*/
      }
    };
    $scope.selectFile = function(file)
    {
      console.log(file)
      if (file)
      {
        $scope.textfile = file.name;
        $scope.check = false;
      }
      //  
    }
    var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj)
{
  // null and undefined are "empty"
  if (obj == null) return true;
  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;
  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj)
  {
    if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

    function checkUserType(utype)
    {
      if (utype === 'Super User' || utype === "superuser")
      {
        $scope.addclienttab = true;
        $scope.updateclienttab = true;
        $scope.salestaxtab = true;
        $scope.customertab = true;
        $scope.controllertab = true;
        $scope.companytab = true;
      }
      if (utype === 'Admin')
      {
        $scope.addclienttab = true;
        $scope.updateclienttab = true;
        $scope.salestaxtab = true;
        $scope.customertab = true;
        $scope.controllertab = false;
        $scope.companytab = false;
        $scope.edittab = false;
      }
    }
    $scope.editAppKey = function(field)
    {
      $scope.editing = $scope.appkeys.indexOf(field);
      $scope.newField = angular.copy(field);
    }
  }
]);