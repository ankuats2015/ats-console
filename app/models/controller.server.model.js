    var mongoose = require('mongoose'),
        crypto = require('crypto'),
        Schema = mongoose.Schema;

    var ControllerSchema = new Schema({

        username: {
            type: String,
            index: true,
            unique: true
        },
        controllername: {
            type: String
        },
        firstname: {
            type: String
        },
        lastname: {
            type: String
        },
        mi: {
            type: String
        },
        temp_pass: {
            type: String
        },
        ssn_tax: {
            type: String
        },
        userType: {
            type: String,
            trim: true,
            default: 'controller',
        },
        userRole: {
            type: String,
            trim: true
        },
        eff_date: {
            type: Date,
            default: Date.now,

        },
        end_date: {
            type: Date,
            default: Date.now,
        },
        update_date: {
            type: Date,
            default: Date.now,

        },
        last_date: {
            type: Date,
            default: "",

        },
        last_update_by: {
            type: String,
            default: "",

        },
        pass_status: {
            type: Number,
        },
        password: {
            type: String,
            validate: [
                function(password) {
                    return password && password.length > 6;
                }, 'Password should be longer'
            ]
        },
        salt: {
            type: String
        },
    });
    ControllerSchema.pre('save', function(next) {

        if (this.password) {
            this.salt = new Buffer(crypto.randomBytes(16).toString('base64'),
                'base64');
            this.password = this.hashPassword(this.password);
        }
        next();
    });
    ControllerSchema.methods.hashPassword = function(password) {
        return crypto.pbkdf2Sync(password, this.salt, 10000,
            64).toString('base64');
    };
    ControllerSchema.methods.authenticate = function(password) {
        return this.password === this.hashPassword(password);
    };

    mongoose.model('Controller', ControllerSchema);