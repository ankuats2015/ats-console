var Department = require('mongoose').model('Department');
var State=require('mongoose').model('State');
var Card = require('mongoose').model('Card');
exports.create = function(req, res, next) {
    var department = new Department(req.body);
    department.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(department);
        }
    });
};

exports.list = function(req, res, next) {
    Department.find({}, function(err, departments) {
        if (err) {
            return next(err);
        } else {
            res.json(departments);
        }
    });
};

exports.read = function(req, res) 
{ res.json(req.department);
};

exports.readData = function(req, res,next) {
   
    var id=req.params.userId;

        console.log(id);
    Department.findOne({_id:id},function(err,department)
    {
        if (err) {
            return next(err);
        } else {
           
            res.send(department);
        }
    });
    
 
};


exports.departmentByID = function(req, res, next, id) {
    /*Department.findOne({
        _id: id
    }, function(err, department) {
        if (err) {
            return next(err);
        } else {
            req.department = department;
            next();
        }
    });*/


    Department.findOne({ _id: id })
            .populate('cards')
            .exec(function (err, department) {
              if (err) {
                        return next(err);
                    } else {
                        req.department = department;
                        next();
                    }
            });


};



exports.departmentByAccessCode = function(req, res, next, code) {
    
     Department.findOne({
            accessCode: code
        }, function(err, department) {

            console.log(department);
            console.log(err);
            if (err) {
                return next(err);
            } else {
                req.department = department;
                next();
            }
        });
};

exports.update = function(req, res, next) {
    Department.findByIdAndUpdate(req.department.id, req.body, function(err, department) {
        if (err) {
            return next(err);
        } else {
            res.json(department);
        }
    });
};

exports.delete = function(req, res, next) {
    req.department.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.department);
        }
    })
};


exports.searchDepartment = function(req, res, next) {
   
    var regex = new RegExp(req.query.word, 'i');  // 'i' makes it case insensitive
    return Department.find({$or : [{name: regex}, {address: regex}]})
                     .populate('cards')
                     .exec(function (err, department) {
                      if (err) {
                                return next(err);
                            } else {
                               res.json(department);
                                
                            }
                    });;
};

exports.searchCities = function(req, res, next) {
   
    var regex = new RegExp(req.query.word, 'i');  // 'i' makes it case insensitive
    return State.find({name:regex})
                  //   .populate('cards')
                     .exec(function (err, department) {
                      if (err) {
                                return next(err);
                            } else {
                                console.log(department);
                               res.json(department);
                                
                            }
                    });;
};



exports.addCards = function(req, res, next) {

   
   var cardCode = req.body.cardCode;
   var departmentStr = req.body.department;
   if (typeof cardCode !== 'undefined' && cardCode !== null){

         /* Fetching department data */
        Department.findOne({
                name: departmentStr
            }, function(err, departmentName) {
                if (err) {
                    return next(err);
                } else {
                   console.log(departmentName);

                    Card.findOne({
                            cardCode: cardCode
                        }, function(err, card) {
                            if (err) {
                                return next(err);
                            } else {
                                departmentName.cards.push(card);
                                /*saving department name */
                                departmentName.save(function(err) {
                                    if (err) {
                                        return next(err);
                                    } else {
                                        res.json(departmentName);
                                    }
                                });
                            }
                        });
                }
            }); 
    }
    else
    {
        res.json('Please provide card Code');
    }


  
};

exports.getDepartment=function(req,res)
{

    console.log("hello");

};