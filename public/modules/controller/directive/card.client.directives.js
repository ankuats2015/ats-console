angular.module('user', [])
  .directive('fieldNotEqual', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        function validateNotEqual(value) {
          var valid = (value !== scope.$eval(attrs.fieldNotEqual));
          ngModelCtrl.$setValidity('notEqual', valid);
          return valid ? value : undefined;
        }
        
        ngModelCtrl.$parsers.push(validateNotEqual);
        ngModelCtrl.$formatters.push(validateNotEqual);
        
        scope.$watch(attrs.validateEquals, function() {
          ngModelCtrl.$setViewValue(ngModelCtrl.$viewValue);
        });
      }
    };
  })