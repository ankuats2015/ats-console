var orders = require('../../app/controllers/cards-order.server.controller');
module.exports = function(app) {
    app.route('/orders').post(orders.create)
    					.get(orders.list);

    app.route('/orders/search-card').get(orders.searchCard);

    
};