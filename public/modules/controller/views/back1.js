var controler = angular.module('controller');
controler.controller('updateClientController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', 'applicationService', 'Upload',
  function($rootScope, $scope, $http, $location, Authentication, myService, applicationService, Upload)
  {
    $scope.user = Authentication.user;
    // If user is not signed in then redirect back home
    if (!$scope.user || $scope.user.userType != 'controller') $location.path('/');
    $scope.date = {};
    $scope.view = {};
    $scope.search = {};
    $scope.search1 = {};
    $scope.formdata = {};
    $scope.showdata = {};
    $scope.showdata.data = [];
    $scope.showdata_user = {};
    $scope.showdata_depart = {};
    $scope.showdata_depart.data = {};
    $scope.showdata_user.data = {};
    $scope.updateclienttab = true;

    $scope.update = {};
    $scope.update.departdata = {};
    $scope.update.carddata = {};
    $scope.update.userdata = {};
   
    $scope.logoname = "";
    $scope.uploadata = [];
    $scope.filenames = [];
    $scope.getInit = function()
    {
      $scope.searchresults = true;
      $scope.updatepage = false;
      $scope.searchclient = true;
      if ($scope.user.userRole)
      {
        checkUserType($scope.user.userRole);
      }
      else
      {
        checkUserType($scope.user.roles[0]);
      }
      $scope.clientviewpage = false;
      $scope.block1view = true;
      $scope.block2view = false;
      $scope.giftcards = ['ISSGC1.jpg', 'ISSGC2.jpg', 'ISSGC3.jpg', 'ISSGC4.jpg', 'ISSGC5.jpg'];
      $scope.advantages = ['ISSAC1.jpg', 'ISSAC2.jpg', 'ISSAC3.jpg', 'ISSAC4.jpg', 'ISSAC5.jpg'];
      $scope.point = ['ISSMPC1.jpg', 'ISSMPC2.jpg', 'ISSMPC3.jpg', 'ISSMPC4.jpg', 'ISSMPC5.jpg'];
      $scope.cashbacks = ['ISSCBC1.jpg', 'ISSCBC2.jpg', 'ISSCBC3.jpg', 'ISSCBC4.jpg', 'ISSCBC5.jpg'];
    }
    $scope.addcard = function($event)
    {
      $event.preventDefault();
      $scope.formdata.card.advantage.datas.push(
      {
        get: "",
        buy: ""
      });
      // $rootScope.alerts.push({type: 'success', msg: 'Profile Details updated!'});
    }
    $scope.showUpdate = function()
    {
      $scope.searchresults = false;
      $scope.searchclient = false;
      $scope.updatepage = true;
      $scope.clientviewpage = false;
    }
    $scope.fillPer = function(val)
    {
      $scope.xvalue = val;
    }
    $scope.clientView = function($event, dptnu)
    {
      $event.preventDefault();
      $scope.searchresults = false;
      $scope.searchclient = false;
      $scope.updatepage = false;
      $scope.clientviewpage = true;
      $scope.date.effective = '10/9/2015';
      $scope.date.end = '12/31/2199';
      $scope.date.update = '10/9/2015';
      $scope.view.bunitname = "New Spice And Sweet";
      $scope.view.taxid = "IN417898";
      $scope.view.ownername = "Ankit";
      $scope.view.category = "Kids";
      $scope.view.addressline1 = "Type Unit:1052";
      $scope.view.addressline2 = "B I Spaze Park";
      console.log(dptnu);
      applicationService.category.query().$promise.then(function(catData)
      {
        $scope.categories = catData;
        $scope.searchBy = "direct";
      });
      applicationService.state.query().$promise.then(function(states)
      {
        $scope.formdata.states = states;
      }, function(error)
      {
        alert('Unable to get state list');
        console.log(error)
      });
      $http.get('/updateclient/getDepartment/' + dptnu).success(function(department)
      {

        console.log(department);
        $scope.update.departdata = department;
          $scope.update.departdata.ownerNames=[];

        var ownername = department.ownerName.split(',');
        console.log(ownername);
        angular.forEach(ownername,function(val,key)
        {
          $scope.update.departdata.ownerNames.push({"name":val});//ownername
        })

     //   $scope.update.departdata.ownerName=ownername;
        $scope.logoname = department.logo;
        
      });
      $http.get('/updateclient/getCard/' + dptnu).success(function(card)
      {
        console.log(card);
        $scope.update.carddata = card;
      });
      $http.get('/updateclient/getUserdata/' + dptnu).success(function(user)
      {
        console.log(user);
        $scope.update.userdata = user;
        $scope.update.userdata.categary = {
          "name": user.categary
        };
        $scope.update.userdata.state = {
          "name": user.state
        };
        console.log();
        var eff_date = new Date(user.eff_date);
        $scope.update.userdata.eff_date = ('0' + (eff_date.getMonth() + 1)).slice(-2) + "/" + ('0' + (eff_date.getDate())).slice(-2) + "/" + eff_date.getFullYear();
        // $scope.update.userdata.eff_date=
        // $scope.update.userdata.end_date=new Date(user.end_date);
        // $scope.update.userdata.upd_date=new Date(user.upd_date);
        /*   var eff_date=user.eff_date.split('T');
           var end_date=user.end_date.split('T');
           var upd_date=user.upd_date.split('T');
           var eff_date=new Date();
             
           $scope.update.userdata.eff_date=('0' + (eff_date.getMonth()+1)).slice(-2) +"/"+('0' + (eff_date.getDate())).slice(-2)+"/"+eff_date.getFullYear();
          console.log($scope.update.userdata.eff_date);
          $scope.update.userdata.end_date=new Date(end_date[0]);
           $scope.update.userdata.upd_date=new Date(upd_date[0]);
           console.log($scope.update.userdata);*/
      });
    }
    $scope.enableEditor = function(enableFlag)
    {
      $scope.success = $scope.error = null;
      $scope[enableFlag] = true;
    };
    $scope.searchDepartment = function(val)
    {
      return $http.get('/department/search',
      {
        params:
        {
          word: val
        }
      }).then(function(response)
      {
        return response.data.map(function(item)
        {
          return item;
        });
      });
    };
    $scope.onSearch = function($event)
    {
      var string = [];
      var string1 = [];
      console.log("$scope.search=", $scope.search);
      angular.forEach($scope.search, function(val, key)
      {
        console.log("key=", key);
        console.log("val=", val);
        switch (key)
        {
          case "buname":
            string.push(
            {
              "tag": "name",
              "val": val
            });
            break;
          case "access":
            string.push(
            {
              "tag": "accessCode",
              "val": val
            });
            break;
          case "confirmation":
            string.push(
            {
              "tag": "confirmation",
              "val": val
            });
            break;
          case "ownername":
            string.push(
            {
              "tag": "ownerName",
              "val": val
            });
            break;
          case "status":
            string.push(
            {
              "tag": "status",
              "val": val
            });
            break;
          case "address1":
            string.push(
            {
              "tag": "address1",
              "val": val
            });
            break;
          case "taxid":
            string.push(
            {
              "tag": "taxid",
              "val": val
            });
            break;
        }
      });
    console.log(string)
      $http.post('/updateclient/search/', string).success(function(data)
      {
        $scope.showdata.data=[];
        console.log(data)
        if(!isEmpty(data))
        {
          angular.forEach(data,function(value,key)
          {
            if(isEmpty(value.accessCode))
        {
          console.log(value.userId);
          $scope.showdata.data.push(value);
       
           $scope.usertab=false;
          $scope.departab=true;
          $scope.nodatatab=false;

        }
        else 
        {
          console.log(value.userId);

          $scope.showdata.data.push(value);
          $scope.usertab=true;
          $scope.departab=false;
          $scope.nodatatab=false;

        }
          })



        

        
       
        }
        else
        {
          $scope.usertab=false;
          $scope.departab=false;
          $scope.errormsg="No Data Found";
          $scope.nodatatab=true;
        }
        
        console.log("$scope.showdata.data=", $scope.showdata.data[0]);
        // $scope.showdata_depart.data="";
      });
    }
    $scope.getLogoName = function(file)
    {
      if (file != null)
      {
        console.log(file.name);
        $scope.uploadtext = file.name;
      }
    }
    $scope.getfilename = function(file)
    {
      if (file != null)
      {
        console.log(file.name);
        $scope.filename = file.name;
      }
    }

    function randomIntFromInterval(min, max)
    {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }
    $scope.uploadLogo = function(file)
    {
      var max_size = 2097000000;
      $scope.accessnumber = randomIntFromInterval(10000, 99990);
      console.log($scope.accessnumber);
      if (file != null)
      {
        if (file.size < max_size)
        {
          Upload.upload(
          {
            url: '/upload/url',
            fields:
            {
              'templatename': $scope.uploadtext,
            },
            file: file,
          }).progress(function(evt)
          {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
          }).success(function(data, status, headers, config)
          {
            console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
            data.templatename = $scope.uploadtext;
            data.id = $scope.user._id;
            data.accessnumber = $scope.accessnumber;
            console.log(data.fName);
            $scope.filename = data.fname;
            $scope.logoname = data.fName;
            $scope.uploaded = data.fName;
            var res = $http.post('/uploadImage', data);
            res.success(function(res, err)
            {
              console.log($scope.uploaded);
              $scope.successmsg = "Logo Update Successfully";
            });
            res.error(function(err)
            {
              console.log(err);
            });
          }).error(function(data, status, headers, config)
          {
            console.log('error status: ' + data);
          })
        }
        else
        {
          $scope.message = "Please Upload  Image greate than 2mb";
          // $rootScope.alerts.push({
          //     'msg': "Please Upload  Image greate than 2mb",
          //     'type': "danger"
          // });
        }
      }
      else
      {
        $scope.message = "Please Upload Valid Image .jpg,.png";
        // $rootScope.alerts.push({
        //     'msg': "Please Upload Valid Image .jpg,.png",
        //     'type': "danger"
        // });
      }
    };
    $scope.uploadFiles = function(file)
    {
      var max_size = 2097000000;
      $scope.accessnumber = randomIntFromInterval(10000, 99990);
      if (file != null)
      {
        if (file.size < max_size)
        {
          Upload.upload(
          {
            url: '/upload/url',
            fields:
            {
              'templatename': $scope.uploadtext,
              'accessnumber': $scope.accessnumber
            },
            file: file,
          }).progress(function(evt)
          {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
          }).success(function(data, status, headers, config)
          {
            console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
            data.templatename = config.file.name;
            data.id = $scope.user._id;
            data.accessnumber = $scope.accessnumber;
            console.log(data.fName);
            // $scope.misl.push(data.fName);
            $scope.filenames.push(data.fName);
            $scope.uploaded = data.fName;
            var res = $http.post('/uploadImage', data);
            res.success(function(res, err)
            {
              console.log($scope.uploaded);
              /*  $rootScope.alerts.push({
                      "msg": config.file.name +" Uploaded",
                      "type": "success"
                  });*/
              $scope.uploadata.push(
              {
                "templatename": config.file.name,
                "filename": $scope.uploaded
              });


              // $scope.formdata.filename.push();
              $scope.successmsg1 = "Attachemnt Successfully";
              //$scope.sendemail = false;
            });
            res.error(function(err)
            {
              console.log(err);
            });
          }).error(function(data, status, headers, config)
          {
            console.log('error status: ' + data);
          })
        }
        else
        {
          $rootScope.alerts.push(
          {
            'msg': "Please Upload  Image greate than 2mb",
            'type': "danger"
          });
        }
      }
      else
      {
        $rootScope.alerts.push(
        {
          'msg': "Please Upload Valid Image .jpg,.png",
          'type': "danger"
        });
      }
    };
    $scope.disableEditor = function(enableFlag)
    {
      $scope[enableFlag] = false;
    };
    $scope.CancelButtonDisableEditor = function(enableFlag)
    {
      $scope[enableFlag] = false;
      //  location.reload(true); 
      $scope.clientviewpage();
    };
     $scope.getTaxRegions = function() {
            console.log($scope.update.userdata.state);
           // $scope.selectOption = false;
            $http.get('/addClient/getTaxRegions/' + $scope.update.userdata.state.abb).success(function(data) {
                console.log(data);
                $scope.taxregions = data;
            })
        }
    $scope.itemRemove = function($event, filename)
    {
      var result = $http.get('/controller/itemRemoveDepart/' + filename);
      //console.log("id=",id);
      result.success(function(res)
      {
        console.log(res);
        uploadget();
      });
      result.error(function(err)
      {
        // console.log(err);
      })
    }
    $scope.save = function(enableFlag, fieldChange)
    {
      $scope.disableEditor(enableFlag);
      var userdata = "";
      var departdata = "";
      var carddata = "";
      userdata = $scope.update.userdata;
      userdata.state = $scope.update.userdata.state.name;
      userdata.taxregion=$scope.update.userdata.taxregion.TaxRegionName;
      userdata.categary = $scope.update.userdata.categary.name;
      departdata = $scope.update.departdata;
      departdata.ownerName = [];
        angular.forEach($scope.update.departdata.ownerNames, function(value, key) {
                departdata.ownerName.push(value.name);
            });
      departdata.miscattach = [];
      $scope.successmsg = "";
      angular.forEach($scope.uploadata, function(value, key)
      {
        console.log(value);
        departdata.miscattach.push(value);
      });
      departdata.logo = $scope.logoname;
      carddata = $scope.update.carddata;
      angular.forEach(carddata,function(val,key)
      {
        if(val.type=='advantage')
        {
          var al=val.amountList;
          val.amountList=[];
          angular.forEach(al,function(v,k)
          {
  var text = "Get $" + v.get + " card for $" + v.buy;
          val.amountList.push({
                        "price": v.get,
                        "text": text
                    });          })
        
        }
      })

       // if ($scope.formdata.card.advantage.type == true) {
       
      console.log("userdata=", userdata);
      console.log("carddata=", carddata);
      console.log("departdata=", departdata);
      $http.post('/controller/updateUserData/', userdata).success(function(data)
      {
        console.log("data=", data);
        $http.post('/controller/updateDepartData/', departdata).success(function(data)
        {

          console.log("data2=", data);

          var i=0;
          angular.forEach($scope.update.carddata, function(val, key)
          {
            val.i=i;
            $http.post('/controller/updateCardData/', val).success(function(data)
            {
              console.log("data3=", data);
              i++;
            })
          });
        });
      });
      /*  
       */
      /*enable loader*/
      // $rootScope.enableLoader = true;
      // $scope.user.fieldChange = fieldChange;
      // /* submit profile info */
      // var resquest = $http.post('/user/update-profile', $scope.user);
      // // console.log($scope.user.email);
      // resquest.success(function(response) {
      //     /*disable loader*/
      //     $rootScope.enableLoader = false;
      //     $rootScope.alerts.push({type: 'success', msg: 'Profile Details updated!'});
      //     $scope.disableEditor(enableFlag);
      // });
      // resquest.error(function(response) {
      //     /*disable loader*/
      //     $rootScope.enableLoader = false;
      //     $rootScope.alerts.push({type: 'danger', msg: response.message});
      // });
    angular.forEach($scope.uploadata,function(val,key)
    {
        $scope.update.departdata.misc_attach.push(val);
    })
    };
     $scope.verifyZip = function($event) {
            $event.preventDefault();
            var state = $scope.update.userdata.state.abb;
            var taxregion = $scope.update.userdata.taxregion.TaxRegionName;
            var zipcode = $scope.update.userdata.zipcode;
            $http.get('/controller/checkforzip/' + state + '/' + taxregion + '/' + zipcode).success(function(data) {
                if (data == null) {
                    $scope.staterror = "One or all of selected State, Tax Region, and Zip Code is incorrect";
                    $scope.formdata.user.zipcode = '';
                } else {
                    console.log(data);
                    $scope.staterror = "";
                }
            });
        }
         $scope.onCheck = function(value, status,card) {
            console.log(value);
            console.log(status);
            switch (value) {
                case 'gift':
                    if (status) {
                        $scope.giftcard = false;
                        $scope.giftselected = true;
                        $scope.checkforgift=false;
                        $scope.getcheck=false;



                    } else {

                        $scope.giftcard = true;
//$scope.formdata.card.giftcard = "";
                        $scope.giftselected = false;
                        $scope.checkforgift=true;

                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].name="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].image="";
                    }
                    break;
                case 'advantage':
                    if (status) {
                        $scope.advantage = false;
                        $scope.advselected = true;
                        $scope.checkforadvantage=false;
                        $scope.getcheck=false;
                    } else {
                        $scope.advantage = true;
                       
                        $scope.advselected = false;
                        $scope.checkforadvantage=true;
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].name="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].image="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].amountList=[{
                            get: "",
                            buy: "",
                            msg: ""
                        }]//{"get":"","buy":""};

                    }
                    break;
                case 'cashback':
                    if (status) {
                        $scope.cashback = false;
                        $scope.cashselected = true;
                         $scope.checkforcashback=false;
                         $scope.getcheck=false;
                    } else {
                        $scope.cashback = true;
                        $scope.formdata.card.cashback = "";
                        $scope.cashselected = false;
                         $scope.checkforcashback=true;
                          $scope.update.carddata[$scope.update.carddata.indexOf(card)].name="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].image="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].amountList=[{
                            get: "",
                            buy: "",
                            msg: ""
                        }]
                    }
                    break;
                case 'point':
                    if (status) {
                        $scope.points = false;
                        $scope.pointselected = true;
                         $scope.checkforpoints=false;
                         $scope.getcheck=false;
                    } else {
                        $scope.points = true;
                        $scope.formdata.card.points = "";
                        $scope.pointselected = false;
                         $scope.checkforpoints=true;
                          $scope.update.carddata[$scope.update.carddata.indexOf(card)].name="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].image="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].cashbackper="";
                        $scope.update.carddata[$scope.update.carddata.indexOf(card)].cashbackvalue="";

                    }
                    break;
                default:
                    break;
            }
        
        }
        $scope.addcard = function($event,card) {
            $event.preventDefault();
            $scope.update.carddata[$scope.update.carddata.indexOf(card)].amountList.push({
                get: "",
                buy: ""
            });
          }

    function checkUserType(utype)
    {
      console.log(utype);
      if (utype === 'Super User' || utype === 'superuser')
      {
        $scope.addclienttab = true;
        $scope.updateclienttab = true;
        $scope.salestaxtab = true;
        $scope.customertab = true;
        $scope.controllertab = true;
        $scope.companytab = true;
      }
      if (utype === 'Admin')
      {
        $scope.addclienttab = true;
        $scope.updateclienttab = true;
        $scope.salestaxtab = true;
        $scope.customertab = true;
        $scope.controllertab = false;
        $scope.companytab = false;
      }
    }
    $scope.addOwnerNames=function($event)
    {
      $scope.update.departdata.ownerNames.push({"name":""});
    }
     $scope.removeName=function($event,name)
    {
       $event.preventDefault();
            if ($scope.update.departdata.ownerNames.indexOf(name) !== -1) $scope.update.departdata.ownerNames.splice($scope.update.departdata.ownerNames.indexOf(name), 1);
            else alert('Something went wrong, Unable to remove recepent.');
    }

  var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}
 $scope.$watch('update.userdata.end_date', function(newval, oldval) {
        
        
            if (new Date($scope.update.userdata.end_date) < new Date($scope.update.userdata.eff_date)) {
              console.log("mlkd");
                $scope.errormsg = "End Date cannot be prior to Effective date";
                $scope.checkForNext = true;
            } else {
                $scope.checkForNext = false;
                $scope.errormsg = "";

            }
            
        });



  }
]);
// angular.module('ui.date', [])
// .constant('uiDateConfig', {})
// .directive('uiDate', ['uiDateConfig', '$timeout', function (uiDateConfig, $timeout) {
//   'use strict';
//   var options;
//   options = {};
//   angular.extend(options, uiDateConfig);
//   return {
//     require:'?ngModel',
//     link:function (scope, element, attrs, controller) {
//       var getOptions = function () {
//         return angular.extend({}, uiDateConfig, scope.$eval(attrs.uiDate));
//       };
//       var initDateWidget = function () {
//         var showing = false;
//         var opts = getOptions();
//         // If we have a controller (i.e. ngModelController) then wire it up
//         if (controller) {
//           // Set the view value in a $apply block when users selects
//           // (calling directive user's function too if provided)
//           var _onSelect = opts.onSelect || angular.noop;
//           opts.onSelect = function (value, picker) {
//             scope.$apply(function() {
//               showing = true;
//               controller.$setViewValue(element.datepicker("getDate"));
//               _onSelect(value, picker);
//               element.blur();
//             });
//           };
//           opts.beforeShow = function() {
//             showing = true;
//           };
//           opts.onClose = function(value, picker) {
//             showing = false;
//           };
//           element.on('blur', function() {
//             if ( !showing ) {
//               scope.$apply(function() {
//                 element.datepicker("setDate", element.datepicker("getDate"));
//                 controller.$setViewValue(element.datepicker("getDate"));
//               });
//             }
//           });
//           // Update the date picker when the model changes
//           controller.$render = function () {
//            // var date = controller.$viewValue;
//            var date = controller.$viewValue;
//             if ( angular.isDefined(date) && date !== null && !angular.isDate(date) ) {
//               throw new Error('ng-Model value must be a Date object - currently it is a ' + typeof date + ' - use ui-date-format to convert it from a string');
//             }
//             element.datepicker("setDate", date);
//           };
//         }
//         // If we don't destroy the old one it doesn't update properly when the config changes
//         element.datepicker('destroy');
//         // Create the new datepicker widget
//         element.datepicker(opts);
//         if ( controller ) {
//           // Force a render to override whatever is in the input text box
//           controller.$render();
//         }
//       };
//       // Watch for changes to the directives options
//       scope.$watch(getOptions, initDateWidget, true);
//     }
//   };
// }
// ])
// .constant('uiDateFormatConfig', '')
// .directive('uiDateFormat', ['uiDateFormatConfig', function(uiDateFormatConfig) {
//   var directive = {
//     require:'ngModel',
//     link: function(scope, element, attrs, modelCtrl) {
//       var dateFormat = attrs.uiDateFormat || uiDateFormatConfig;
//       if ( dateFormat ) {
//         // Use the datepicker with the attribute value as the dateFormat string to convert to and from a string
//         modelCtrl.$formatters.push(function(value) {
//           if (angular.isString(value) ) {
//             return jQuery.datepicker.parseDate(dateFormat, value);
//           }
//           return null;
//         });
//         modelCtrl.$parsers.push(function(value){
//           if (value) {
//             return jQuery.datepicker.formatDate(dateFormat, value);
//           }
//           return null;
//         });
//       } else {
//         // Default to ISO formatting
//         modelCtrl.$formatters.push(function(value) {
//           if (angular.isString(value) ) {
//             return new Date(value);
//           }
//           return null;
//         });
//         modelCtrl.$parsers.push(function(value){
//           if (value) {
//             return value.toISOString();
//           }
//           return null;
//         });
//       }
//     }
//   };
//   return directive;
// }]);