var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var CardsOrderSchema = new Schema({
    orderId: {
        type:String,
        required : 'Order Id is required',
    },
    user: {
        name : {
            type:String,
            required : 'User name requried',
        },
        email : {
            type: String,
            required : 'User email is required',
        }
    },
    department :{ type: Schema.ObjectId,
              ref: 'Department'
            },
    cards :[{ 
            cardNumber : {
                type  : String,
                required : 'Card number requried',
            },
            cardId : {
                type: Schema.ObjectId,
                ref: 'Card'
            },
            amount: {price:Number, text:String},
            useStatus: {
                type : Boolean,
                "default": false,
            } ,
            user : {
                name  : { type : String,
                          required: 'Card user is required',
                    },
                email  : {
                    type : String,
                    required: 'Card user email is required',
                }
            }
            }],
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('CardOrder', CardsOrderSchema);