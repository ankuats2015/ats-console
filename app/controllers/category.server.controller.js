var Category = require('mongoose').model('Category');
var State = require('mongoose').model('State');


var getErrorMessage = function(err) {
    if (err.errors) {
        for (var errName in err.errors) {
            if (err.errors[errName].message) 
                return err.errors[errName].message;
        }
    } else {
        return 'Unknown server error';
    }
};


exports.create = function(req, res, next) {
    var category = new Category(req.body, { _id: false });
    category.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(category);
        }
    });
};

exports.list = function(req, res, next) {
    /*Category.find({}, function(err, categorys) {
        if (err) {
            return next(err);
        } else {
            res.json(categorys);
        }
    });*/

Category.find().sort('-created').populate('stateList').exec(function(err, categorys) {
    if (err) {
        return res.status(400).send({
            message: getErrorMessage(err)
        });
    } else {
        res.json(categorys);
    }
});
};

exports.read = function(req, res) {
    res.json(req.category);
};

exports.categoryByID = function(req, res, next, id) {
    Category.findOne({
        _id: id
    }, function(err, category) {
        if (err) {
            return next(err);
        } else {
            req.category = category;
            next();
        }
    });
};


exports.update = function(req, res, next) {
    Category.findByIdAndUpdate(req.category.id, req.body, function(err, category) {
        if (err) {
            return next(err);
        } else {
            res.json(category);
        }
    });
};

exports.delete = function(req, res, next) {
    req.category.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.category);
        }
    })
};

exports.addState = function(req, res, next) {

    
    State.findOne({
        name: req.body.state
    }, function(err, state) {
        if (err) {
            return next(err);
        } else {

            /* Updating state list */
            req.category.stateList.push(state);
            req.category.save(function(err) {
                if (err) {
                    return next(err);
                } else {
                    res.json(req.category);
                }
            });
        }
    });
};