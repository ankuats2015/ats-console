var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

function setPrice(num){
    return parseFloat(num).toFixed(2);
}

var itemSchema = new Schema({  
    productId : {
                type: Schema.ObjectId,
                ref: 'Product'
            },
    name : String,
    quantity: Number,
    /* Is amount Override */
    iAO : {type: Boolean, 
        default : false
    }, 
    lb : {type: Boolean, 
        default : false
    }, 
    pUAmount : {type : Number, set: setPrice}, /*per unit Amount */
    amount: {type : Number, set: setPrice},
    tax : {type : Number, set: setPrice},
    totalAmount : {type : Number, set: setPrice}, 
});

var card = new Schema({
                user : {
                        type: Schema.ObjectId,
                        ref: 'User'
                    },
                no : String, /*Card Number */
                type: String,
                note: String,
                email: String,
                phone: Number
    });


var OrderSchema = new Schema({
    orderNum: {
        type:Number,
        required : 'Order number is required',
    },
    department : {
        type: Schema.ObjectId,
        ref: 'Department'
    },
    phone: Number, 
    cards : [card],
    items : [itemSchema],
    returnItems : [itemSchema],
    transaction : [{
        cardNum : String,
        cardType : String,
        amount : {type : Number, set: setPrice},
        type : {
            type : String,
           // enum: ['USED', 'EARNED', 'CASH', 'LOST', 'RETURN-CASH', 'RETURNTOCARD'],
            default: 'CASH'
        },
        created: {
            type: Date,
            default: Date.now
        }
    }],
    note: String,
    email: String,
    subTotal: {
        type: Number,
        set: setPrice,
        required: 'Sub total amount is required',
    },
    tax: {
        type: Number,
        set: setPrice,
        required: 'Total tax amount is required'
    },
    grandTotal: {
        type: Number,
        set: setPrice,
        required: 'Grand total amount is required'
    },
    barcode: {
        type: Number,
        required: 'Barcode is requried'
    },
    created: {
        type: Date,
        default: Date.now
    }
    
});

mongoose.model('Orders', OrderSchema);