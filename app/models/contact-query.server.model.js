var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var contactQuerySchema = new Schema({
    requestNumber: {
        type:String,
        required : 'Request Number is required',
    },
    fullname: {
        type:String,
        required : 'Full name is required',
    },
    unitname: {
        type:String,
        required : 'Business unit name is required',
    },
    email: {
        type:String,
        required : 'Email is required',
    },
    address: {
        type:String,
        required : 'Address is required',
    },
    city: {
        type:String,
        required : 'City is required',
    },
    state: {
        type:String,
        required : 'State is required',
    },
    zip: {
        type:String,
        required : 'Zip Code is required',
    },
    contact : [{
        number : {type: Number},
        type : {type: String},
        ext : {type: Number}
    }],
    contactfor: {
        type:String,
        required : 'Contact for details is required',
    },
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('contactQuery', contactQuerySchema);