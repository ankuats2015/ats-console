var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var TaxRegionSchema = new Schema({
	State:{
		type:String
	},
	
	ZipCode:{type:String},
	TaxRegionName:{type:String},
	TaxRegionCode:{type:String},
	CombinedRate:{type:Number},
	StateRate:{type:Number},
	CountyRate:{type:Number},
	CityRate:{type:Number},
	SpecialRate:{type:Number},
	lastupdateby:{type:String},
	lastupdate:{type:Date}
   
});

mongoose.model('TaxRegion', TaxRegionSchema);