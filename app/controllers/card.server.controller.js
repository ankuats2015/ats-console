var Card = require('mongoose').model('Card');
var UserTypes = require('mongoose').model('userType');
exports.create = function(req, res, next) {
    var card = new Card(req.body, { _id: false });
    card.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(card);
        }
    });
};

exports.list = function(req, res, next) {
    Card.find({}, function(err, cards) {
        if (err) {
            return next(err);
        } else {
            res.json(cards);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.card);
};

exports.cardByID = function(req, res, next, id) {
    Card.findOne({
        _id: id
    }, function(err, card) {
        if (err) {
            return next(err);
        } else {
            req.card = card;
            next();
        }
    });
};

exports.readData = function(req, res) {
   
    var id=req.params.id;
   
    Card.findOne({_id:id},function(err,card)
    {
        if (err) {
            return next(err);
        } else {
           
            res.send(card);
        }
    });
    
 
};


exports.update = function(req, res, next) {
    Card.findByIdAndUpdate(req.card.id, req.body, function(err, card) {
        if (err) {
            return next(err);
        } else {
            res.json(card);
        }
    });
};

exports.delete = function(req, res, next) {
    req.card.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.card);
        }
    })
};

exports.addAmount = function(req, res, next) {


    var cardCode = req.body.cardCode;
   if (typeof cardCode !== 'undefined' && cardCode !== null){

         /* Fetching department data */
        Card.findOne({
                cardCode: cardCode
            }, function(err, cardData) {
                if (err) {
                    return next(err);
                } else {
                    
                    cardData.amountList.push(req.body.amountdata);
                        res.json(cardData);
                    /* card.save(function(err) {
                                if (err) {
                                    return next(err);
                                } else {
                                    res.json(card);
                                }   
                            });*/

                }
            });
    }
    else
    {

    }
};

