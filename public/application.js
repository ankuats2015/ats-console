var mainApplicationModuleName = 'GiftCard';
var mainApplicationModule = angular.module(mainApplicationModuleName, ['ui.bootstrap', 'ngResource', 'ngRoute','controller','chart.js','ngFileUpload','ngDialog','colorpicker.module', 'wysiwyg.module','ui.date']);

mainApplicationModule.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('!');
}]);

if (window.location.hash === '#_=_') window.location.hash = '';

angular.element(document).ready(function() {
    angular.bootstrap(document, [mainApplicationModuleName]);
});