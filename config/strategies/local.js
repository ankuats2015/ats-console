var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('mongoose').model('User');
    Control=require('mongoose').model('Controller');
module.exports = function() {
    // Use local strategy
    passport.use('user-local',new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        },
        function(username, password, done) {

       
            var criteria = {username: username};//(isNaN(parseInt(username))) ?  : {phone: username, userType : userType};   
           
            User.findOne(criteria, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }

                return done(null, user);
            });

       
        }
    ));
     // Use local strategy
    passport.use('control-local',new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        },
        function(username, password, done) {

       // console.log(password);
            var criteria = {username: username};//(isNaN(parseInt(username))) ?  : {phone: username, userType : userType};   
         //  console.log(criteria);
            Control.findOne(criteria, function(err, control) {
                //console.log(control);
                if (err) {
                    return done(err);
                }
                if (!control) {
                    return done(null, false, {
                        message: 'Unknown controller or invalid password'
                    });
                }
                if (!control.authenticate(password)) {
                    return done(null, false, {
                        message: 'Unknown controller or invalid password'
                    });
                }

                return done(null, control);
            });

       
        }
    ));
};