var productOrder = require('../../app/controllers/orders.server.controller');
module.exports = function(app) {
    app.route('/product-orders').post(productOrder.create)
                        .get(productOrder.list);

    app.route('/product-orders/:orderId').get(productOrder.read)
                               .put(productOrder.update)
                               .delete(productOrder.delete);

    app.param('orderId', productOrder.orderByID);

    app.route('/product-order/search').get(productOrder.orderByQuery);
    app.route('/product-orders/send-mail').post(productOrder.sendOrderMail);

    app.route('/card-tranaction').get(productOrder.getCardTranactions);
};