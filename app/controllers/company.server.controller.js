var Company = require('mongoose').model('Company');
exports.create = function(req, res, next) {
    var cmpany = new Company(req.body, { _id: false });
    cmpany.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(cmpany);
        }
    });
};

exports.list = function(req, res, next) {
    Company.find({}, function(err, companies) {
        if (err) {
            return next(err);
        } else {
            res.json(companies);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.company);
};

exports.companyById = function(req, res, next, id) {
    Company.findOne({
        _id: id
    }, function(err, company) {
        if (err) {
            return next(err);
        } else {
            req.company = company;
            next();
        }
    });
};

exports.companyByCode = function(req, res, next, code) {
    Company.findOne({
        code: id
    }, function(err, company) {
        if (err) {
            return next(err);
        } else {
            req.company = company;
            next();
        }
    });
};

exports.update = function(req, res, next) {
    Company.findByIdAndUpdate(req.company.id, req.body, function(err, company) {
        if (err) {
            return next(err);
        } else {
            res.json(company);
        }
    });
};

exports.delete = function(req, res, next) {
    req.company.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.company);
        }
    })
};