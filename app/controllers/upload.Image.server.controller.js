var Upload = require('mongoose').model('uploadImage'),
  taxregion = require('mongoose').model('TaxRegion'),
  sendEmail = require('mongoose').model('sendEmailImage'),
  Users = require('mongoose').model('User'),
  mongoose = require('mongoose'),
  Company = mongoose.model('Company'),
  config = require('../../config/config'),
  nodemailer = require('nodemailer'),
  csv = require('fast-csv');
async = require('async');
var smtpTransport = nodemailer.createTransport(config.mailer.options);

function sendQueryDetailsToAdmin(req, res, query)
{
  async.waterfall([
    function(done)
    {
      res.render('templates/send-email-background',
      {
        filename: reqData.filename,
        textData: reqData.textData,
        templatename: reqData.templatename,
      }, function(err, emailHTML)
      {
        done(err, emailHTML);
      });
    },
    // If valid email, send reset email using service
    function(emailHTML, done)
    {
      var mailOptions = {
        to: config.company.email,
        from: "ankitmca2009_12@yahoo.in",
        subject: 'New Customer Request: ' + query.requestNumber,
        html: emailHTML
      };
      smtpTransport.sendMail(mailOptions, function(err)
      {
        if (!err)
        {
          /*res.send({
              message: 'Request  send to admin.'
          });*/
        }
        else
        {
          /*return res.status(400).send({
              message: 'Request success, Failure sending email'
          });*/
        }
        done(err);
      });
    }
  ], function(err)
  {
    if (err) return next(err);
  });
}
exports.create = function(req, res, next)
{
  var queryRequest = req.body;
  var cQuery = new Upload(
  {
    filename: req.body.fName,
    templatename: req.body.templatename,
    type: req.body.type,
    userid: req.body.id,
    accessnumber: req.body.accessnumber,
  });
  cQuery.save(function(err)
  {
    if (err)
    {
      return next(err);
    }
    else
    {
      return res.json(req.body);
    }
  });
};
exports.getUpload = function(req, res)
{
  Upload.find().exec(function(err, data)
  {
    if (err) throw err;
    else
    {
      return res.json(data);
    }
  });
};
exports.getTemplatename = function(req, res)
{
  Upload.find(
  {
    filename: req.params.tmp
  }).exec(function(err, data)
  {
    if (err) return err;
    else
    {
      return res.json(data);
    }
  });
};
exports.getAllEmail = function(req, res)
  {
    Users.find(
    {},
    {
      "email": 1,
      "_id": 0
    }, function(err, data)
    {
      return res.json(data);
    })
  }
  //sending mail to customers
exports.sendEmail = function(req, res)
{
  var reqData = req.body;
  var send = new sendEmail(
  {
    referenceid: reqData._id,
    filename: reqData.filename,
    templatename: reqData.templatename,
    type: reqData.type,
    tempText: reqData.tempText,
  });
  var temptext = "";
  console.log(reqData);
  send.save(function(err)
  {
    if (err)
    {
      return next(err);
    }
  });
  emails = reqData.emaildata;
  /*      for(var obj in emails)
        {
           console.log(emails[obj].email);
        }
       */
  async.forEach(emails, function(email, callback)
  { //The second argument (callback) is the "task callback" for a specific messageId
    // var action = trafficLightActions[color];
    //Play around with the color and action
    async.waterfall([
      function(done)
      {
        console.log(reqData.tempText);
        var text = reqData.tempText.replace(/\n/, '<br>');
        console.log(text);
        res.render('templates/send-email-background',
        {
          filenames: reqData.filename,
          textData: text ? text : '',
          templatename: reqData.templatename,
          siteurl: config.url
        }, function(err, emailHTML)
        {
          done(err, emailHTML);
        });
      },
      // If valid email, send reset email using service
      function(emailHTML, done)
      {
        console.log(email.email);
        var mailOptions = {
          to: email.email,
          from: "ankitmca2009_12@yahoo.in",
          subject: 'Message',
          html: emailHTML
        };
        smtpTransport.sendMail(mailOptions, function(err, data)
        {
          /*if (!err) {
             return res.json(data);
          } else {
              return res.json(err);
          }*/
          done(err);
        });
      }
    ], function(err)
    {
      if (err) callback(err);
      else callback();
      //
    });
  }, function(err)
  {
    if (err) return console.error(err);
    else
    {
      return res.send('complete');
    }
  });
  /*    for(var obj in emails)
      {
        
      }*/
}
exports.remImage = function(req, res)
{
  console.log(req.params.name);
  Upload.remove(
  {
    'filename': req.params.name
  }).exec(function(err, data)
  {
    if (err)
    {
      return res.json(err);
    }
    else
    {
      return res.json(
      {
        "msg": "Remove Done"
      });
    }
  });
};
exports.createCsv = function(req, res)
{
  //console.log(req.body);
  console.log(req.params.code);
  console.log(req.params.fname);
  console.log(req.params.username);
  //   var csv = Assets.getText('../public/upload/'+req.params.fname+'.csv');
  taxregion.remove(
  {
    'State': req.params.code
  }).exec(function(err, data)
  {
    if (err)
    {
      throw err;
    }
    else
    {
      var csv = require("fast-csv");
      csv.fromPath('./public/modules/controller/img/logo/' + req.params.fname,
      {
        headers: true
      }).on("data", function(data)
      {
        //console.log(data);
        data.lastupdate=Date.now();
        data.lastupdateby=req.params.username;
        var details = new taxregion(data);
        details.save(function(saveErr, savedetail)
        {
          if (saveErr)
          {
            console.log(saveErr)
          }
          else
          {
            // console.log("done");
          }
        });
      }).on("end", function()
      {
        res.json(
        {
          'msg': 'Done'
        });
      });
    }
  });
}
exports.getLogoName = function(req, res)
{
  Upload.findOne(
  {
    accessnumber: req.params.accessnumber
  }).exec(function(err, data)
  {
    if (err)
    {
      throw err;
    }
    else
    {
      res.json(data);
    }
  });
}