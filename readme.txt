Please follow below steps one by one to make it work.

1) Import get-card Database using "mongorestore --db get-card db/get-card/"
2) Install Node dependency using "npm install". If you get Error to update your dependency module then please check this "https://github.com/birchroad/node-barc" and check Dependencies Section. Because for bar code some system related dependency requried. I have installed the bar code module on my UBUNTU 14.10 using these commands.
/* Bar Code */
Cairo sudo apt-get install libcairo2-dev
Pango sudo apt-get install libpango1.0-dev 
sudo apt-get install libjpeg-dev
npm install canvas
npm install barc
/* Bar Code */

3) Install bower module for front-end dependency using "npm install -g bower"
4) Run Command "bower install", this will install the frontend modules like angular.
5) Update User email and password in "Config/env/development.js" file.
6) If you did all without getting any error. Run "node server" Command to run site.

Thanks





