var videos = require('../../app/controllers/videos.server.controller');
module.exports = function(app) {
    app.route('/videos').post(videos.create)
                        .get(videos.list);

    app.route('/videos/:videoId').get(videos.read)
                               .put(videos.update)
                               .delete(videos.delete);

    app.param('videoId', videos.videoByID);
};