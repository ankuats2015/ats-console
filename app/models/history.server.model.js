    var mongoose = require('mongoose'),
      crypto = require('crypto'),
      Schema = mongoose.Schema;
    var HistorySchema = new Schema(
    {
      update_date:
      {
        type: Date,
        default: Date.now,
      },
      updateby:
      {
        type: String
      },
      before: [{tag:String,data:String}],
      after: [
      {tag:String,data:String}],
      userid:
      {
        type: Schema.ObjectId,
        ref: 'User'
      },
      username:
      {
        type: String
      }
    });
    mongoose.model('History', HistorySchema);