var config = require('./config'),
	express = require('express'),
	morgan = require('morgan'),
 	compress = require('compression'),
 	bodyParser = require('body-parser'),
 	methodOverride = require('method-override'),
 	session = require('express-session'),
 	cookieParser = require('cookie-parser'),
 	flash = require('connect-flash'),
 	multipart = require('connect-multiparty'),
 	// /limit=require('limit');
 	passport = require('passport');
 	



module.exports = function() {
 var app = express();
 

	if (process.env.NODE_ENV === 'development') {
	app.use(morgan('dev'));
	} else if (process.env.NODE_ENV === 'production') {
	app.use(compress());
	}
	app.use(bodyParser.urlencoded({
		limit: '50mb',
	    extended: true,

	}));
	app.use(bodyParser.json({limit: '50mb'}));
	app.use(methodOverride());
	//console.log('Limit file size: '+limit);

	// CookieParser should be above session
	app.use(cookieParser());

	
	app.use(session({
	 saveUninitialized: true,
	 resave: true,
	 secret: config.sessionSecret
	 }));

	app.use(flash());

	app.use(passport.initialize());
 	app.use(passport.session());

	app.set('views', './app/views');
 	app.set('view engine', 'ejs');

 	app.use(express.static('./public'));

 	app.use(multipart({
	    uploadDir: './public/lib/temp_folder'
	}));



 require('../app/routes/index.server.routes.js')(app);
 require('../app/routes/file.routes.js')(app);


 require('../app/routes/cards.server.routes.js')(app);
 require('../app/routes/category.server.routes.js')(app);
 require('../app/routes/product-category.server.routes.js')(app);
 require('../app/routes/product.server.routes.js')(app);
 require('../app/routes/department.server.routes.js')(app);
 require('../app/routes/state.server.routes.js')(app);
 require('../app/routes/videos.server.routes.js')(app);
 require('../app/routes/email.server.routes.js')(app);
 require('../app/routes/cards-order.server.routes.js')(app);
 require('../app/routes/users.server.routes.js')(app);
 require('../app/routes/contact-query.server.routes.js')(app);
 require('../app/routes/company.server.routes.js')(app);
 require('../app/routes/orders.server.routes.js')(app);
 require('../app/routes/controller.user.routes.js')(app);

 return app;
};