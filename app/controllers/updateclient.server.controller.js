var Upload = require('mongoose').model('uploadImage');
var TaxRegion = require('mongoose').model('TaxRegion');
var user = require('mongoose').model('User');
var department = require('mongoose').model('Department');
var Card = require('mongoose').model('Card');
var history = require('mongoose').model('History');
var category=require('mongoose').model('Category');
var state=require('mongoose').model('State');
var changecase=require('change-case');

var async = require('async');
var _ = require('lodash');
var time = require('time');

var diff = require('deep-diff').diff;
var carddata = [];
//var errorHandler = require('errors.server.controller.js');
exports.getSearch = function(req, res, next) {
  var query = {
    $and: []
  };
  var forsingleD = {};
  var forsingleU = {};
  var query1 = {
    $and: []
  };
  var ids = [];
  var dptnu = [];
  // console.log(req.body.length);
  for (var i = 0; i < req.body.length; i++) {
    switch (req.body[i].tag) {
      case 'name':
        var name = new RegExp(req.body[i].val, 'i');
        var temp = {}
        temp[req.body[i].tag] = name;
        forsingleD = temp;
        query.$and.push(temp);
        /*  depart[req.body[i].tag] = name;
          string +=depart+',';*/
        break;
      case 'ownerName':
        var name = new RegExp(req.body[i].val, 'i');
        var temp = {}
        temp[req.body[i].tag] = name;
        forsingleD = temp;
        query.$and.push(temp);
        break;
      case 'confirmation':
        var name = new RegExp(req.body[i].val, 'i');
        var temp = {}
        temp[req.body[i].tag] = name;
        forsingleU = temp;
        query1.$and.push(temp);
        break;
      case 'accessCode':
        var name = new RegExp(req.body[i].val, 'i');
        var temp = {}
        temp[req.body[i].tag] = name;
        forsingleD = temp;
        query.$and.push(temp);
        break;
      case 'status':
        var name = new RegExp(req.body[i].val, 'i');
        var temp = {}
        temp[req.body[i].tag] = name;
        forsingleU = temp;
        query1.$and.push(temp);
        query1.$and.push({
          roles: ['Super User']
        });
        break;
      case 'address1':
        var name = new RegExp(req.body[i].val, 'i');
        var temp = {}
        temp[req.body[i].tag] = name;
        forsingleU = temp;
        query1.$and.push(temp);
        query1.$and.push({
          roles: ['Super User']
        });
        break;
      case 'taxid':
        var temp = {}
        temp[req.body[i].tag] = req.body[i].val;
        forsingleU = temp;
        query1.$and.push(temp);
        query1.$and.push({
          roles: ['Super User']
        });
        break;
    }
  }
  switch (req.body.length) {
    case 1:
      var data = [];
      // console.log(isEmpty(forsingleD));
      if (!isEmpty(forsingleD)) {
        // console.log("msndm");
        department.find(forsingleD).exec(function(err, department) {
          if (err) {
            // console.log(err);
            return next(err);
          } else {
            for (var i = 0; i < department.length; i++) {
              ids.push(department[i]._id);
            }
            user.find({
              $and: [{
                vendorId: {
                  "$in": ids
                }
              }, {
                roles: ['Super User']
              }]
            }).populate('vendorId').exec(function(err, user) {
              if (err) {
                return next(err);
              } else {
                // console.log(user)
                res.json(user);
              }
            });
          }
        });
      }
      if (!isEmpty(forsingleU)) {
        var departData = [];
        // departData.userId={};
        console.log(forsingleU);
        user.find(forsingleU).exec(function(err, user) {
          if (err) {
            console.log(err);
            return next(err);
          } else {
            //console.log("xznm" + user);
            //  res.json(department);    
            for (var i = 0; i < user.length; i++) {
              ids.push(user[i].vendorId);
              dptnu.push(user[i].dptnu);
            }
            console.log("IDS" + ids);
            console.log("DPTNU" + dptnu)
            department.find({
              $or: [{
                _id: {
                  "$in": ids
                }
              }, {
                dptnu: {
                  "$in": dptnu
                }
              }]
            }).populate('userId').exec(function(err, depart) {
              if (err) {
                return next(err);
              } else {
                //  console.log(depart)
                var i = 0;
                async.each(depart, function(value) {

                  departData.push({
                    "_id": value._id,
                    "name": value.name,
                    "ownerName": value.ownerName,
                    "accessCode": value.accessCode,
                    "address": value.address,
                    "disclaimer": value.disclaimer,
                    "logo": value.logo,
                    "misc_attach": value.misc_attach,
                    "processor_notes": value.processor_notes,
                    "created": value.created,
                    "cards": value.cards,
                    "onlyCheckoutAccess": value.onlyCheckoutAccess,
                    "dptnu": value.dptnu,
                    "userId": user[i]
                  });

                  i++;
                });
                //console.log(departData)


                // async.waterfall([
                //   function(done)
                //   {
                //     console
                //   }
                //   ])

                res.json(departData);
              }
            });
          }
        });
      }
      break;
    case 2:
      // console.log("case 2");
      if (query.$and.length > 0) {
        console.log("first");
        department.find(query).exec(function(err, department) {
          if (err) {
            return next(err);
          } else {
            // console.log(department)
            for (var i = 0; i < department.length; i++) {
              ids.push(department[i]._id);
              dptnu.push(department[i].dptnu);
            }
            query1.$and.push({
              $or: [{
                vendorID: {
                  "$in": ids
                }
              }, {
                dptnu: {
                  "$in": dptnu
                }
              }]
            });
            if (query1.$and.length > 0) {
              user.find(query1).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  console.log("hi");
                  //    console.log(user)
                  res.json(user);
                }
              });
            } else {
              console.log(ids);
              user.find({
                $or: [{
                  vendorID: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  //console.log(user)
                  res.json(user);
                }
              });
            }
          }
        });
      } else {
        user.find(query1).exec(function(err, user) {
          if (err) {
            console.log(err);
            return next(err);
          } else {
            // console.log(user);
            //  res.json(department);    
            for (var i = 0; i < user.length; i++) {
              ids.push(user[i].vendorId);
              dptnu.push(user[i].dptnu);
            }
            //  console.log(ids);
            if (query.$and.length > 0) {
              department.find({
                $and: [{
                  $or: [{
                    id: {
                      "$in": ids
                    }
                  }, {
                    dptnu: {
                      "$in": dptnu
                    }
                  }]
                }, query]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //   console.log(depart)
                  res.json(depart);
                }
              });
            } else {
              department.find({
                $or: [{
                  id: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //   console.log(depart)
                  res.json(depart);
                }
              });
            }
          }
        });
      }
      break;
    case 3:
      console.log("case 2");
      if (query.$and.length > 0) {
        //console.log("first");
        department.find(query).exec(function(err, department) {
          if (err) {
            return next(err);
          } else {
            //console.log(department)
            for (var i = 0; i < department.length; i++) {
              ids.push(department[i]._id);
              dptnu.push(department[i].dptnu);
            }
            query1.$and.push({
              $or: [{
                vendorID: {
                  "$in": ids
                }
              }, {
                dptnu: {
                  "$in": dptnu
                }
              }]
            });
            if (query1.$and.length > 0) {
              user.find(query1).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  // console.log("hi");
                  //  console.log(user)
                  res.json(user);
                }
              });
            } else {
              // console.log(ids);
              user.find({
                $or: [{
                  vendorID: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  //console.log(user)
                  res.json(user);
                }
              });
            }
          }
        });
      } else {
        user.find(query1).exec(function(err, user) {
          if (err) {
            console.log(err);
            return next(err);
          } else {
            //console.log(user);
            //  res.json(department);    
            for (var i = 0; i < user.length; i++) {
              ids.push(user[i].vendorId);
              dptnu.push(user[i].dptnu);
            }
            // console.log(ids);
            if (query.$and.length > 0) {
              department.find({
                $and: [{
                  $or: [{
                    id: {
                      "$in": ids
                    }
                  }, {
                    dptnu: {
                      "$in": dptnu
                    }
                  }]
                }, query]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //  console.log(depart)
                  res.json(depart);
                }
              });
            } else {
              department.find({
                $or: [{
                  id: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //console.log(depart)
                  res.json(depart);
                }
              });
            }
          }
        });
      }
      break;
    case 4:
      // console.log("case 2");
      if (query.$and.length > 0) {
        // console.log("first");
        department.find(query).exec(function(err, department) {
          if (err) {
            return next(err);
          } else {
            console.log(department)
            for (var i = 0; i < department.length; i++) {
              ids.push(department[i]._id);
              dptnu.push(department[i].dptnu);
            }
            //console.log(query1);
            query1.$and.push({
              $or: [{
                vendorID: {
                  "$in": ids
                }
              }, {
                dptnu: {
                  "$in": dptnu
                }
              }]
            });
            if (query1.$and.length > 0) {
              user.find(query1).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  // console.log("hi");
                  // console.log(user)
                  res.json(user);
                }
              });
            } else {
              // console.log(ids);
              user.find({
                $or: [{
                  vendorID: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  //   console.log(user)
                  res.json(user);
                }
              });
            }
          }
        });
      } else {
        user.find(query1).exec(function(err, user) {
          if (err) {
            // console.log(err);
            return next(err);
          } else {
            //  console.log(user);
            //  res.json(department);    
            for (var i = 0; i < user.length; i++) {
              ids.push(user[i].vendorId);
              dptnu.push(user[i].dptnu);
            }
            //console.log(ids);
            if (query.$and.length > 0) {
              department.find({
                $and: [{
                  $or: [{
                    id: {
                      "$in": ids
                    }
                  }, {
                    dptnu: {
                      "$in": dptnu
                    }
                  }]
                }, query]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //   console.log(depart)
                  res.json(depart);
                }
              });
            } else {
              department.find({
                $or: [{
                  id: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //   console.log(depart)
                  res.json(depart);
                }
              });
            }
          }
        });
      }
      break;
    case 5:
      // console.log("case 2");
      if (query.$and.length > 0) {
        //console.log("first");
        department.find(query).exec(function(err, department) {
          if (err) {
            return next(err);
          } else {
            console.log(department)
            for (var i = 0; i < department.length; i++) {
              ids.push(department[i]._id);
              dptnu.push(department[i].dptnu);
            }
            query1.$and.push({
              $or: [{
                vendorID: {
                  "$in": ids
                }
              }, {
                dptnu: {
                  "$in": dptnu
                }
              }]
            });
            if (query1.$and.length > 0) {
              user.find(query1).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  // console.log("hi");
                  //console.log(user)
                  res.json(user);
                }
              });
            } else {
              console.log(ids);
              user.find({
                $or: [{
                  vendorID: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  //  console.log(user)
                  res.json(user);
                }
              });
            }
          }
        });
      } else {
        user.find(query1).exec(function(err, user) {
          if (err) {
            // console.log(err);
            return next(err);
          } else {
            //console.log(user);
            //  res.json(department);    
            for (var i = 0; i < user.length; i++) {
              ids.push(user[i].vendorId);
              dptnu.push(user[i].dptnu);
            }
            //console.log(ids);
            if (query.$and.length > 0) {
              department.find({
                $and: [{
                  $or: [{
                    id: {
                      "$in": ids
                    }
                  }, {
                    dptnu: {
                      "$in": dptnu
                    }
                  }]
                }, query]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //console.log(depart)
                  res.json(depart);
                }
              });
            } else {
              department.find({
                $or: [{
                  id: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //console.log(depart)
                  res.json(depart);
                }
              });
            }
          }
        });
      }
      break;
    case 6:
      //console.log("case 2");
      if (query.$and.length > 0) {
        // console.log("first");
        department.find(query).exec(function(err, department) {
          if (err) {
            return next(err);
          } else {
            //  console.log(department)
            for (var i = 0; i < department.length; i++) {
              ids.push(department[i]._id);
              dptnu.push(department[i].dptnu);
            }
            query1.$and.push({
              $or: [{
                vendorID: {
                  "$in": ids
                }
              }, {
                dptnu: {
                  "$in": dptnu
                }
              }]
            });
            if (query1.$and.length > 0) {
              user.find(query1).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  //  console.log("hi");
                  // console.log(user)
                  res.json(user);
                }
              });
            } else {
              console.log(ids);
              user.find({
                $or: [{
                  vendorID: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('vendorId').exec(function(err, user) {
                if (err) {
                  return next(err);
                } else {
                  //   console.log(user)
                  res.json(user);
                }
              });
            }
          }
        });
      } else {
        user.find(query1).exec(function(err, user) {
          if (err) {
            console.log(err);
            return next(err);
          } else {
            // console.log(user);
            //  res.json(department);    
            for (var i = 0; i < user.length; i++) {
              ids.push(user[i].vendorId);
              dptnu.push(user[i].dptnu);
            }
            // console.log(ids);
            if (query.$and.length > 0) {
              department.find({
                $and: [{
                  $or: [{
                    id: {
                      "$in": ids
                    }
                  }, {
                    dptnu: {
                      "$in": dptnu
                    }
                  }]
                }, query]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //console.log(depart)
                  res.json(depart);
                }
              });
            } else {
              department.find({
                $or: [{
                  id: {
                    "$in": ids
                  }
                }, {
                  dptnu: {
                    "$in": dptnu
                  }
                }]
              }).populate('userId').exec(function(err, depart) {
                if (err) {
                  return next(err);
                } else {
                  //console.log(depart)
                  res.json(depart);
                }
              });
            }
          }
        });
      }
      break;
    case 7:
      var query = {
        $and: []
      };
      var query1 = [];
      var depart = {};
      var depart1 = {};
      var depart2 = {};
      var depart3 = {};
      var user1 = {};
      var user2 = {};
      var user3 = {};
      var string = "";
      var ids = [];
      for (var i = 0; i < 7; i++) {
        switch (req.body[i].tag) {
          case 'name':
            var name = new RegExp(req.body[i].val, 'i');
            var temp = {}
            temp[req.body[i].tag] = name;
            query.$and.push(temp);
            /*  depart[req.body[i].tag] = name;
              string +=depart+',';*/
            break;
          case 'ownerName':
            var name = new RegExp(req.body[i].val, 'i');
            var temp = {}
            temp[req.body[i].tag] = name;
            query.$and.push(temp);
            break;
          case 'confirmation':
            var name = new RegExp(req.body[i].val, 'i');
            var temp = {}
            temp[req.body[i].tag] = name;
            query.$and.push(temp);
            break;
          case 'accessCode':
            var name = new RegExp(req.body[i].val, 'i');
            var temp = {}
            temp[req.body[i].tag] = name;
            query.$and.push(temp);
            break;
          case 'status':
            var name = new RegExp(req.body[i].val, 'i');
            var temp = {}
            temp[req.body[i].tag] = name;
            query1.push(temp);
            break;
          case 'address1':
            var name = new RegExp(req.body[i].val, 'i');
            var temp = {}
            temp[req.body[i].tag] = name;
            query1.push(temp);
            break;
          case 'taxid':
            var name = new RegExp(req.body[i].val, 'i');
            var temp = {}
            temp[req.body[i].tag] = name;
            query1.push(temp);
            break;
        }
      }
      var strLen = string.length;
      string = string.slice(0, strLen - 1);
      console.log(depart);
      department.find(query).exec(function(err, department) {
        if (err) {
          return next(err);
        } else {
          console.log(department)
            //  res.json(department);  
          for (var i = 0; i < department.length; i++) {
            ids.push(department[i]._id);
          }
          user.find({
            $and: [{
              vendorId: {
                "$in": ids
              }
            }, query1]
          }).populate('vendorId').exec(function(err, user) {
            if (err) {
              return next(err);
            } else {
              // console.log(user)
              res.json(user);
            }
          });
        }
      });
      break;
  }
  /* if(req.body[0].tag=='name' || req.body[0].tag=='ownerName' )
      {
        

      }
         if(req.body[1].tag=='name' || req.body[1].tag=='ownerName' )
      {
         var name=new RegExp(req.body[1].val,'i');
          var query = {};
         query[req.body[1].tag] = name;
         var query1 = {};
         query1[req.body[0].tag] = req.body[1].val;     
         department.find({$and:[query,query1]})            
                     .exec(function (err, department) {
                      if (err) {
                                return next(err);
                            } else {
                              //console.log()
                                res.json(department);                               
                             }
                     }) ; 

      }
      if(req.body[0].tag=='name' && req.body[1].tag=='ownerName' )
      {
         var name0=new RegExp(req.body[0].val,'i');
         var name1=new RegExp(req.body[1].val,'i');

         var query = {};
         query[req.body[0].tag] = name0;
         var query1 = {};
         query1[req.body[1].tag] = name1; 
         console.log("ang");
         department.find({$and:[query,query1]})            
                     .exec(function (err, department) {
                      if (err) {
                                return next(err);
                            } else {
                              //console.log()
                                res.json(department);                               
                             }
                     }) ; 

      }
      else
      {
         var query = {};
         query[req.body[0].tag] = req.body[0].val;
         var query1 = {};
         query1[req.body[1].tag] = req.body[1].val; 
         department.find({$and:[query,query1]})            
                     .exec(function (err, department) {
                      if (err) {
                                return next(err);
                            } else {
                              //console.log()
                                res.json(department);                               
                             }
                     }) ; 
      } 



     
      
      
    }
  }*/
};
exports.getSearchUserDetail = function(req, res, next) {
  console.log(req.params.vendorid);
  user.findOne({
    vendorId: req.params.vendorid
  }).exec(function(err, data) {
    if (err) {
      throw err;
    } else {
      // console.log(data);
      res.json(data);
    }
  });
}
exports.getSearchFull = function(req, res, next) {
  console.log(req.body.length);
  console.log(req.body);
  switch (req.body.length) {
    case 1:
      var name = new RegExp(req.body[0].val, 'i');
      var query = {};
      query[req.body[0].tag] = name;
      var query1 = {};
      query1['vendorId'] = req.body[0].vendorId;
      user.find({
        $and: [query, query1]
      }).exec(function(err, department) {
        if (err) {
          return next(err);
        } else {
          //console.log(department);
          res.json(department);
        }
      });
      break;
    case 2:
      var name = new RegExp(req.body[0].val, 'i');
      var name1 = new RegExp(req.body[1].val, 'i');
      var query = {};
      query[req.body[0].tag] = name;
      var query1 = {};
      query1[req.body[1].tag] = name1;
      var query2 = {};
      query2['vendorId'] = req.body[0].vendorId;
      user.find({
        $and: [query, query1, query2]
      }).exec(function(err, department) {
        if (err) {
          return next(err);
        } else {
          //console.log(department);
          res.json(department);
        }
      });
      break;
    case 3:
      var name = new RegExp(req.body[0].val, 'i');
      var name1 = new RegExp(req.body[1].val, 'i');
      var name3 = new RegExp(req.body[2].val, 'i');
      var query = {};
      query[req.body[0].tag] = name;
      var query1 = {};
      query1[req.body[1].tag] = name1;
      var query2 = {};
      query2[req.body[2].tag] = name1;
      var query3 = {};
      query3['vendorId'] = req.body[0].vendorId;
      user.find({
        $and: [query, query1, query2, query3]
      }).exec(function(err, department) {
        if (err) {
          return next(err);
        } else {
          //console.log(department);
          res.json(department);
        }
      });
      break;
  }
};
exports.getUserData = function(req, res, next) {
  console.log(req.params.dptnu);
  switch(req.body.tag)
  {
    case "uniquecode":
    var uniquecode=req.body.uniquecode;
     user.findOne({
    "uniqueCode": uniquecode
  }).exec(function(err, data) {
    if (err) {
      return next(err);
    } else {
      // console.log(data);
      res.json(data);
    }
  })
    break;

    case "dptnu":
    var dptnu=req.body.dptnu;
     user.findOne({
    "dptnu": dptnu
  }).exec(function(err, data) {
    if (err) {
      return next(err);
    } else {
      // console.log(data);
      res.json(data);
    }
  })
    break;
  }
  // var dptnu = req.params.dptnu;
  // userid = req.params.userid;
  // //console.log(regex);
 
}
exports.getDepartment = function(req, res, next) {
  console.log("snkdjf" + req.body.tag);
  switch(req.body.tag)
  {
    case "uniquecode":
    var uniquecode=req.body.uniquecode;
     department.findOne({
    "uniqueCode": uniquecode
  }).exec(function(err, data) {
    if (err) {
      return next(err);
    } else {
      // console.log(data);
      res.json(data);
    }
  })
    break;

    case "dptnu":
    var dptnu=req.body.dptnu;
     department.findOne({
    "dptnu": dptnu
  }).exec(function(err, data) {
    if (err) {
      return next(err);
    } else {
      // console.log(data);
      res.json(data);
    }
  })
    break;
  }
  // var dptnu = req.params.dptnu;
  // //console.log(regex);
  // department.findOne({
  //   "uniqueCode": dptnu
  // }).exec(function(err, data) {
  //   if (err) {
  //     return next(err);
  //   } else {
  //     // console.log("nzdkjfs" + data);
  //     res.json(data);
  //   }
  // })
}
exports.getCard = function(req, res, next) {
  console.log(req.params.dptnu);
 // var dptnu = req.params.dptnu;
  switch(req.body.tag)
  {
    case "uniquecode":
    var uniquecode=req.body.uniquecode;
     Card.find({
    "uniqueCode": uniquecode
  }).exec(function(err, data) {
    if (err) {
      return next(err);
    } else {
      // console.log(data);
      res.json(data);
    }
  })
    break;

    case "dptnu":
    var dptnu=req.body.dptnu;
     Card.find({
    "dptnu": dptnu
  }).exec(function(err, data) {
    if (err) {
      return next(err);
    } else {
      // console.log(data);
      res.json(data);
    }
  })
    break;
  }
  // //console.log(regex);
  // Card.find({
  //   "uniqueCode": dptnu
  // }).exec(function(err, data) {
  //   if (err) {
  //     return next(err);
  //   } else {
  //     // console.log(data);
  //     res.json(data);
  //   }
  // })
}
exports.updateUserData = function(req, res) {
  var users = req.body;
  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;
  //console.log(req.body);
  /* init user informations */
  var userBeforeSave = {};
  async.waterfall([
    function(done) {
      user.findOne({
        _id: users._id
      }, function(err, currentUser) {
        //console.log(currentUser);
        userBeforeSave = currentUser;
        done(err);
      });
    },
    function(done) {
      // Merge existing user
      users = _.extend(userBeforeSave, req.body);
      var eff_date = new time.Date(req.body.eff_date);
      var end_date = new time.Date(req.body.end_date);
      console.log(eff_date.toString());
      console.log(end_date.toString());
      eff_date.setTimezone('UTC', true);
      end_date.setTimezone('UTC', true);
      console.log(eff_date.toString());
      console.log(end_date.toString());
      users.eff_date = eff_date.toString();
      users.end_date = end_date.toString();

      users.updated = Date.now();
      users.save(function(err) {
        if (err) {
          console.log(err);
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(user);
        }
        console.log(req.body)

        if (!isEmpty(req.body.categary) && !isEmpty(req.body.stateid)) {
          category.findOne({
            $and: [{
              name: req.body.categary
            }, {
              stateList: {
                $in: req.body.stateid
              }
            }]
          }, function(err, categorymain) {


            console.log("lkhasdkj" + categorymain);
            if (categorymain == null) {
              category.findOne({
                name: req.body.categary
              }, function(err, data) {
                data.stateList.push(req.body.stateid)
                data.save(function(err, data) {
                  console.log(data);
                })
              });
            } else {}

          });


          if (!isEmpty(req.body.cities)) {
            state.findOne({
              $and: [{
                name: req.body.state
              }, {
                "cities.name": new RegExp(req.body.cities.name, 'i')
              }]
            }, function(err, statemain) {
              if (statemain != null) {
                console.log(statemain);
                statemain.cities.forEach(function(element, index, array) {
                  if (element.name == changecase.titleCase(req.body.cities.name))
                    console.log(req.body.cities)
                  element.departments.push({
                    "catname": req.body.cities.departments.catname,
                    "depId": req.body.cities.departments.depId
                  });


                });
                console.log(statemain);
                statemain.save(function(err, state) {
                  //    console.log("save");
                  console.log(state);
                })
              } else {
                state.findOne({
                  name: req.body.state
                }, function(err, stateno) {
                  var data = {};
                  data.departments = [];

                  data.name = changecase.titleCase(req.body.cities.name);

                  data.departments.push({
                    "catname": req.body.cities.departments.catname,
                    "depId": req.body.cities.departments.depId
                  }); //req.body.cities.departments;


                  stateno.cities.push(data);

                  stateno.save(function(err, stateres) {
                    if (err) throw err;
                    else {
                      console.log(stateres);
                    }
                  });
                });

              }

            });
          }




        }



      });
    }
  ], function(err) {
    if (err) return next(err);
  });
}
exports.updateDepartData = function(req, res) {
  var users = req.body;
  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;
  // console.log(req.body);
  /* init user informations */
  var userBeforeSave = {};
  async.waterfall([
    function(done) {
      department.findOne({
        _id: users._id
      }, function(err, currentUser) {
        /// console.log(currentUser);
        userBeforeSave = currentUser;
        done(err);
      });
    },
    function(done) {
      // Merge existing user
      users = _.extend(userBeforeSave, req.body);
      // console.log(req.body.miscattach);
      async.each(req.body.miscattach, function(value, callback) {
        users.misc_attach.push(value);
        callback();
      });
      users.updated = Date.now();
      users.save(function(err) {
        if (err) {
          console.log(err);
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(user)

        }
      });
    }
  ], function(err) {
    if (err) return next(err);
  });
}

function arrayDiff(a1, a2) {
  var o1 = {},
    o2 = {},
    diff = [],
    i, len, k;
  for (i = 0, len = a1.length; i < len; i++) {
    o1[a1[i]] = true;
  }
  for (i = 0, len = a2.length; i < len; i++) {
    o2[a2[i]] = true;
  }
  for (k in o1) {
    if (!(k in o2)) {
      diff.push(k);
    }
  }
  for (k in o2) {
    if (!(k in o1)) {
      diff.push(k);
    }
  }
  return diff;
}
exports.updateCardData = function(req, res) {
  var carddata = [];



  Card.remove({
    'dptnu': req.body[0].dptnu
  }).exec(function(err, data) {
    if (err) {
      console.log(err)
    } else {
      console.log("here")
    }
  });
  console.log("card length" + req.body.length)

  if (req.body.length > 0) {
    for (var i = 0; i < req.body.length; i++) {
      console.log(" cardCode" + req.body[i].cardCode);
      switch (req.body[i].type) {
        case 'gift':
          var cardd = new Card(req.body[i]);
          cardd.save(function(err, dep) {
            if (err) {
              console.log("err" + err);
            } else {
              //  console.log(dep);
              // res.json({"msg":"Done"});

            }
          });
          break;
        case 'advantage':
          console.log("heres");
          console.log(req.body[i])
          var card = new Card(req.body[i]);
          card.save(function(err, dep) {
            if (err) {
              console.log("err" + err);
            } else {
              console.log(dep);
              // res.json({"msg":"Done"});

            }
          });
          break;
        case 'cashback':
          var card = new Card(req.body[i]);
          card.save(function(err, dep) {
            if (err) {
              console.log("err" + err);
            } else {
              //  console.log(dep);
              // res.json({"msg":"Done"});

            }
          });
          break;
        case 'points':
          var card = new Card(req.body[i]);
          card.save(function(err, dep) {
            if (err) {
              console.log("err" + err);
            } else {
              //  console.log(dep);
              // res.json({"msg":"Done"});

            }
          });
          break;
      }
    }
    res.json({
      "msg": "Done"
    });
  } else {
    res.json({
      "msg": "Done"
    });
  }


};
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {
  // null and undefined are "empty"
  if (obj == null) return true;
  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;
  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}
exports.saveHistory = function(req, res) {
  console.log(req.body)
  var hist = new history(req.body);
  hist.save(function(err, data) {
    if (err) {
      console.log(err)
    } else {
      console.log(data);
    }
  })
}
exports.getHistoryData = function(req, res) {
  // history.remove({"userid":"56a31d4d12451f11008015a4"}).exec(function(err,data)
  // {
  //     if(err)
  //     {
  //         console.log(err)
  //     }
  //     else
  //     {
  //         console.log(data)
  //     }
  // })
  var userid = req.params.userid;
  var updateby = req.params.username;
  //console.log(userid)
  history.find({
    username: userid
  }).sort({
    update_date: -1
  }).exec(function(err, data) {
    if (err) {
      console.log(err)
    } else {
      // console.log(data)
      res.json(data);
    }
  })


}

exports.getOwnerName = function(req, res) {
  department.findOne({
    dptnu: req.params.dptnu
  }, {
    ownerName: 1
  }).exec(function(err, data) {
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })

}