var users = require('../../app/controllers/users.server.controller'),
     upload=require('../../app/controllers/upload.Image.server.controller'),
     departments=require('../../app/controllers/department.server.controller'),
     cards=require('../../app/controllers/card.server.controller')
    passport = require('passport');
module.exports = function(app) {

    /*Routes for user sign up */
    app.route('/user/signup')
        .post(users.signup);

app.route('/user/register')
.post(users.registercard);


    /*Routes for vendor sign up */

    app.route('/user/vendor-signup')
        .post(users.vendorSignup);

       /*Routes for Controller sign up */

    app.route('/user/contSignup')
        .post(users.contSignup);
    /*Routes for login */
    app.route('/auth/signin')
        .post(users.signin);
         /*Routes for login */
 

    /*Routes for logout */
    app.route('/auth/signout').get(users.signout);

    /* add cards to user*/
    app.route('/user/add-new-cards').post(users.addCards);

    
    app.get('/signout', users.signout);

    /* route for getting user cards */
    app.route('/usercards/:userId').get(users.getUserCards);    

    /* route for update profile */
    app.route('/user/update-profile').post(users.update);


    // Setting up the users password api
    app.route('/users/password').post(users.changePassword);
    app.route('/auth/forgot').post(users.forgot);
    app.route('/auth/reset/:token').get(users.validateResetToken);
    app.route('/auth/reset/:token').post(users.reset);
    app.route('/auth/security-questions/:token').get(users.getSecurityQuestions);
    app.route('/user/user-by-id/:userId').get(users.getUserById);

    // get vendor details
    app.route('/users/:id/vendor').get(users.userWithVendorByID);


    // vendor get user detail by these routes
    app.route('/vendor-section/search-cards-by-card-num').get(users.getUserCardsByCardNumber);   

    app.route('/vendor-section/search-cards-by-phone').get(users.getUserByPhone);   

    app.route('/users/get-card').get(users.getCardByCardNoAndUser);  

    app.route('/card/validate').get(users.validateCard);  

    app.route('/user/reg-card-auto').post(users.regCardAuto);  
	app.route('/users/:email/register').get(users.getEmail);
    app.route('/users/numberCheck/:sd').get(users.numberCheck);
    app.route('/users/getAllUsers/:id').get(users.getAllUsers);
    app.route('/upload/url').post(users.uploadImage);
    app.route('/uploadImage').post(upload.create);
    app.route('/sendEmail').post(upload.sendEmail);
    app.route('/uploadget').get(upload.getUpload);
    app.route('/getTemplatename/:tmp/').get(upload.getTemplatename);
    app.route('/getAllEmail').get(upload.getAllEmail);
   app.route('/department/getdata/:userId').get(departments.readData)
                   .put(departments.update)
                   .delete(departments.delete);


    app.route('/cards/cardByID/:id').get(cards.readData)
                               .put(cards.update)
                               .delete(cards.delete);

    app.route('/user/saveUserData').post(users.saveUserData);
    app.route('/users/salesDetail/:date/:mon/:yr').get(users.getSalesDetail);
    app.route('/users/myPrice/:id').get(users.getMyPrice);
    app.route('/users/usertypes').get(users.getSalesDetails);
    app.route('/users/monthlysales/:yr/:mon').get(users.getMonthlySales);
    app.route('/users/quartelySales/:qtr/:yr').get(users.getQuartelySales);
    app.route('/users/yearlySales/:yr').get(users.getYearlySales);
   
    app.route('/upload/removeImage/:name').post(upload.remImage);
    app.route('/getemaillist').get(users.getEmailList);

    app.route('/getcardetail/:em').get(users.getCardDetail);
    app.route('/getTotalTran/:no').get(users.getTotalTran);
    app.route('/sendMail/').post(users.sendMailToClient);
    
    /*Routes for PrimeUser Signup sign up */

    app.route('/user/primeUser_signup')
        .post(users.primeUser_signup);
    
    app.route('/vendor/resetpassword/:id').get(users.getUserDetail);
    app.route('/auth/resetPassword').post(users.resetPassword);


    
       
    

    
};