var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var CategorySchema = new Schema({
    name: {
        type:String,
        required : 'Category Name is required',
    },
    stateList: [{
                        type: Schema.ObjectId,
                        ref: 'State'
                    }],
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('Category', CategorySchema);