var app = angular.module('controller', []);

app.directive('lowerThan', [
  function() {

    var link = function($scope, $element, $attrs, ctrl) {

      var validate = function(viewValue) {
        var comparisonModel = $attrs.lowerThan;
        var t, f;
        
        if(!viewValue || !comparisonModel){
          // It's valid because we have nothing to compare against
          ctrl.$setValidity('lowerThan', true);
        }
        if (comparisonModel) {
            var to = comparisonModel.split("/");
            t = new Date(to[2], to[1] - 1, to[0]);
        }
        if (viewValue) {
            var from = viewValue.split("/");
            f = new Date(from[2], from[1] - 1, from[0]);

        }

        console.log(ctrl.$setValidity('lowerThan', Date.parse(t) < Date.parse(f)));
        // It's valid if model is lower than the model we're comparing against
          
        return viewValue;
      };

      ctrl.$parsers.unshift(validate);
      //ctrl.$formatters.push(validate);

    };

    return {
      require: 'ngModel',
      link: link
    };

  }
]);
