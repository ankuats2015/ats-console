var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var DepartmentSchema = new Schema({
    name: {
        type: String,
        required: 'Department Name is required',
    },
    accessCode: {
        type: String,
        unique: true,
        required: 'Access Code is required',
    },
    ownerName: {
        type: String,
       // required: 'Owner name requried',
    },
    dptnu: {
        type: Number, //Department Number unit
        default: 3456,
    },
    uniqueCode:{
        type:String,
        unique:true
    },
    onlyCheckoutAccess: {
        type: Boolean,
        default: false
    },
    address: String,
    cards: [{
        type: Schema.ObjectId,
        ref: 'Card'
    }],
    userId: {
        type: Schema.ObjectId,
        ref: 'User',
    },
    logo: String,
    contact: String,
    disclaimer: String,
    created: {
        type: Date,
        default: Date.now
    },
    processor_notes: [{
         notes:String ,
        created: {
        type: Date,
        default: Date.now
    }
    }],
    misc_attach: [{templatename:String,
            filename:String
    }]
});

mongoose.model('Department', DepartmentSchema);