'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User'),
	Department = mongoose.model('Department'),
	Company = mongoose.model('Company'),
	Order = mongoose.model('CardOrder'),
	config = require('../../../config/config'),
	nodemailer = require('nodemailer'),
	async = require('async');


var smtpTransport = nodemailer.createTransport(config.mailer.options);

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}


function sendAutoRegEmail(res, user, password){
	async.waterfall([
	                function(done) {
	                	res.render('templates/user/registration/dummy-registration-notification-to-user', {
							user: user,
							url : config.url,
							password : password,
							company: config.company
						}, function(err, emailHTML) {
							done(err, emailHTML, user);
						});
	                },
	                // If valid email, send reset email using service
					function(emailHTML, user, done) {
						var mailOptions = {
							to: user.email,
							from: config.mailer.from,
							subject: 'Registration Confirmation with "'+config.company.name,
							html: emailHTML
						};
						smtpTransport.sendMail(mailOptions, function(err) {
							done(err);
						});
					}

	                ], function(err){
	                	if (err) 
	                		console.log(err);
	                	else
	                		console.log('User user email sent');

	                });
}


exports.regCardAuto = function(req, res, next){

		if (req.user && req.user.userType == 'vendor') {

			var reqBody = req.body;

			if (reqBody.card_num) {

				async.waterfall([
	                function(done) {

	                	Order.findOne({
						        'cards.cardNumber':reqBody.card_num
						    }).
	                		populate([{path:'cards.cardId', select:'name type'}])
	                		.exec(function(err, order) {
						        if (err) {
						            return next(err);
						        } else {


						        if (order){
						            order.cards.forEach(function(card) {
						                    if(card.cardNumber == reqBody.card_num)
						                    {
						                    	card.department = order.department;
						                       done(err, card);
											}
						                  });
						            }
						            else
						            {
						                return res.status(200).send({
						                    error: 'No records found'
						                });
						            }
						        }
						    });      
	                   
	                },
	                function(card, done) {

	                	User.findOne({
						        'email' : card.user.email, 
						        'userType' : 'customer'
						    }, function(err, user) {
						        if (err) 
						        	return res.status(200).send({message: errorHandler.getErrorMessage(err)});
						            
						        done(err, user, card);
						    })


	                    
	                },
	                function(user, card, done) {

	                	var cardPin = randomIntFromInterval(1000, 9999);
                		var userCard = require('mongoose').model('userCards');
                        var newCard = new userCard({cardId: card.cardId.id,department: card.department, cn: card.user.name, cardNumber: card.cardNumber, pin: cardPin});
                            
                        if (card.cardId.type == 'gift' || card.cardId.type == 'advantage') {
                            newCard.initialVal = card.amount.price;
                            newCard.balance = card.amount.price;
                            newCard.txt = card.amount.text;
                        }

                        if (card.cardId.type == 'cashback') 
                            newCard.cashback = {earned:card.amount.price, used:0, balance:card.amount.price};
                        

                        if (card.cardId.type == 'points') 
                            newCard.points = {earned:card.amount.price, used:0, balance:card.amount.price};


	                	if (user)
	                	{
	                		user.cards.push(newCard);
                    		user.save(function(err) {
						        if (err) {
						            return res.status(200).send({message: errorHandler.getErrorMessage(err)});
						        } else {
						           return res.json({'message' : 'Card added !!!'});
						        }
						    });

	                	}
	                	else{

	                		var username = card.user.email.substring(0, card.user.email.indexOf('@'))+randomIntFromInterval(1000, 9999);
				    		var phone = randomIntFromInterval(1000000000, 9999999999);
				    		var upassword = randomIntFromInterval(1000000, 9999999);
				    		var newUser = { email : card.user.email,
				    						phone : phone,
				    						username : username,
				    						password : upassword,
				    						names : [username],
				    						contact : [{ number : phone, type : 'primary'}],
				    						security : [
															{
																question : "Your best car ?",
																answer : "Ferrari"
															},
															{
																question : "Your best Teacher ?",
																answer : "Ram"
															}
														],
											userType : 'customer',
											provider : 'local',
											createdBy : req.user._id
											};

							var newUser = new User(newUser);

							/* add card to user account */
                			newUser.cards.push(newCard);

                			newUser.save(function(err) {
				        	    	 if (err) {
							                var message = errorHandler.getErrorMessage(err);
							                return res.status(400).send({message:message });
							            }
							        else
							        	done(err, newUser ,upassword);
							    });
						}
	                    
	                },
					function(user, upassword, done) {
						/*send email after registration */
						sendAutoRegEmail(res, user, upassword);

						done(null);
					}
	            ], function(err) {
	                if (err) 
	                	return res.status(400).send({'message' : errorHandler.getErrorMessage(err) });
	                else
	                	return res.send({ 'message' : 'User Created!!!' });
	            });
			}
			else
				res.status(400).send({'message' : 'Card inforation missing'});
		

		}
		else
		{
			res.status(400).send({
	            message: 'User is not signed in'
	        });
		}


};

/**
 * Signup
 */
exports.signup = function(req, res, next) {
 

    if (!req.user) {

    	var userId = req.body.username;
    	var useremail = req.body.email.split("@");
    	var security = req.body.security;

    	if ((userId.indexOf('@') !== -1) ||  useremail[0] == userId)
    		return res.status(400).send({'message' : 'Email Id’s cannot be used for User Id. Please enter a valid User Id.'});
    	
    	if (req.body.email == req.body.alternativeEmails[0])
    		return res.status(400).send({'message' : 'Alternative Email Id should be different from Primary Email Id. Please correct'});
    	
    	if (security[0].question == security[1].question)
    		return res.status(400).send({'message' : 'Enter question different from Security Question 1'});
    	

    	
    	async.waterfall([
		// Generate random token
		function(done) {

			var user = new User(req.body);
	        var message = null;
	        user.provider = 'local';
	        
	        user.save(function(err) {
	        	console.log(err);
	        	 if (err) {
		                var message = errorHandler.getErrorMessage(err);
		                res.status(400).send({message:message });
		            }
		        else
		        	done(err, user);
		    });
		},
		function(user, done) {


			Company.findOne({code: 'giftcards'}, function(err, company) {
                  done(err, company, user);

                });
		},
		function(company, user, done) {

			var namelist = "";
			user.names.forEach(function(element, index, array){
				if (element)
					namelist += ((namelist == "") ? '' : ', ')+element;
			});

			res.render('templates/user/registration/registration-notification-to-user', {
				namelist: namelist,
				company: company
				//appName: config.app.title,
				
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Registration Confirmation with “Company Name”',
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'Registration success, An email has been sent to ' + user.email + '.'
					});
				} else {
					return res.status(400).send({
						message: 'Registration success, Failure sending email'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});

    } else {
        return res.redirect('/');
    }
};

/**
 * Vendor Signup
 */
exports.vendorSignup = function(req, res, next) {   
console.log("inside vendorsignup");

    if (!req.user) {

   console.log("inside block");
    	var userId = req.body.userId;
    	var useremail = req.body.email.split("@");
    	var security = req.body.security;

    	if ((userId.indexOf('@') !== -1) ||  useremail[0] == userId)
    		return res.status(400).send({'message' : 'Email Id’s cannot be used for User Id. Please enter a valid User Id.'});
    	
    	if (req.body.email == req.body.alternativeEmails[0])
    		return res.status(400).send({'message' : 'Alternative Email Id should be different from Primary Email Id. Please correct'});
    	
    	if (security[0].question == security[1].question)
    		return res.status(400).send({'message' : 'Enter question different from Security Question 1'});
    	

	async.waterfall([
		// Generate random token
		function(done) {

			User.findOne({dptnu:req.body.dptnu
			}, function(err, user) {
				console.log("user->"+user);
				var message = null;
	        user.provider = 'local';
	        user.save(function(err, user) {
	            if (err) {
	                var message = errorHandler.getErrorMessage(err);
	                //console.log(err);
	                return res.status(400).send({'message' : message});
	            }
	            else
	            {
	                var query = {"_id": user.vendorId};
	                var update = {"userId": user._id};
	                Department.findOneAndUpdate(query, update, function(err, department) {
	                  if (err) {
	                    var message = errorHandler.getErrorMessage(err);
	                    //console.log(err);
						return res.status(400).send({'message' : message});
	                        
	                  }
	                  else
	                    done(err, user, department);

	                });

	            }
	       }); 
			});

	    },
		function(user, department, done) {

			res.render('templates/user/registration/registration-notification-to-vendor', {
				owner: department.ownerName,
				company: config.company,
				userEmail: user.email
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Registration Confirmation with '+config.company.name,
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'Registration success, An email has been sent to ' + user.email + '.'
					});
				} else {
					return res.status(400).send({
						message: 'Registration success, Failure sending email'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});


    } else {
        return res.redirect('/');
    }
};


/**
 * Signin after passport authentication
 */
exports.signin = function(req, res, next) {

    passport.authenticate('user-local', function(err, user, info) {

        if (err || !user) {
        	
        	if (typeof myVar != 'undefined')
            	res.status(400).send(info);
            else
            	res.status(400).send({message: 'Unknown user or invalid password'});
            
        } else {

            user.password = undefined;
            user.salt = undefined;

            req.login(user, function(err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.json(user);
                }
            });
        }
    })(req, res, next);
};

/**
 * Signout
 */
exports.signout = function(req, res) {
	req.logout();
	res.redirect('/');
};

/**
 * OAuth callback
 */
exports.oauthCallback = function(strategy) {
	return function(req, res, next) {
		passport.authenticate(strategy, function(err, user, redirectURL) {
			if (err || !user) {
				return res.redirect('/#!/signin');
			}
			req.login(user, function(err) {
				if (err) {
					return res.redirect('/#!/signin');
				}

				return res.redirect(redirectURL || '/');
			});
		})(req, res, next);
	};
};

/**
 * Helper function to save or update a OAuth user profile
 */
exports.saveOAuthUserProfile = function(req, providerUserProfile, done) {
	if (!req.user) {
		// Define a search query fields
		var searchMainProviderIdentifierField = 'providerData.' + providerUserProfile.providerIdentifierField;
		var searchAdditionalProviderIdentifierField = 'additionalProvidersData.' + providerUserProfile.provider + '.' + providerUserProfile.providerIdentifierField;

		// Define main provider search query
		var mainProviderSearchQuery = {};
		mainProviderSearchQuery.provider = providerUserProfile.provider;
		mainProviderSearchQuery[searchMainProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

		// Define additional provider search query
		var additionalProviderSearchQuery = {};
		additionalProviderSearchQuery[searchAdditionalProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

		// Define a search query to find existing user with current provider profile
		var searchQuery = {
			$or: [mainProviderSearchQuery, additionalProviderSearchQuery]
		};

		User.findOne(searchQuery, function(err, user) {
			if (err) {
				return done(err);
			} else {
				if (!user) {
					var possibleUsername = providerUserProfile.username || ((providerUserProfile.email) ? providerUserProfile.email.split('@')[0] : '');

					User.findUniqueUsername(possibleUsername, null, function(availableUsername) {
						user = new User({
							firstName: providerUserProfile.firstName,
							lastName: providerUserProfile.lastName,
							username: availableUsername,
							displayName: providerUserProfile.displayName,
							email: providerUserProfile.email,
							provider: providerUserProfile.provider,
							providerData: providerUserProfile.providerData
						});

						// And save the user
						user.save(function(err) {
							return done(err, user);
						});
					});
				} else {
					return done(err, user);
				}
			}
		});
	} else {
		// User is already logged in, join the provider data to the existing user
		var user = req.user;

		// Check if user exists, is not signed in using this provider, and doesn't have that provider data already configured
		if (user.provider !== providerUserProfile.provider && (!user.additionalProvidersData || !user.additionalProvidersData[providerUserProfile.provider])) {
			// Add the provider data to the additional provider data field
			if (!user.additionalProvidersData) user.additionalProvidersData = {};
			user.additionalProvidersData[providerUserProfile.provider] = providerUserProfile.providerData;

			// Then tell mongoose that we've updated the additionalProvidersData field
			user.markModified('additionalProvidersData');

			// And save the user
			user.save(function(err) {
				return done(err, user, '/#!/settings/accounts');
			});
		} else {
			return done(new Error('User is already connected using this provider'), user);
		}
	}
};

/**
 * Remove OAuth provider
 */
exports.removeOAuthProvider = function(req, res, next) {
	var user = req.user;
	var provider = req.param('provider');

	if (user && provider) {
		// Delete the additional provider
		if (user.additionalProvidersData[provider]) {
			delete user.additionalProvidersData[provider];

			// Then tell mongoose that we've updated the additionalProvidersData field
			user.markModified('additionalProvidersData');
		}

		user.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			}
		});
	}
};


//For Updating Email id 

exports.registercard=function(req,res)
{	
	 console.log("hello");
   

};

exports.saveUserData=function(req,res,next)
{

    if (req.user) {

    	/*var userId = req.body.username;
    	var useremail = req.body.email.split("@");
    	var security = req.body.security;

    	if ((userId.indexOf('@') !== -1) ||  useremail[0] == userId)
    		return res.status(400).send({'message' : 'Email Id’s cannot be used for User Id. Please enter a valid User Id.'});
    	
    	if (req.body.email == req.body.alternativeEmails[0])
    		return res.status(400).send({'message' : 'Alternative Email Id should be different from Primary Email Id. Please correct'});
    	
    	if (security[0].question == security[1].question)
    		return res.status(400).send({'message' : 'Enter question different from Security Question 1'});*/
    	

    	
    	async.waterfall([
		// Generate random token
		function(done) {

			var user = new User(req.body);
	        var message = null;
	        user.provider = 'local';
	        
	        user.save(function(err) {
	        	console.log(err);
	        	 if (err) {
		                var message = errorHandler.getErrorMessage(err);
		                res.status(400).send({message:message });
		            }
		        else
		        	done(err, user);
		    });
		},
		function(user, done) {


			Company.findOne({code: 'giftcards'}, function(err, company) {
                  done(err, company, user);

                });
		},
		function(company, user, done) {

			var namelist = "";
			user.names.forEach(function(element, index, array){
				if (element)
					namelist += ((namelist == "") ? '' : ', ')+element;
			});

			res.render('templates/user/registration/registration-notification-to-user', {
				namelist: namelist,
				company: company
				//appName: config.app.title,
				
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Registration Confirmation with “Company Name”',
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'Registration success, An email has been sent to ' + user.email + '.'
					});
				} else {
					return res.status(400).send({
						message: 'Registration success, Failure sending email'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});

    } else {
    	console.log("else part");
        return res.redirect('/');
    }


};

exports.primeUser_signup=function(req,res,next)
{
	console.log(req.body);
	if (req.user) {


    	var userId = req.body.userId;
    	var useremail = req.body.email.split("@");
    	

    	if ((userId.indexOf('@') !== -1) ||  useremail[0] == userId)
    		return res.status(400).send({'message' : 'Email Id’s cannot be used for User Id. Please enter a valid User Id.'});
    	
    	

	async.waterfall([
		// Generate random token
		function(done) {

			var user = new User(req.body);
			console.log(user);
	        var message = null;
	        user.provider = 'local';
	        user.save(function(err, user) {
	            if (err) {
	                var message = errorHandler.getErrorMessage(err);
	                //console.log(err);
	                return res.status(400).send({'message' : message});
	            }
	            else
	            {
	            	console.log(req.user._id);
	                var query = {"userId": req.user._id};
	               
	                Department.findOne(query, function(err, department) {
	                  if (err) {
	                    var message = errorHandler.getErrorMessage(err);
	                    console.log(err);
						return res.status(400).send({'message' : message});
	                        
	                  }
	                  else
	                  	console.log(department);
	                    done(err, user, department);

	                });

	            }
	       }); 

	    },
		function(user, department, done) {

			res.render('templates/user/registration/registration-notification-to-vendor', {
				 owner: department.ownerName?department.ownerName:'',
				 company: config.company,
				userEmail: user.email
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Registration Confirmation with '+config.company.name,
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'Registration success, An email has been sent to ' + user.email + '.'
					});
				} else {
					return res.status(400).send({
						message: 'Registration success, Failure sending email'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});


    } 


}
	/**
 * Controller Signup
 */
exports.contSignup = function(req, res, next) {   

    if (!req.user) {


    	var userId = req.body.userId;
    	var useremail = req.body.email.split("@");
    	var security = req.body.security;

    	// if ((userId.indexOf('@') !== -1) ||  useremail[0] == userId)
    	// 	return res.status(400).send({'message' : 'Email Id’s cannot be used for User Id. Please enter a valid User Id.'});
    	
    	// if (req.body.email == req.body.alternativeEmails[0])
    	// 	return res.status(400).send({'message' : 'Alternative Email Id should be different from Primary Email Id. Please correct'});
    	
    	// if (security[0].question == security[1].question)
    	// 	return res.status(400).send({'message' : 'Enter question different from Security Question 1'});
    	

	async.waterfall([
		// Generate random token
		function(done) {

			var user = new User(req.body);
	        var message = null;
	        user.provider = 'local';
	        user.save(function(err, user) {
	            if (err) {
	                var message = errorHandler.getErrorMessage(err);
	                //console.log(err);
	                return res.status(400).send({'message' : message});
	            }
	            else
	            {
	            	console.log("Done")
	            }
	          
	       }); 

	    },
		function(user, department, done) {

			res.render('templates/user/registration/registration-notification-to-vendor', {
				owner: department.ownerName,
				company: config.company,
				userEmail: user.email
			}, function(err, emailHTML) {
				done(err, emailHTML, user);
			});
		},
		// If valid email, send reset email using service
		function(emailHTML, user, done) {
			var mailOptions = {
				to: user.email,
				from: config.mailer.from,
				subject: 'Registration Confirmation with '+config.company.name,
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'Registration success, An email has been sent to ' + user.email + '.'
					});
				} else {
					return res.status(400).send({
						message: 'Registration success, Failure sending email'
					});
				}

				done(err);
			});
		}
	], function(err) {
		if (err) return next(err);
	});


    } else {
        return res.redirect('/');
    }
};

