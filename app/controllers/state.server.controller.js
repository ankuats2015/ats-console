var State = require('mongoose').model('State'),
    Department = require('mongoose').model('Department');
   



exports.create = function(req, res, next) {
    var state = new State(req.body);
    state.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(state);
        }
    });
};

exports.list = function(req, res, next) {
    State.find({}, function(err, states) {
        if (err) {
            return next(err);
        } else {
            res.json(states);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.state);
};

exports.stateByID = function(req, res, next, id) {
    State.findOne({
        _id: id
    }, function(err, state) {
        if (err) {
            return next(err);
        } else {
            req.state = state;
            next();
        }
    });
};


exports.update = function(req, res, next) {
    State.findByIdAndUpdate(req.state.id, req.body, function(err, state) {
        if (err) {
            return next(err);
        } else {
            res.json(state);
        }
    });
};

exports.delete = function(req, res, next) {
    req.state.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.state);
        }
    })
};

exports.addDepartmentToCity = function(req, res, next) {

    Department.findOne({'name': req.body.Department}, function(err, department){
        if(err) return next(err);
        if(department){

            /* Fetching cities name */
            
            State.findOne({'cities.name': req.body.city}, function(err, state){
                if(err) return next(err);
                if(state){
                  
                    state.cities.forEach(function(element, index, array) {
                            if (element.name == req.body.city)
                            element.departments.push(department);
                    
                    });

                    /* Saving State after updating */
                    state.save(function(err) {
                        if (err) {
                            return next(err);
                        } else {
                            res.json(state);
                        }
                    });
  
                }
            });

        }
    });

};
