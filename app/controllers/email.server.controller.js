var config = require('../../config/config'),
    nodemailer = require('nodemailer'),
    async = require('async');


exports.sendRecepentMail = function(req, res, next) {


var smtpTransport = nodemailer.createTransport(config.mailer.options);
   // main async.each
var sender = req.body.sender;

  async.each(req.body.order.cards, function(card, callback2) {
        
        async.waterfall([
                    function(done) {
                        
                        res.render('templates/email-newsletter', {
                            recepent: card.user,
                            department: req.body.department,
                            sender : sender,
                            card : card,
                            company : config.company, 
                            siteurl: config.url
                        }, function(err, emailHTML) {
                            done(err, emailHTML);
                        });
                    },
                    // If valid email, send email using service
                    function(emailHTML, done) {
                        var mailOptions={
                            to : card.user.email,
                            //subject : req.body.department.name+' Mail',
                            subject : sender.name+" has sent you "+card.cardId.name,
                            html: emailHTML
                            };
                        smtpTransport.sendMail(mailOptions, function(err) {

                           /* if (!err) {
                               done(err);
                                
                            } else {
                                return false;
                              
                            }*/
                            done(err);
                        });
                    }
                ], function(err) {
                    if (err) callback2(err);
                    else
                        callback2();

                });


        
      }, function(err) {
         if (err)
            return console.error(err);
          else
          {
             res.send('complete');
          }
      });
    
   
};




exports.sendAdminOrderMail = function(req, res, next) {
var smtpTransport = nodemailer.createTransport(config.mailer.options);
  async.waterfall([
                    function(done) {
                        
                        res.render('templates/admin-order', {
                           company : config.company, 
                           order: req.body.order,
                           department: req.body.department,
                           siteurl: config.url
                           
                        }, function(err, emailHTML) {
                            done(err, emailHTML);
                        });
                    },
                    // If valid email, send email using service
                    function(emailHTML, done) {
                        var mailOptions={
                            to : req.body.admin.email,
                            subject : 'Your Consumer Card Order Confirmation',
                            html: emailHTML
                            };
                        smtpTransport.sendMail(mailOptions, function(err) {

                           /* if (!err) {
                               done(err);
                                
                            } else {
                                return false;
                              
                            }*/
                            done(err);
                        });
                    }
                ], function(err) {

                  res.send('success');

                  /*
                    if (err) callback2(err);
                    else
                        callback2();*/

                });
};
