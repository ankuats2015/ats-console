

exports.create=function(req,res){
var fs = require("fs");
img = JSON.stringify(req.files);
console.log(img);
var tmp_path = img.path;
//set where the file should actually exists - in this case it is in the "images" directory
var target_path = '/picture-upload/' + img.name;
//move the file from the temporary location to the intended location
fs.rename(tmp_path, '.'+target_path, function(err) {
  if (err) throw err;
    //delete the temporary file, so that the explicitly set temporary upload
    //dir does not get filled with unwanted files
    fs.unlink(tmp_path, function() {
       if (err) throw err;
       res.send('File uploaded to: ' + target_path + ' - ' + img.size + ' bytes');
    });
});
 
}