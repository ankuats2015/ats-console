var Product = require('mongoose').model('Product'),
    errorHandler = require('./errors.server.controller'),
    async = require('async');

exports.create = function(req, res, next) {
    var product = new Product(req.body, { _id: false });
    product.save(function(err) {
        if (err) {
            return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
            } else {
            res.json(product);
        }
    });
};

exports.list = function(req, res, next) {
    var query = (req.query) ? req.query : {};
    
    Product.find(query, function(err, products) {
        if (err) {
            return next(err);
        } else {
            res.json(products);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.product);
};

exports.ProductByID = function(req, res, next, id) {
    Product.findOne({
        _id: id
    }, function(err, product) {
        if (err) {
            return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
        } else {
            req.product = product;
            next();
        }
    });
};


exports.update = function(req, res, next) {
    delete req.body._id;
    
    Product.findByIdAndUpdate(req.product.id, req.body, function(err, product) {
        if (err) {
             return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
        } else {
            res.json(product);
        }
    });
};

exports.delete = function(req, res, next) {
    req.product.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.product);
        }
    })
};

exports.deleteCategoryProducts = function(req, res, next) {
    
    if (!req.body.category)
        return res.json({message: 'Please provide category id'});


    Product.find({ category: req.body.category },function(err,products){  
        if (err) {
                    return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
        } else {
             
            async.each(products, function(product, callback) {

                console.log(product);
                product.remove(function(err) {
                    if (err) {
                        return next(err);
                    } else {
                       callback();
                    }
                })

                 
            }, function(err){
                if( err ) {
                  return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                } else {
                  res.json({message : 'Products removed successfully'});
                }
               

            });

        }
        
});  

};


exports.searchProducts = function(req, res, next) {

    var regex = new RegExp(req.query.word, 'i');  // 'i' makes it case insensitive


    if (req.user) 
        var query = { $and: [ { department: req.user.vendorId }, { $or:[ {name: regex}, {code:regex} ] } ] };
    else
        var query = { $or:[ {name: regex}, {code:regex} ]};


    return Product.find(query)
                     .populate('category')
                     .exec(function (err, products) {
                      if (err) {
                                return next(err);
                            } else {
                               res.json(products);
                                
                            }
                    });;
}

exports.searchByQuery = function(req, res, next){
    return Product.find(req.query)
                     .exec(function (err, products) {
                      if (err) {
                                return next(err);
                            } else {
                               res.json(products);
                                
                            }
                    });;
}