var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var userCardsSchema = new Schema({
    cardId: {
        type: Schema.ObjectId,
        ref: 'Card'
    },
    department: {
        type: Schema.ObjectId,
        ref: 'Department'
    },
    cn: String,
    /* Consumer name */
    cardNumber: String,
    pin: Number,
    txt: String,
    initialVal: {
        type: Number,
        default: 0
    },
    balance: {
        type: Number,
        default: 0
    },
    cashback: {
        earned: {
            type: Number,
            default: 0
        },
        used: {
            type: Number,
            default: 0
        },
        balance: {
            type: Number,
            default: 0
        }
    },
    points: {
        earned: {
            type: Number,
            default: 0
        },
        used: {
            type: Number,
            default: 0
        },
        balance: {
            type: Number,
            default: 0
        }
    }
});


var UserSchema = new Schema({
    fullname: String,
    //userId: String,
    email: {
        type: String
    },
    phone: {
        type: Number,
        unique: true
    },
    alternativeEmails: [{
        type: String
    }],
    username: {
        type: String,
        index: true,
        unique: true
    },
    names: [{
        type: String,
        "default": '',
        trim: true
    }],
    address1: {
        type: String,
        "default": '',
        trim: true
    },
    address2: {
        type: String,
        "default": '',
        trim: true
    },
    address3: {
        type: String,
        "default": '',
        trim: true
    },
    city: {
        type: String
    },
    state: {
        type: String
    },
    stateid:{
        type:String
    },
    zipcode: {
        type: String
    },
    contact: [{
        number: {
            type: Number
        },
        type: {
            type: String
        },
        phoneType: {
            type: String
        }
    }],
    security: [{
        question: String,
        answer: String
    }],
    cards: [userCardsSchema],
    password: {
        type: String,
        validate: [
            function(password) {
                return password && password.length > 6;
            }, 'Password should be longer'
        ]
    },
    salt: {
        type: String
    },
    provider: {
        type: String,
        required: 'Provider is required'
    },
    /*facebook: {},
    twitter: {},
    github: {},
    google: {},*/
    roles: Array,
    vendorId: {
        type: Schema.ObjectId,
        ref: 'Department'
    },
    userType: {
        type: String,
        "default": 'customer',
        trim: true
    },
    avatar: {
        original: {
            type: String,
            "default": '',
            trim: true
        }
    },
    status: {
        type: String,
        "default": 'Active',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    /* For reset password */
    resetPasswordToken: {
        type: String
    },
    resetPasswordExpires: {
        type: Date
    },
    eff_date: {
        type: Date
    },
    end_date: {
        type: Date
    },
    upd_date: {
        type: Date,
        default: Date.now
    },
    categary: {
        type: String
    },
    taxregion: {
        type: String
    },
    taxid: {
        type: String
    },
    confirmation: {
        type: String
    },
    dptnu: {
        type: Number,
        ref: 'Department'
    },
    uniqueCode:{
        type:String,
        unique:true
    }

});


UserSchema.virtual('fullName').get(function() {
    return this.firstName + ' ' + this.lastName;
}).set(function(fullName) {
    var splitName = fullName.split(' ');
    this.firstName = splitName[0] || '';
    this.lastName = splitName[1] || '';
});
UserSchema.pre('save', function(next) {

    if (this.password) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'),
            'base64');
        this.password = this.hashPassword(this.password);
    }
    next();
});
UserSchema.methods.hashPassword = function(password) {
    return crypto.pbkdf2Sync(password, this.salt, 10000,
        64).toString('base64');
};
UserSchema.methods.authenticate = function(password) {
    return this.password === this.hashPassword(password);
};

/*UserSchema.methods.getPassword = function() {
     crypto.pbkdf2(this.password, this.salt, 10000, 64, 'base64', function(err, key) {
      if (err)
        throw err;
      console.log(key);  // 'c5e478d...1469e50'
    });
};*/

UserSchema.statics.findUniqueUsername = function(username, suffix,
    callback) {
    var _this = this;
    var possibleUsername = username + (suffix || '');
    _this.findOne({
        username: possibleUsername
    }, function(err, user) {
        if (!err) {
            if (!user) {
                callback(possibleUsername);
            } else {
                return _this.findUniqueUsername(username, (suffix || 0) + 1,
                    callback);
            }
        } else {
            callback(null);
        }
    });
};
UserSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
mongoose.model('User', UserSchema);
mongoose.model('userCards', userCardsSchema);