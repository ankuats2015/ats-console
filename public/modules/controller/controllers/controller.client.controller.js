var app = angular.module('controller');
app.controller('signController', ['$rootScope', '$scope', '$http', '$location', 'Authentication', 'myService', '$sce',
    function($rootScope, $scope, $http, $location, Authentication, myService, $sce) {
        $scope.authentication = {};
        $scope.authentication = Authentication;
        // If user is signed in then redirect back home
        //  if ($scope.authentication.user) $location.path('/');
        $scope.successmsg = "";
        $scope.userid = "";

        var storedData = myService.getTempData();
        //$scope.userType = 'customer';
        $scope.userType = (storedData && storedData.userType) ? storedData.userType : 'customer';
        $scope.authentication = Authentication;
        $scope.credentials = {};
        $scope.init = function() {

            $http.get('/controller/getCompanyDetail/').success(function(data) {
                $scope.aboutus = $sce.trustAsHtml(data[1].aboutus);
                $scope.aboutusbgcolor = data[1].aboutusbgcolor;
                $scope.faq = $sce.trustAsHtml(data[1].faq);
                $scope.faqbgcolor = data[1].faqbgcolor;
                $scope.disclaimers = $sce.trustAsHtml(data[1].disclaimer);
                //$scope.bgcolor=data[1].clientsbgcolor;
                $scope.client = $sce.trustAsHtml(data[1].clients);
                $scope.clientsbgcolor = data[1].clientsbgcolor;
                $scope.howtoget = $sce.trustAsHtml(data[1].howtoget);
                $scope.howtogetbgcolor = data[1].howtogetbgcolor;
            })
        }
        $scope.signin = function() {
                console.log($scope.credentials);
                $http.get('/controller/getUserInit/' + $scope.credentials.username).success(function(data) {
                    console.log(data);
                    if (data != null) {
                        if (data.temp_pass == $scope.credentials.password && data.pass_status == 0) {
                            // $scope.userid=$scope.credentials.username;
                            myService.setUserId($scope.credentials.username);
                            console.log($scope.userid);
                            $location.path('/controller/newcontroller');
                        }
                        if (data.username == $scope.credentials.username) {
                            console.log("njdsjk");
                            $http.post('/controller/signin', {
                                username: $scope.credentials.username,
                                password: $scope.credentials.password
                            }).success(function(response) {
                                console.log(response);
                                /*disable loader*/
                                $rootScope.enableLoader = false;
                                // If successful we assign the response to the global user model
                                $scope.authentication.user = response;
                                myService.setUserId(response);
                                window.user = response.username;
                                // And redirect to the index page
                                $location.path('/controller/addclient');
                            }).error(function(response) {
                                /*disable loader*/
                                $rootScope.enableLoader = false;
                                $scope.error = response.message;
                            });
                        }
                    } else {
                        /*enable loader*/
                        $rootScope.enableLoader = true;
                        
                        $http.get('/controller/getCustDetail/' + $scope.credentials.username).success(function(data) {
                            console.log(data)
                            if (data.userType != 'customer' && data.userType != 'vendor') {
                                $http.post('/auth/signin', {
                                    username: $scope.credentials.username,
                                    password: $scope.credentials.password
                                }).success(function(response) {
                                    console.log(response);
                                    /*disable loader*/
                                    $rootScope.enableLoader = false;
                                    // If successful we assign the response to the global user model
                                    $scope.authentication.user = response;
                                    // And redirect to the index page
                                    $location.path('/controller/addclient');
                                }).error(function(response) {
                                    /*disable loader*/
                                    $rootScope.enableLoader = false;
                                    $scope.error = response.message;
                                });
                            } else {
                                $scope.errormsg = "Login Credentials are  Wrong";
                                $rootScope.enableLoader = false;
                            }
                        })

                    }
                });
            }
            // Change user password
        $scope.resetUserPassword = function(pass) {
            console.log($scope.userid);
            var password = "";
            password = pass;
            password.userid = myService.getUserId();
            $scope.submitted = true;
            $scope.success = $scope.error = null;
            $rootScope.enableLoader = true;
            console.log(password);
            $http.post('/controller/resetPassword', password).success(function(response) {
                // If successful show success message and clear form
                $scope.passwordDetails = null;
                $rootScope.enableLoader = false;
                // Attach user profile
                //Authentication.user = response;
                $scope.successmsg = response.message;
                console.log(response.message);
                // And redirect to the index page
                $location.path('/password-reset/success');
            }).error(function(response) {
                $rootScope.enableLoader = false;
                $scope.error = response.message;
            });
        };

        function checkUserType(utype) {
            if (utype === 'Super User' || utype === "superuser") {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = true;
                $scope.controllertab = true;
                $scope.companytab = true;

            }

            if (utype === 'Admin') {
                $scope.addclienttab = true;
                $scope.updateclienttab = true;
                $scope.salestaxtab = true;
                $scope.customertab = false;
                $scope.controllertab = false;
                $scope.companytab = false;
            }


        }




    }
]);