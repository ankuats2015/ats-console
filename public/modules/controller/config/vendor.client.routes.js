angular.module('controller').config(['$routeProvider',
    function($routeProvider) {

        $routeProvider.
        when('/',{
            templateUrl:'/modules/controller/views/signin.controller.view.html'
        }).
        when('/controller/addclient', {
            templateUrl: '/modules/controller/views/add.client.view.html'
        }).
        when('/controller/updateclient', {
            css: ['modules/controller/css/vendor.css'],
            templateUrl: '/modules/controller/views/update.client.view.html'
        }).
        when('/controller/view', {
            css: ['modules/controller/css/vendor.css'],
            templateUrl: '/modules/controller/views/controller.client.view.html'
        }).
        when('/controller/salestax', {
            css: ['modules/controller/css/vendor.css'],
            
            templateUrl: '/modules/controller/views/sales.tax.client.view.html'
        }).
         when('/controller/custprofile', {
            css: ['modules/controller/css/vendor.css'],
           
            templateUrl: '/modules/controller/views/customer.profile.view.html'
        }).
        when('/controller/contprofile', {
            css: ['modules/vendor/css/vendor.css'],
            controller: 'contProfileController',
            templateUrl: '/modules/controller/views/controller.profile.view.html'
        }).
          when('/controller/company', {
            css: ['modules/controller/css/vendor.css'],
            templateUrl: '/modules/controller/views/company.view.html'
        }). 
          when('/controller/newcontroller', {
            css: ['modules/controller/css/vendor.css'],
            templateUrl: '/modules/controller/views/newcontroller.view.html'
        }).
         when('/aboutus', {
            templateUrl: '/modules/controller/views/aboutus.client.view.html'
        }).
          when('/disclaimers', {
            templateUrl: '/modules/controller/views/disclaimer.client.view.html'
        }).
           when('/faq', {
            templateUrl: '/modules/controller/views/faq.client.view.html'
        }).
            when('/howtoget', {
            templateUrl: '/modules/controller/views/howtoget.client.view.html'
        }).
             when('/clients', {
            templateUrl: '/modules/controller/views/client.view.page.html'
        }).  
        when('/successPage/',{
            templateUrl:'/module/controller/views/save-data-success.client.view.html'
        }).
        otherwise({
            redirectTo: '/'
        });
       
    }
]);

