var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var ProductCategorySchema = new Schema({
    name: {
        type:String,
        required : 'Category Name is required',
    },
    department : {
              type: Schema.ObjectId,
              ref: 'Department'
    },
    parent: { type: Schema.ObjectId,
              ref: 'ProductCategory'
            },
    slug : String,
    childs : [{
        type: Schema.ObjectId,
        ref: 'ProductCategory'
    }],
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('ProductCategory', ProductCategorySchema);