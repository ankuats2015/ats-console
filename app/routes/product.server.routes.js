var product = require('../../app/controllers/product.server.controller');
module.exports = function(app) {
    app.route('/products').post(product.create)
                        .get(product.list);

    app.route('/products/:productId').get(product.read)
                               .put(product.update)
                               .delete(product.delete);

    app.param('productId', product.ProductByID);

    app.route('/product/search-by-word').get(product.searchProducts);		
    app.route('/product/search').get(product.searchByQuery);

    app.route('/category-products/delete').post(product.deleteCategoryProducts);
};